#!/bin/bash

#Script to calculate the Caesar SST finger print for AMOC for every single one of Maryam's monthly ensemble members

data_in_path="/home/ucakmil/lk_non_par_ensemble"
data_out_path="/data/aod/Caesaretal2018_AMOC_from_Ilyasetal2017/individual_members"

HD_COUNTER=1
while [  $HD_COUNTER -le 100 ]
do
    echo " "
    echo "Starting on HD_CONTOUR = $HD_COUNTER"
    echo " "
    ENS_COUNTER=1
    while [  $ENS_COUNTER -le 100 ]
    do
	export INPUT_FILE="$data_in_path/hd_ens$HD_COUNTER/ensemble$ENS_COUNTER.nc"
	export OUTPUT_FILE="$data_out_path/CaesarAMOC_hd_ens$HD_COUNTER.ensemble$ENS_COUNTER.nc"
	if [ -f $OUTPUT_FILE ]; then
            rm $OUTPUT_FILE
	fi
	ncl -n make_individual_Caesaretal2018_timeseries.ncl
	let ENS_COUNTER=ENS_COUNTER+1 
    done  
    let HD_COUNTER=HD_COUNTER+1 
done


#
ncecat -O -v caesar_sst -u ens_mem $data_out_path/CaesarAMOC_hd_ens*.*.nc $data_out_path/../CaesarAMOC_all_ensmem.caesar_sst.nc
ncecat -O -v gm_NDJFMAM -u ens_mem $data_out_path/CaesarAMOC_hd_ens*.*.nc $data_out_path/../CaesarAMOC_all_ensmem.gm_NDJFMAM.nc
ncecat -O -v sg_NDJFMAM -u ens_mem $data_out_path/CaesarAMOC_hd_ens*.*.nc $data_out_path/../CaesarAMOC_all_ensmem.sg_NDJFMAM.nc
ncecat -O -v gm -u ens_mem $data_out_path/CaesarAMOC_hd_ens*.*.nc $data_out_path/../CaesarAMOC_all_ensmem.gm.nc
ncecat -O -v sg -u ens_mem $data_out_path/CaesarAMOC_hd_ens*.*.nc $data_out_path/../CaesarAMOC_all_ensmem.sg.nc

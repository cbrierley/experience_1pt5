; Calculates the AMOC SST-fingerprint index advocated by Caesar et al (2018, Nature) for an individual file on the HadCRUT4 5x5 grid
load "~/ncl/common.ncl" ;only needed to load in *my* functions in NCL prior to v6.4

INPUT_FILE = getenv("INPUT_FILE")  
OUTPUT_FILE = getenv("OUTPUT_FILE")  

;load INPUT_FILE
ifil=addfile(INPUT_FILE,"r")
lat=ifil->latitude
lon=ifil->longitude
time=ifil->time
temp_anom=ifil->temperature_anomaly

;Create a land-sea mask 
lsfil = addfile("$NCARG_ROOT/lib/ncarg/data/cdf/landsea.nc","r")
lsdata = lsfil->LSMASK
lsm = landsea_mask(lsdata,lat,lon)

;Apply mask (otherwise 2m air temp included in global mean)
ssta=temp_anom
ssta=mask(ssta,lsm.gt.0,False)

;create weights
lat!0="lat"
wgt  = NormCosWgtGlobe(lat)

;Create monthly global mean SST index
gm=wgt_areaave_Wrap(ssta,wgt,1.0,1)
gm@long_name="global mean SST anomaly"

;Create the Sub-polar Gyre region mask
sg_mask=ssta(0,:,:);just take year to create the mask
sg_mask=(/0/);start by setting all the values to 0
;individually start turning on locations specified by http://www.pik-potsdam.de/~caesar/AMOC_slowdown/sg_grid.txt
sg_mask(30,23:32)=(/1/)
sg_mask(29,24:32)=(/1/)
sg_mask(28,26:32)=(/1/)
sg_mask(27,27:31)=(/1/)
sg_mask(26,28:30)=(/1/)
;incidentally, it is unclear precisely how to one should apply this dataset on a different grid.
;I have chosen to select common mid-points (as that seems to be what is implied by only providing midpoints).
;An alternative would be drawing a line around the edges of Caesar et al's gridboxes and only selecting sites within that region. 

;Apply that mask to the Temperature anomalies
masked_ssta=ssta ;transfer meta-data
masked_ssta=mask(ssta,sg_mask,1)

;Create the monthly sub-polar gyre SST index
sg=wgt_areaave_Wrap(masked_ssta,wgt,1.0,1)
sg@long_name="sub-polar gyre SST anomaly"

;Create the years for the output file
year=time(1:2003:12) ;tagging as 1st Feb for Nov-May average
     ;striding by 12 from Feb 1850 (i=1) to Dec 2016 (i=2003)

;Create a holding array for the Nov-May indices
NDJFMAM_arr=new(dimsizes(year),float)
NDJFMAM_arr!0="year"
NDJFMAM_arr&year=year
NDJFMAM_arr=(/NDJFMAM_arr@_FillValue/)
NDJFMAM_arr@timing_note="The date stored for the Nov-May average is the February in the middle"

;Create the NDJFMAM sub-polar gyre record
sg_NDJFMAM=NDJFMAM_arr
sg_NDJFMAM(1:)=(sg(10:1998:12)+sg(11:1998:12)+sg(12:1998:12)+sg(13:1998:12)+\
        sg(14:1998:12)+sg(15:1998:12)+sg(16:1998:12))/7.
;Stepping through Nov 1850(i=10) to May 1851 (i=16) [until Jul 2016 (i=1998), so that we don't catch the final months of 2016. 
sg@long_name="sub-polar gyre SST anomaly"

;Create the NDJFMAM global mean record
gm_NDJFMAM=NDJFMAM_arr
gm_NDJFMAM(1:)=(gm(10:1998:12)+gm(11:1998:12)+gm(12:1998:12)+gm(13:1998:12)+\
        gm(14:1998:12)+gm(15:1998:12)+gm(16:1998:12))/7.
;Stepping through Nov 1850(i=10) to May 1851 (i=16) [until Jul 2016 (i=1998), so that we don't catch the final months of 2016. 
gm_NDJFMAM@long_name="Nov-May global mean SST anomaly"

;calculate the difference in area-average temperature anomalies
diff=sg-gm

;Create the new array
caesar_sst=new((/dimsizes(year)/),typeof(ssta))
caesar_sst!0="year"
caesar_sst&year=year
caesar_sst@methodology="<SST_sg>-<SST_gm> after Caesar et al. (2018)"
caesar_sst@code_created_by="make_individual_Caesaretal2018_timeseries.ncl"
caesar_sst@AMOC_conversion="3.8Sv/K (quoted in Caesar et al., 2018)"
caesar_sst@input_file=INPUT_FILE
caesar_sst@creation_date = systemfunc("date")

;Create the final NDJFMAM sst fingerprint
caesar_sst(1:)=(diff(10:1998:12)+diff(11:1998:12)+diff(12:1998:12)+\
        diff(13:1998:12)+diff(14:1998:12)+diff(15:1998:12)+diff(16:1998:12))/7.
;Stepping through Nov 1850(i=10) to May 1851 (i=16) [until Jul 2016 (i=1998), so that we don't catch the final months of 2016. 

;create OUTPUT_FILE
ofil=addfile(OUTPUT_FILE,"c")

;write out to file
ofil->caesar_sst = caesar_sst
ofil->gm_NDJFMAM = gm_NDJFMAM
ofil->sg_NDJFMAM = sg_NDJFMAM
ofil->gm = gm
ofil->sg = sg 



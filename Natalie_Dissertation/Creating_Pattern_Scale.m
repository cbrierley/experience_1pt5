clc, clear all, close all

%% Creating Pattern Scaling From CMIP5 model runs
%Chris Brierley has already put the model runs on the correct 5x5 degree
%grid

%% Input model runs
%note: all model runs must be in same folder as this script for this code
%to work properly

%Read in the filenames
directory='/home/ucfanaw/Natalie_Dissertation_2018'; %this is the directory to the folder with the files... alternatively use the function "uigetdir" to have a pop-up that allows you to select the folder
filename='*ann_alaHadCRUT.nc';

files = dir(fullfile(directory, filename));                                        

%Reformat the names
files=struct2cell(files);
files=files(1,:);


data=[];
num_30_per_model=[];
for i=1:length(files)  
    name=char(files(:,i));
    %read in temperatures from ncfile
    ncid=netcdf.open(name);
    dataset=ncread(name,'temp');
    
    %only take amount of entries that are divisible by 30 (since later we
    %will take the 30-yr average)
    remainder=rem(size(dataset,3),30);
    dataset=dataset(:,:,1:(size(dataset,3)-remainder));
    
    % Reduce data to decadal averages (to reduce inter-annual variability/noise)
    dataset=movmean(dataset,30,3); %creates moving average of 30 elements
    dataset=dataset(:,:,16:30:end); %keeps only the 30th average
    
    % Concatenate the data from this model to data from other models from each iteration of this loop
    data=cat((ndims(dataset)),data,dataset); %stores the 30th average of all the models
    num_30_per_model=[num_30_per_model,(size(data,3))]; %stores how many datapoints per model, in order to identify in figures later
    
    
    netcdf.close(ncid);
end

%% Get the EOF's and PC's

%Area weight the decadaldata first

wgt=sqrt(cosd(-87.5:5:90)); %weight for the middle latitudes for each of 36 lat grids

for i=2:72 %fit the weightings into the same size multi-dimensional array as the decadal data
    wgt(i,:)=wgt(1,:);
end
for i=1:579
    wgt579(:,:,i)=wgt(:,:,1);
end


decadaldataweighted=bsxfun(@times,data,wgt579); %area weight the decadal data

%Calculate the EOF_MAPS(modes), PC, Percent Variance Explained

numeofs=578; %guess and check the number of EOF's needed to explain 95% of variance
[eof_maps_aw,pc_aw,expv_aw]=eof(decadaldataweighted,numeofs);

%Un-area weight the EOF_MAPS in order to plot correctly

for i=1:numeofs
    wgtnumeofs(:,:,i)=wgt(:,:,1);
end
eof_maps_gridded=bsxfun(@ldivide,wgtnumeofs,eof_maps_aw);


%% Plot the EOF's (the Modes)


% change sign of EOFs(and their PCs) to be w.r.t. warming (positive PC during positive global
% temps)

% We know that the global mean temperature for entry 5430/7731 is >1
% degrees K... so let's examine if the PC's are positive or negative

howmanytoplot=7;

globalmeantemp_areaweighted=mean(decadaldataweighted,1);
globalmeantemp_areaweighted=mean(globalmeantemp_areaweighted,2);
GlobMeanTemp(1,1:size(pc_aw,2))=globalmeantemp_areaweighted(1,1,1:size(pc_aw,2)); 


for i=1:howmanytoplot
    % r is regression value
    % m is slope of regression fit
    % b is offset of regression fit
    [r(i),m(i),b(i)]=regression(GlobMeanTemp(1,:),pc_aw(i,:),'one');
end


% If slope (m) is negative, then we want to flip the sign of it's PC and EOF

mult_neg=m>0; % no need to multiply by negative 1, so multiply by 1
mult_neg=double(mult_neg); %change from logical to double
mult_neg(mult_neg==0)=-1;

%multiply the eof_maps and corresponding PCs by what was assigned in
%mult_neg1
for i=1:howmanytoplot
    mult_neg1_sized_eofmaps(1:72,1:36,i)=mult_neg(i);    
    mult_neg1_sized_pcs(i,1:length(data))=mult_neg(i); 
end

eof_maps_gridded_xneg1=bsxfun(@times,eof_maps_gridded(:,:,1:howmanytoplot),mult_neg1_sized_eofmaps);
pc_aw_xneg1=bsxfun(@times,pc_aw(1:howmanytoplot,:),mult_neg1_sized_pcs);



% multiply the eof's by the std of pc, in order to plot them with units of degrees/K


for i=1:howmanytoplot
    std_pc_sized(1:72,1:36,i)=std(pc_aw(i,:));    
end
eof_maps_gridded_degrees=bsxfun(@times,eof_maps_gridded_xneg1(:,:,1:howmanytoplot),std_pc_sized);



% NEED THESE FOR FOLLOWING TO WORK
lat=87.5:-5:-87.5;
long=[-177.5:5:177.5]';
for i=2:72
    lat(i,:)=lat(1,:);
end
for i=2:36
    long(:,i)=long(:,1);
end



% **************************************
% EOF PLOTTED FIGURE (this script plots 7)
% **************************************

% plots the eofs with unique colorbar for first eof
figure
[ha, pos] = tight_subplot_EOFsubset(3,3,[.02 .01],[.03 .03],[.1 .01]) ;

load coastlines
for i=1:howmanytoplot
    hold on
    %subplot(2,3,i) %need to edit this according to how many are 'howmanytoplot'
    axes(ha(i));
    axesm robinson %% rounded sides
    %gridm on
    %framem on
    surfm(lat,long,fliplr(eof_maps_gridded_degrees(:,:,i)));
    plotm(coastlat,coastlon,'k')
    %imagesc(fliplr(eof_maps_gridded_degrees(:,:,i))');
    box off
    set(gca,'XColor','none','YColor','none')

    %colormap(jet)
    
    newmap = jet;                    %starting map
    ncol = size(newmap,1);           %how big is it?
    zpos = floor(1/2 * ncol);    %1/2 of way through
    zpos2=1+floor(1/2 * ncol);
    newmap(zpos,:) = [1 1 1];        %set that position to white
    newmap(zpos2,:)=[1 1 1];
    colormap(newmap);                %activate it
    
    
    title(['Mode ',num2str(i),' - Explained Variance ',num2str(round(expv_aw(i),2)),'%'],'FontWeight','normal','FontSize',8);
    if i==1
        caxis([-3 3])
    else
        caxis([-1.5 1.5])
    end
    hold off
    %set (gca,'XTick',[1:16:72],'XTickLabel',{'-180','-90','0','90','180'},'YTick',[1,9,18,27,36],'YTickLabel',{'90','45','0','-45','-90'})
end
set(ha(9),'Visible','off')
set (ha(8),'Visible','off')
axes(ha(8));
caxis([-3 3])
c=colorbar('Ticks',-3:1:3,'TickLabels',{'-3','-2','-1','0','1','2','3'});
set(c, 'YAxisLocation','left')
%c.Label.String='Temperature change at +1 standard deviation of principal component (K)';
axes(ha(9));
caxis([-1.5 1.5])
c=colorbar('Ticks',-3:1:3,'TickLabels',{'-3','-2','-1','0','1','2','3'});
c.Label.String='                                                                     Temperature change at +1 standard deviation of principal component (K)';
c.Label.Position = [-1 0.001];
set(c, 'YAxisLocation','left')

% **************************************
% VARIANCE EXPLAINED PER MODE FIGURE
% **************************************

cs=cumsum(expv_aw);
x=[1.783,6.7895]; %location of 90% and 95% overall variance explained
y=[100 100];
colour=['r','gn'];
figure
hold on
semilogy(expv_aw(1:50),'LineWidth',2);
%plot(expv_aw(1:20),'LineWidth',2)
for i=1:2
    stem(x(i),y(i),'marker','none','Color',colour(i),'LineWidth',2);
end
legend('Variance Explained by Mode','90% Overall Variance Explained','95% Overall Variance Explained') 
set(gca,'yscal','log')
set(gca,'XMinorTick','on')
hA=gca;
hA.XAxis.MinorTickValues = [1:1:50];
ylim([0 100])
xlim([1 50])
xlabel('Mode')
ylabel('Log(Variance Explained (%))')
title('Variance Explained per Mode')  %do include titles on top of plots?
hold off



% **************************************
% PLOT THE PCs TO SEE WHICH ARE SIGNAL AND WHICH ARE NOISE
% **************************************

%{
% this creates the xticklabels (xxx)
xxx=[];
for i=0:length(num_30_per_model)
    xxx=[xxx;'      '];
end
for i=10:10:length(num_30_per_model)
    xxx(i,:)=['    ',num2str(i)];
end
xxx(1,:)='    1 ';
%}




figure
for i=1:howmanytoplot
    subplot(howmanytoplot,1,i)
    plot(pc_aw_xneg1(i,:))
    if i==howmanytoplot
        xlabel('CMIP5 models (30 year means of one or more runs stiched consecutively')
        set(gca,'XTick',[0,num_30_per_model([1 7 13 14 24 29 30 31 34 35 37 43 44 49 53 57 58 60])],'XTickLabel',{'1' '2' '3' '4' '5' '6' '7' '8' '9' '10' '11' '12' '13' '14' '15' '16' '17' '18' '19'})
    else
        set(gca,'XTickLabel',[],'XTick',[0,num_30_per_model([1 7 13 14 24 29 30 31 34 35 37 43 44 49 53 57 58 60])])
    end
    if i==1
        title('Principal Components of Modes 1-7 for each 30 year time step')   
    end
    xlim([0 579])
    %ylim([-110 100])
    ylabel(['Mode ',num2str(i),' PC'],'Position',[-30 0])
    hYLabel = get(gca,'YLabel');
    set(hYLabel,'rotation',0,'VerticalAlignment','middle')
    set(gca,'Ticklength', [.005 .01])
    box off
end



%% Clear some memory
clear x y wgt ncid name hYLabel ha h1 files filename directory dataset data cs colour coastlon coastlat c


%% Calculate the regression slope of PC/global mean temp (and it's standard deviation)

%Re-calculate r, m, and b, for the adjusted (always positive) pc_aw_xneg1
figure   

%plotregression_7(pc_aw_xneg1(1,:),GlobMeanTemp(1,:),'PC 1',pc_aw_xneg1(2,:),GlobMeanTemp(1,:),'PC 2',pc_aw_xneg1(3,:),GlobMeanTemp(1,:),'PC 3',pc_aw_xneg1(4,:),GlobMeanTemp(1,:),'PC 4',pc_aw_xneg1(5,:),GlobMeanTemp(1,:),'PC 5',pc_aw_xneg1(6,:),GlobMeanTemp(1,:),'PC 6',pc_aw_xneg1(7,:),GlobMeanTemp(1,:),'PC 7')

plot7regressions(GlobMeanTemp(1,:),pc_aw_xneg1(1,:),'Mode 1',GlobMeanTemp(1,:),pc_aw_xneg1(2,:),'Mode 2',GlobMeanTemp(1,:),pc_aw_xneg1(3,:),'Mode 3',GlobMeanTemp(1,:),pc_aw_xneg1(4,:),'Mode 4',GlobMeanTemp(1,:),pc_aw_xneg1(5,:),'Mode 5',GlobMeanTemp(1,:),pc_aw_xneg1(6,:),'Mode 6',GlobMeanTemp(1,:),pc_aw_xneg1(7,:),'Mode 7')

% Check if offset is normal distribution

%{
for i=1:howmanytoplot
    % r is regression value
    % m is slope of regression fit
    % b is offset of regression fit
    [r(i),m(i),b(i)]=regression(GlobMeanTemp(1,:),pc_aw_xneg1(i,:),'one');
end
%}


for i=1:howmanytoplot
    differ(i,:)=pc_aw(i,:)-(m(i)*GlobMeanTemp(1,:)+b(i)); 
    z(i)=std(differ(i,:)); 
    q(i)=mean(differ(i,:)); % mean of offset should be 0
    for j=1:3
        AmountStd(j,i)=(length(differ(differ(i,:)>=(-j*abs(z(i))) & differ(i,:)<=(j*abs(z(i)))))/size(GlobMeanTemp,2))*100;
    end    
end


%% Calculate the residuals

Calculated_World_EOFs=zeros(72,36,579);
Calculated_World_EOFs7=zeros(72,36,howmanytoplot);

for t=1:579
    for i=howmanytoplot
        Calculated_World_EOFs7(:,:,i)=eof_maps_aw(:,:,i).*(m(i)*GlobMeanTemp(t)+b(i)+q(i));
    end
    Calculated_World_EOFs(:,:,t)=sum(Calculated_World_EOFs7,3);
end

Residuals=bsxfun(@minus,decadaldataweighted,Calculated_World_EOFs);



%% Compare Residuals to Global Mean Temperature

mean_residual=mean(Residuals,1);
mean_residual=mean(mean_residual,2);
mean_residual=squeeze(mean_residual);


[residual_r,residual_m,residual_b]=regression(GlobMeanTemp,mean_residual','one');


%% Lets see if there is a trend between residual and latitude or longitude

Res_r=zeros(72,36);
Res_m=zeros(72,36);
Res_b=zeros(72,36);
for i=1:72
    for j=1:36
        [Res_r(i,j),Res_m(i,j),Res_b(i,j)]=regression(squeeze(GlobMeanTemp)',squeeze(Residuals(i,j,:)),'one');
    end
end
            
% This plot shows IF there is a relatiom\nship between residual (slope) and
% latitude/longitude
figure
subplot(2,1,1)
plot(mean(Res_m,1))
xlim([1 36])
xlabel('Latitude (degrees)')
ylabel({'Mean error in reconstruction slope';'for all longitudinal grids'})
title('Zonal mean slope of error in reconstruction')
set(gca,'XTick',[0:3:36],'XTickLabel',{'90','75','60','45','30','15','0','-15','-30','-45','-60','-75','-90'});
    
subplot(2,1,2)
plot(mean(Res_m,2))
ylabel({'Mean error in reconstruction slope';'for all latitudinal grids'})
title('Meridional mean slope of error in reconstruction')
xlim([1 72])
set(gca,'XTick',[0:6:72],'XTickLabel',{'180','150','120','90','60','30','0','-30','-60','-90','-120','-150','-180'});
xlabel('Longitude (degrees)') 

% CONCLUSION: there is no relationship

%%
%% There isn't a relationship... and there is sufficient difference... so: lets treat each grid individually to avoide adding more uncertainty from making a poor average resid for each grid
%%


% calculate the mean and std of each difference REAL RESIDUAL to CALCULATED RESIDUAL (from Res_m and Res_b)

differ_R=zeros(72,36,579);
z_R=zeros(72,36);
q_R=zeros(72,36);

for i=1:72
    for j=1:36
        differ_R(i,j,:)=squeeze(Residuals(i,j,:))'-(Res_m(i,j)*squeeze(GlobMeanTemp)+Res_b(i,j));
        z_R(i,j)=std(differ_R(i,j,:));
        q_R(i,j)=mean(differ_R(i,j,:));
    end
end

% Attempt to calculate the 10000 random differs from real residual to
% calculated

        
ErrorInRecon10000=zeros(72,36,10000);

for i=1:72
    for j=1:36
        ErrorInRecon10000(i,j,:)=z_R(i,j)*randn(10000,1)+q_R(i,j);
    end
end







%% 
%% Save Data For Use In Calculating Pre-Ind/+1.5 Worlds
%%



%% Create and open NetCDF
filename='ValuesUsedForNatalieDissertation4.nc';
ncid=netcdf.create(filename,'CLASSIC_MODEL');
netcdf.close(ncid)
netcdf.open(filename,'WRITE');

%% Create variables that we will save data to

nccreate(filename,'EOF_Maps','Dimensions',{'lon',72,'lat',36,'EOF_Maps',howmanytoplot}) % Will be used
nccreate(filename,'PCs','Dimensions',{'NumberPCs',size(pc_aw,1),'NumberYears',size(pc_aw,2)}) % Just to store
nccreate(filename,'ExplainedVariance','Dimensions',{'ExplainedVariance',size(expv_aw,1),'NumberPCs',size(expv_aw,2)}) % Just to store

nccreate(filename,'PC_Slopes','Dimensions',{'Item',1,'HowManyPCs',howmanytoplot}) % Will be used
nccreate(filename,'PC_offsets','Dimensions',{'Item',1,'HowManyPCs',howmanytoplot}) % Will be used

nccreate(filename,'PC_MeanDifferenceDataPointToSlope','Dimensions',{'Item',1,'HowManyPCs',howmanytoplot}) % Just to store for future reference
nccreate(filename,'PC_StdDifferenceDataPointToSlope','Dimensions',{'Item',1,'HowManyPCs',howmanytoplot}) % Just to store for future reference

nccreate(filename,'PC_10000DifferenceDataPointsToSlope','Dimensions',{'SlopeError10000',10000,'HowManyPCs',howmanytoplot}) % Will be used.  This datset is to match the 10000 from Maryam's, calculated from the PC_Mean... and PC_Std... above

nccreate(filename,'MeanErrorInReconstruction','Dimensions',{'lon',72,'lat',36,'MeanErrorRecon',1}) % OLD, INCORRECT, do not use

nccreate(filename,'StdErrorInReconstruction','Dimensions',{'lon',72,'lat',36,'StdErrorRecon',1}) % OLD, INCORRECT, do not use

nccreate(filename,'Error_MeanDiffer','Dimensions',{'lon',72,'lat',36})
nccreate(filename,'Error_StdDiffer','Dimensions',{'lon',72,'lat',36})

nccreate(filename,'10000ErrorInReconstruction','Dimensions',{'lon',72,'lat',36,'ErrorRecon10000',10000}) % Will be used.  This datset is to match the 10000 from Maryam's, calculated from the MeanError... and StdError... above


nccreate(filename,'Error_Slope','Dimensions',{'lon',72,'lat',36}) % Will be used
nccreate(filename,'Error_Offset','Dimensions',{'lon',72,'lat',36}) % Will be used

%% Write data to the variables

ncwrite(filename,'EOF_Maps',eof_maps_aw(:,:,1:howmanytoplot)) 
ncwrite(filename,'PCs',pc_aw)
ncwrite(filename,'ExplainedVariance',expv_aw)

ncwrite(filename,'PC_Slopes',m)
ncwrite(filename,'PC_offsets',b) 

ncwrite(filename,'PC_MeanDifferenceDataPointToSlope',q) 
ncwrite(filename,'PC_StdDifferenceDataPointToSlope',z) 



%ncwrite(filename,'MeanErrorInReconstruction',Resids(:,:,1))  % OLD INCORRECT do not use
%ncwrite(filename,'StdErrorInReconstruction',Resids(:,:,2)) % OLD INCORRECT do not use

ncwrite(filename,'Error_MeanDiffer',q_R)
ncwrite(filename,'Error_StdDiffer',z_R)


ncwrite(filename,'10000ErrorInReconstruction',ErrorInRecon10000)
ncwrite(filename,'Error_Slope',Res_m)
ncwrite(filename,'Error_Offset',Res_b)

% commented out while working above


% clear some memory
clearvars -except z q howmanytoplot ncid filename
% Calculate the 10 000 dataset using the PC_Mean... and PC_Std...

        % Calculate the 10 000 dataset using the PC_Mean... and PC_Std...

        %Calculate new values for distribution (create more than 10000, so that
        %can make ~perfect values for normal distribution)
        for n=1:howmanytoplot
            
            Error_Slope=z(n).*randn(1000000000,1)+q(n);

            %Create only 10000 values for the distribution (these values are ~perfect
            %normal distribution)
            Error_Slope=sort(Error_Slope,1);

            Error_Slope_10000=[];
            dummy=1;

            while dummy<1000000000

               Error_Slope_10000=[Error_Slope_10000;mean(Error_Slope(dummy:(dummy+99999)))]; 
               dummy=dummy+100000;

            end

            Error_Slope_7_10000(1:10000,n)=Error_Slope_10000(randperm(length(Error_Slope_10000)));
        end
        
ncwrite(filename,'PC_10000DifferenceDataPointsToSlope',Error_Slope_7_10000) 


% clear some memory
clearvars -except z q howmanytoplot ncid filename
% Calculate the 10 000 dataset using the PC_Mean... and PC_Std...

        % Calculate the 10 000 dataset using the PC_Mean... and PC_Std...

        %Calculate new values for distribution (create more than 10000, so that
        %can make ~perfect values for normal distribution)
        for n=1:howmanytoplot
            
            Error_Slope=z(n).*randn(1000000,1)+q(n);

            %Create only 10000 values for the distribution (these values are ~perfect
            %normal distribution)
            Error_Slope=sort(Error_Slope,1);

            Error_Slope_10000=[];
            dummy=1;

            while dummy<1000000

               Error_Slope_10000=[Error_Slope_10000;mean(Error_Slope(dummy:(dummy+99)))]; 
               dummy=dummy+100;

            end

            Error_Slope_7_10000(1:10000,n)=Error_Slope_10000(randperm(length(Error_Slope_10000)));
        end
        
ncwrite(filename,'PC_10000DifferenceDataPointsToSlope',Error_Slope_7_10000) 
netcdf.close(ncid)


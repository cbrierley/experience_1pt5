function [ files ] = GetFileNames(filename)

%% This function:
% 1)Generates 1-by-(number of files of interest) cells holding the names of files of interest

%% Variable Explanation
% files = cells with the names of all the files of interest
% filename = a portion of the filenames common to all files desired to be read in, which is used to identify which files to read in
% variable = (A.K.A. 'field') of interest to be read in, from the NetCDF
%            ***************************************** direct description might need change *****************************************************
%                           direct = 
%            ***********************************************************************************************************************************************

%%
%% *****1*****
%% 
%%
%Make a list of the file names


d='/home/ucfanaw/Natalie_Dissertation_2018'; %this is the directory to the folder with the files... alternatively use the function "uigetdir" to have a pop-up that allows you to select the folder

files = dir(fullfile(d, filename));                                        

%Reformat the names
files=struct2cell(files);
files=files(1,:);

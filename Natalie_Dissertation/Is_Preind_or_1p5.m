
%clc, close all

%% This is the master code that calls in the data from:
%   - Creating_Pattern_Scaling (which generated the pattern scale from CMIP5)
%   - (SOMETHING) (which generates the offset between 1400-1800 and 1850-1900... aka YY3A)
%   - ClimGen Patterns for -1 K global mean temp change (23 patterns, for variety of CMIP5 models)
%        ... also, calculate the 10000 model indices (and the mean pattern for all 23 models) for use
%   - (SOMEONE's) Population Dataset (1851-2016)
%   - MARYAM's 10 000 Global Temperature Dataset (1850-2016)
% Then,
% This code calculates the percent of population (or area/land area) in
% pre-ind or 1.5K for each year 1851-2016 of Maryam's Temperature Dataset
%%
%% Bring in data, all but Maryam's
%%
% ______________________________________
% Bring in Creating_Pattern_Scaling data
% --------------------------------------

filename='ValuesUsedForNatalieDissertation4.nc';
ncid=netcdf.open(filename);

% to fix in future (not ot have two file names)
filename2='ValuesUsedForNatalieDissertation3.nc';
ncid2=netcdf.open(filename2);


    EOF_Maps=ncread(filename,'EOF_Maps');

    PC_Slopes=ncread(filename,'PC_Slopes');
    PC_Offsets=ncread(filename,'PC_offsets');

    PC_MeanDifDataPointToSlope=ncread(filename,'PC_MeanDifferenceDataPointToSlope');
    
    PC_10000DifferenceDataPointsToSlope=ncread(filename,'PC_10000DifferenceDataPointsToSlope');
    
    MeanErrorReconstruction=ncread(filename,'Error_MeanDiffer');
    ErrorInReconstruction10000=ncread(filename,'10000ErrorInReconstruction');
    
    Error_Slopes=ncread(filename,'Error_Slope');
    Error_Offset=ncread(filename,'Error_Offset');
    
    
% ___________________________
% Bring in Preind 1400-1800 to 1850-1900 offset (yy3a)
% ---------------------------

PreindOff_yy3a_mean=ncread(filename2,'PreindOff_yy3a_mean');
Temp=ncread(filename2,'PreindOff_yy3a_10000');
netcdf.close(ncid2)
% _________________________
% Bring in ClimGen Patterns
% -------------------------

PatternNames=GetFileNames('tmp_chgCMIP5*');
    
Patterns=ReadIn(PatternNames,'TMP_YEARLY',1,length(PatternNames),'DoReorg');

PatternMean=mean(Patterns,3); %Creates the mean of the 23 climgen model patterns 

Patterns=cat(ndims(Patterns),Patterns,PatternMean); %Saves the mean as the 24th indexed pattern

% Create the indexing for both using unique patterns per ensemble member, 
% and using the pattern's mean for all ensemble members

climgenin=[];

for i=1:23 %This makes each model be used ~evenly
    climgenin=[climgenin;(ones(434,1)*i)];
    if i<19
        climgenin=[climgenin;i];
    end
end

% Create randomized model pattern selection, as well as mean model pattern selection
%   This is to bring Maryam's dataset back to 1400-1800 baseline
climgenindices=climgenin(randperm(length(climgenin)));%Randomize the model indices
climgenindices(:,2)=ones(10000,1)*24; %All indices are to the pattern mean

%{
% Save these climate indices, as well as the order of the ClimGen CMIP5 Model Patterns
    nccreate(filename,'ClimGen_ModelPatternIndices','Dimensions',{'climgenindices1',10000,'climgenindices2',1});
    ncwrite(filename,'ClimGen_ModelPatternIndices',climgenindices(:,1)) 
    nccreate(filename,'ClimGen_ModelPatterns','Dimensions',{'lon',72,'lat',36,'climgenmodelpatterns',23})
    ncwrite(filename,'ClimGen_ModelPatterns',Patterns(:,:,1:23)) 
%}
netcdf.close(ncid)

% ___________________________
% Bring in Population Dataset
% ---------------------------

population=ncread('popc_05x05.nc','population');

%%
%% Calculate the 'True' Pre-Ind and 'True' +1.5 worlds
%  The 'True' Pre-Ind is where the temperature anomalies everywhere are 0
%  The 'True' +1.5 World is the pattern associated with +1.5
%%

% _______
% ClimGen
% -------

% Pre-industrial
PreIndTrue_ClimGen=zeros(72,36);

% +1.5 K from Pre-ind 1400-1800
True1p5_ClimGen=Patterns(:,:,24).*(-1.5); % - because Patterns is w.r.t. -1, and want over all to be +1.5

% Format the PreIndTrue_ClimGen and True1p5_Climgen to match the format of one of Maryam's ensembles
PreIndTrue_ClimGen_Formatted=zeros(72,36,167,100);
True1p5_ClimGen_Formatted=zeros(72,36,167,100);
for i=1:167
    for j=1:100
        True1p5_ClimGen_Formatted(:,:,i,j)=True1p5_ClimGen;
    end
end

% _______
% Created
% -------

% Pre-industrial
PreIndTrue_Created=PreIndTrue_ClimGen; %These are the same

% +1.5 K (which will be from Pre-ind of 1400-1800)
T=1.5;
for i=1:7
    EOF_PC_1p5(:,:,i)=EOF_Maps(:,:,i).*(PC_Slopes(i)*T+PC_Offsets(i)+PC_MeanDifDataPointToSlope(i)); % note: PC_MeanDifDataPointToSlope ought to be 0, but it is slightly Not zero due to error
end


Created1p5_Offset=sum(EOF_PC_1p5,3)+(Error_Slopes*T*-0.235+Error_Offset+MeanErrorReconstruction);%(Error_Slopes*(-0.235*T));%(Error_Slopes*Temp100(n)+Error_Offset+MeanErrorReconstruction);+Error_Offset+MeanErrorReconstruction);


    % Calculate what the area weighting is
    wgt=sqrt(cosd(-87.5:5:90)); %weight for the middle latitudes for each of 36 lat grids
    for i=2:72 %fit the weightings into the same size multi-dimensional array as the decadal data
        wgt(i,:)=wgt(1,:);
    end

%sum(sum(Created1p5_Offset))/sum(sum(wgt))

    Created1p5_Offset=bsxfun(@rdivide,Created1p5_Offset,wgt); % Un-area weight

True1p5_Created=PreIndTrue_Created+Created1p5_Offset; % THIS IS THE TRUE +1.5

% Format the PreIndTrue_Created and True1p5_Created to match the format of one of Maryam's ensembles
PreIndTrue_Created_Formatted=PreIndTrue_ClimGen_Formatted;
True1p5_Created_Formatted=zeros(72,36,167,100);
for i=1:167
    for j=1:100
        True1p5_Created_Formatted(:,:,i,j)=True1p5_Created;
    end
end

%%
%% Bring in Maryam's (1/100) at a time (AKA 100/10000 at a time)
%   Make it have baseline w.r.t. 1850-1900
%   Do the following twice (once for ClimGen, once for Creating_Patten_Scale)
%       - Apply the pre-ind offset
%       - Calculate the exact Pre-Ind temperature using the 1850-1900 mean and pre-ind offset
%       - Calculate the +1.5 World, using the exact Pre-Ind temperature(and adding mean(pattern)*1.5 K)
%       - Assess which places for all years 1850-2016 are still in Pre-Ind/are now in +1.5 K
%           - Record these as sums
%%

% Document all the file names for Maryam's datasubset
DatasetNames=GetFileNames('ann*');

% Create empty matrices
IsPre_ClimGen=zeros(72,36,(2016-1849)); % because we know Maryam's datasets are from 1850 to 2016
IsPre_Created=zeros(72,36,(2016-1849));
Is1p5_ClimGen=zeros(72,36,(2016-1849));
Is1p5_Created=zeros(72,36,(2016-1849));
TemperatureMean_ClimGen=zeros(72,36,(2016-1849),100);
TemperatureVar_ClimGen=zeros(72,36,(2016-1849),100);
TemperatureMean_Created=zeros(72,36,(2016-1849),100);
TemperatureVar_Created=zeros(72,36,(2016-1849),100);

for i=1:length(DatasetNames) % Do actions for one of Maryam's datasubsets at a time
    
    % Read in (1 at a time) Maryam's datasubsets
    Datasubset=ReadIn(DatasetNames,'temperature_anomaly',i,i,'NoReorg');
    
    % __________________________________________________
    % Make these datasubsets have the baseline 1850-1900
    % --------------------------------------------------
    Mean18501900Subset=mean(Datasubset(:,:,1:50,:),3);
    Datasubset1850=bsxfun(@minus,Datasubset,Mean18501900Subset);
    
    % ___________
    % !!CLIMGEN!!
    % -----------
    
    % ___________________________________________________
    % Make these datasubsets be w.r.t. 1400-1800 baseline
    % ---------------------------------------------------
    Patterns100=Patterns(:,:,climgenindices((i-1)*100+1:(i*100),1));
    Temp100=Temp((i-1)*100+1:(i*100));


    ClimGen_Offset14001800=zeros(72,36,167,100);
    ClimGen_Datasubset1400=[];

    for m=1:100
        for q=1:167    
            for lon=1:72 %************************** There might be better way than these loops
                for lat=1:36
                    ClimGen_Offset14001800(lon,lat,q,m)=Patterns100(lon,lat,m)*Temp100(m);
                end
            end
        end
    end  

    ClimGen_Datasubset1400=bsxfun(@minus,Datasubset1850,ClimGen_Offset14001800);
    
    % ______________________________
    % How Many Are Pre-Ind?  +1.5 K?
    % ------------------------------

    % Pre-Ind
    preindfirst_ClimGen=bsxfun(@minus,ClimGen_Datasubset1400,PreIndTrue_ClimGen_Formatted); % determine difference between grid temperature and pre-ind's grid temperature
    
    subset_ispreind_ClimGen=zeros(72,36,167,100); % default, mark all grids as "not pre-industrial"
    subset_ispreind_ClimGen(preindfirst_ClimGen<=0)=1; % it is pre-industrial if the temperature was less than that of the PreIndTrue_ClimGen
    
    subset_ispreind_ClimGen=sum(subset_ispreind_ClimGen,4,'omitnan'); %sum the 100 ensemble members
    
    IsPre_ClimGen=IsPre_ClimGen+subset_ispreind_ClimGen; % This is the master, that will add them from each loop
    
    % 1.5
    first1p5_ClimGen=bsxfun(@minus,ClimGen_Datasubset1400,True1p5_ClimGen_Formatted); % determine difference between grid temperature and pre-ind's grid temperature
    
    subset_is1p5_ClimGen=zeros(72,36,167,100); % default, mark all grids as "not pre-industrial"
    subset_is1p5_ClimGen(first1p5_ClimGen>=0)=1; % it is +1p5 if the temperature was greater than that of the True1p5_ClimGen
    
    subset_is1p5_ClimGen=sum(subset_is1p5_ClimGen,4,'omitnan'); %sum the 100 ensemble members
    
    Is1p5_ClimGen=Is1p5_ClimGen+subset_is1p5_ClimGen; % This is the master, that will add them from each loop 
    
    % ________________________________________
    % Also record average temperature, and std
    % ----------------------------------------
    TemperatureMean_ClimGen(:,:,:,i)=mean(ClimGen_Datasubset1400,4,'omitnan');
    TemperatureVar_ClimGen(:,:,:,i)=var(ClimGen_Datasubset1400,1,4,'omitnan');
    
    % ___________
    % !!CREATED!!
    % -----------
    
    % ___________________________________________________
    % Make these datasubsets be w.r.t. 1400-1800 baseline
    % ---------------------------------------------------

    PC_DifferenceDataPointsToSlope100=PC_10000DifferenceDataPointsToSlope((i-1)*100+1:(i*100),:);
        
    ErrorInReconstruction100=ErrorInReconstruction10000((i-1)*100+1:(i*100));
    
    EOF_PC_100=zeros(72,36,7);
    Created_Pattern100=zeros(72,36,167,100);
    
    for n=1:100
        
        for j=1:7
            EOF_PC_100(:,:,j)=EOF_Maps(:,:,j)*(PC_Slopes(j)*Temp100(n)+PC_Offsets(j)+PC_DifferenceDataPointsToSlope100(n,j));
        end
        
        for u=1:167
            Created_Pattern_Holder=sum(EOF_PC_100,3)+(Error_Slopes*Temp100(n)*-0.235+Error_Offset+ErrorInReconstruction100(n));%(Error_Slopes*(-0.235*T));%(Error_Slopes*Temp100(n)+Error_Offset+MeanErrorReconstruction);+Error_Offset+MeanErrorReconstruction);
% (Error_Slopes*T+Error_Offset+MeanErrorReconstruction);%(Error_Slopes*(-0.235*T));   %(Error_Slopes*Temp100(n)+Error_Offset+MeanErrorReconstruction);%ErrorInReconstruction100(1,n); % I dont know if 1:167 will work
            Created_Pattern100(:,:,u,n)=bsxfun(@rdivide,Created_Pattern_Holder,wgt); % Un-area weit
        end
        
    end
    
    Created_Datasubset1400=bsxfun(@plus,Datasubset1850,Created_Pattern100);
    
    % ______________________________
    % How Many Are Pre-Ind?  +1.5 K?
    % ------------------------------

    %Pre-Ind
    preindfirst_Created=bsxfun(@minus,Created_Datasubset1400,PreIndTrue_Created_Formatted); % determine difference between grid temperature and pre-ind's grid temperature
    
    subset_ispreind_Created=zeros(72,36,167,100); % default, mark all grids as "not pre-industrial"
    subset_ispreind_Created(preindfirst_Created<=0)=1; % it is pre-industrial if the temperature was less than that of the PreIndTrue_ClimGen
    
    subset_ispreind_Created=sum(subset_ispreind_Created,4); %sum the 100 ensemble members
    
    IsPre_Created=IsPre_Created+subset_ispreind_Created; % This is the master, that will add them from each loop
    
    %1.5
    first1p5_Created=bsxfun(@minus,Created_Datasubset1400,True1p5_Created_Formatted); % determine difference between grid temperature and 1.5's grid temperature
    
    subset_is1p5_Created=zeros(72,36,167,100); % default, mark all grids as "not 1.5"
    subset_is1p5_Created(first1p5_Created>=0)=1; % it is +1p5 if the temperature was greater than that of the True1p5_ClimGen
    
    subset_is1p5_Created=sum(subset_is1p5_Created,4); %sum the 100 ensemble members
    
    Is1p5_Created=Is1p5_Created+subset_is1p5_Created; % This is the master, that will add them from each loop
    
    % ____________________________________________________________
    % Also record average temperature
    %   by recording sum of temps now, and dividing by 10000 later
    % ------------------------------------------------------------
    TemperatureMean_Created(:,:,:,i)=mean(Created_Datasubset1400,4);
    TemperatureVar_Created(:,:,:,i)=var(Created_Datasubset1400,1,4);
    
end

%%
%% Calculate the probability of each grid in each time to be Pre-Ind / +1.5
%   Divide all the master sets by 10000 (that is how many opportunities
%   they have to be the temp)
%%

IsPre_ClimGen=IsPre_ClimGen/10000; %(gives probability, out of 1)... not a percent (out of 100)
IsPre_Created=IsPre_Created/10000;
Is1p5_ClimGen=Is1p5_ClimGen/10000;
Is1p5_Created=Is1p5_Created/10000;


for o=1:167
    WhereIsNaN(:,:,o)=Patterns(:,:,1);
end


% .....Need to reset ClimGen areas to NaN, which are NaN
IsPre_ClimGen_holder=zeros(72,36,167);
IsPre_ClimGen_holder(:,:,:)=NaN;

IsPre_ClimGen_holder(WhereIsNaN>-100)=IsPre_ClimGen(WhereIsNaN>-100);

Is1p5_ClimGen_holder=zeros(72,36,167);
Is1p5_ClimGen_holder(:,:,:)=NaN;

Is1p5_ClimGen_holder(WhereIsNaN>-100)=Is1p5_ClimGen(WhereIsNaN>-100);

% Replace IsPre_ClimGen and Is1p5_ClimGen with the holders
IsPre_ClimGen=IsPre_ClimGen_holder;
Is1p5_ClimGen=Is1p5_ClimGen_holder;

%%
%% Calculate the global mean temperatures and (and their std)
%       1) based on land/surface area
%       2) based on population
%%

% Complete calculation of mean and std. (for each grid/based on gridded dataset)

% Mean
ClimGen_MeanTemps_wrt14001800=mean(TemperatureMean_ClimGen,4,'omitnan');
Created_MeanTemps_wrt14001800=mean(TemperatureMean_Created,4,'omitnan');

% Std
Numerator_ClimGen_StdGridded=zeros(72,36,167);
Numerator_Created_StdGridded=zeros(72,36,167);

for a=1:100
    Numerator_ClimGen_StdGridded=Numerator_ClimGen_StdGridded+1000*(Numerator_ClimGen_StdGridded+(TemperatureVar_ClimGen(:,:,:,i).^2+(TemperatureMean_ClimGen(:,:,:,i)-ClimGen_MeanTemps_wrt14001800).^2));
    Numerator_Created_StdGridded=Numerator_Created_StdGridded+1000*(Numerator_Created_StdGridded+(TemperatureVar_Created(:,:,:,i).^2+(TemperatureMean_Created(:,:,:,i)-Created_MeanTemps_wrt14001800).^2));
end

Denominator_ClimGen=10000;
Denominator_Created=10000;

Variance_ClimGen=Numerator_ClimGen_StdGridded./Denominator_ClimGen;
Variance_Created=Numerator_Created_StdGridded./Denominator_Created;

Std_ClimGen=sqrt(Variance_ClimGen);
Std_Created=sqrt(Variance_Created);

% ___________________________________________________
% Global Mean Temperatures Based On Land/Surface Area
% ---------------------------------------------------

% Calculate total land area (for the ClimGen grid)

for i=1:167
    wgt167(:,:,i)=wgt(:,:);
end

WeightedGrid_LandArea_Total=sum(wgt(Patterns(:,:,1)>-1000)); % only include land area where there is data
WeightedGrid_TotalGlobe_Total=sum(wgt167,2); 
WeightedGrid_TotalGlobe_Total=sum(WeightedGrid_TotalGlobe_Total,1);% all area (land and ocean)

% ____
% Mean Temp --> on the ClimGen Grid <-- AKA "Land"
% ----
% CLIMGEN
ClimGen_Land_GlobalMeanTemps_wrt14001800_AllYears=ClimGen_MeanTemps_wrt14001800.*wgt167;

ClimGen_Land_GlobalMeanTemps_wrt14001800=zeros(167,1);
for f=1:167
    num=ClimGen_Land_GlobalMeanTemps_wrt14001800_AllYears(:,:,f);
    ClimGen_Land_GlobalMeanTemps_Sum=sum(num(Patterns(:,:,1)>-1000));

    ClimGen_Land_GlobalMeanTemps_wrt14001800(f)=ClimGen_Land_GlobalMeanTemps_Sum/WeightedGrid_LandArea_Total; %ClimGen Temp Mean wrt Land Area
end

% ____
% Mean Temp --> on the ClimGen Grid <-- AKA "Land"
% ----
% CREATED
Created_Land_GlobalMeanTemps_wrt14001800_AllYears=Created_MeanTemps_wrt14001800.*wgt167;

Created_Land_GlobalMeanTemps_wrt14001800=zeros(167,1);
for f=1:167
    num=Created_Land_GlobalMeanTemps_wrt14001800_AllYears(:,:,f);
    Created_Land_GlobalMeanTemps_Sum=sum(num(Patterns(:,:,1)>-1000));

    Created_Land_GlobalMeanTemps_wrt14001800(f)=Created_Land_GlobalMeanTemps_Sum/WeightedGrid_LandArea_Total; % Created Temp Mean wrt Land Area
end

    % Also Total Globe for Created

    Created_TotalGlobe_GlobalMeanTemps_wrt14001800_Total=sum(Created_Land_GlobalMeanTemps_wrt14001800_AllYears,2);
    Created_TotalGlobe_GlobalMeanTemps_wrt14001800_Total=sum(Created_TotalGlobe_GlobalMeanTemps_wrt14001800_Total,1);

    Created_TotalGlobe_GlobalMeanTemps_wrt14001800=Created_TotalGlobe_GlobalMeanTemps_wrt14001800_Total./WeightedGrid_TotalGlobe_Total; % Created Temp Mean wrt Total Globe

% ___
% Std of Temp.... --> on the ClimGen Grid <-- AKA "Land"
% ---
Numerator_ClimGen_StdGridded=zeros(167,1); 
Numerator_Created_StdGridded=zeros(167,1); 

    % Also Total Globe for Created
    
        Numerator_Created_StdGlobal=zeros(167,1);


for c=1:167
    for p=1:72
        for e=1:36
            if Patterns(p,e,1)>-1000 % ignore the NaN grids
                Numerator_ClimGen_StdGridded(c)=Numerator_ClimGen_StdGridded(c)+wgt(p,e)*(Variance_ClimGen(p,e,c)^2+(ClimGen_MeanTemps_wrt14001800(p,e,c)+ClimGen_Land_GlobalMeanTemps_wrt14001800(c))^2);
                Numerator_Created_StdGridded(c)=Numerator_Created_StdGridded(c)+wgt(p,e)*(Variance_Created(p,e,c)^2+(Created_MeanTemps_wrt14001800(p,e,c)+Created_Land_GlobalMeanTemps_wrt14001800(c))^2);
            end
            Numerator_Created_StdGlobal(c)=Numerator_Created_StdGlobal(c)+wgt(p,e)*(Variance_Created(p,e,c)^2+(Created_MeanTemps_wrt14001800(p,e,c)+Created_TotalGlobe_GlobalMeanTemps_wrt14001800(c))^2);
        end
    end
end

ClimGen_Land_StdGlobalMeanTemps_wrt14001800=Numerator_ClimGen_StdGridded./sum(wgt(Patterns(:,:,1)>-1000)); % Std global temp mean CLIMGEN gridded
Created_Land_StdGlobalMeanTemps_wrt14001800=Numerator_Created_StdGridded./sum(wgt(Patterns(:,:,1)>-1000)); % Std global temp mean CREATED gridded

Created_Global_StdGlobalMeanTemps_wrt14001800=Numerator_Created_StdGlobal./sum(sum(wgt)); % Std global temp mean CREATED whole globe

% _____
% 1.5 K World! on land grid, and whole world grid
% -----

LandGrid_ClimGen_MeanTemp1p5=bsxfun(@times,True1p5_ClimGen,wgt);
LandGrid_ClimGen_MeanTemp1p5=(sum(LandGrid_ClimGen_MeanTemp1p5(Patterns(:,:,1)>-1000)))/sum(wgt(Patterns(:,:,1)>-1000));

Created_MeanTemp1p5_temporary=True1p5_Created(Patterns(:,:,1)>-1000).*wgt(Patterns(:,:,1)>-1000);
LandGrid_Created_MeanTemp1p5=sum(Created_MeanTemp1p5_temporary)/sum(wgt(Patterns(:,:,1)>-1000));

WorldGrid_Created_MeanTemp1p5=sum(sum(bsxfun(@times,True1p5_Created,wgt)))/sum(sum(wgt));

% for interest of comparison, also calculate the ocean temperature (for good
% pattern scaling, according to IPCC, the land should be 1.4 to 1.7 x the
% ocean temp)

hold=ones(72,36);
hold(Patterns(:,:,1)>-1000)=0;
hold1p5=True1p5_Created(hold>0.5).*wgt(hold>0.5);
OceanGrid_Created_MeanTemp1p5=sum(hold1p5)/sum(wgt(hold>0.5));

OceanGrid_ClimGen_MeanTemp1p5=(1.5*sum(sum(wgt))-sum(wgt(Patterns(:,:,1)>-1000))*LandGrid_ClimGen_MeanTemp1p5)/sum(wgt(hold>0.5));

% check to see if good pattern sclaing (if land is 1.4 to 1.7 times ocean
% temp)
GoodScale_Created=LandGrid_Created_MeanTemp1p5/OceanGrid_Created_MeanTemp1p5;
GoodScale_ClimGen=LandGrid_ClimGen_MeanTemp1p5/OceanGrid_ClimGen_MeanTemp1p5;

% ____________________________________________
% Global Mean Temperatures Based On POPULATION
% --------------------------------------------

% Calculate total population (ensure only for the ClimGen grid)
population_climgengrid=zeros(72,36,166);
for i=1:166
    hold=population(:,:,i);
    
    for long=1:72
        for lat=1:36
            if Patterns(long,lat,1)>-1000
                if hold(long,lat)>-1000
                    population_climgengrid(long,lat,i)=hold(long,lat);
                end
            end
        end
    end
    
end
Population_Total=sum(population_climgengrid,1,'omitnan'); % let there only be data where there are ClimGen grids
Population_Total=sum(Population_Total,2,'omitnan'); 
Population_Total=squeeze(Population_Total);

% ____
% Mean Temp with respect to population
% ----
% CLIMGEN
ClimGen_Population_GlobalMeanTemps_wrt14001800_AllYears=ClimGen_MeanTemps_wrt14001800(:,:,2:167).*population_climgengrid;

ClimGen_Population_GlobalMeanTemps_wrt14001800=zeros(166,1); 
for f=1:166
    ClimGen_Population_GlobalMeanTemps_Sum=sum(ClimGen_Population_GlobalMeanTemps_wrt14001800_AllYears(:,:,f),1,'omitnan');
    ClimGen_Population_GlobalMeanTemps_Sum=sum(ClimGen_Population_GlobalMeanTemps_Sum,2,'omitnan');
    
    ClimGen_Population_GlobalMeanTemps_wrt14001800(f)=ClimGen_Population_GlobalMeanTemps_Sum/Population_Total(f); %ClimGen Temp Mean wrt Population
end

% ____
% Mean Temp with respect to population
% ----
% CREATED
Created_Population_GlobalMeanTemps_wrt14001800_AllYears=Created_MeanTemps_wrt14001800(:,:,2:167).*population_climgengrid;

Created_Population_GlobalMeanTemps_wrt14001800=zeros(166,1);
for f=1:166
    Created_Population_GlobalMeanTemps_Sum=sum(Created_Population_GlobalMeanTemps_wrt14001800_AllYears(:,:,f),1,'omitnan');
    Created_Population_GlobalMeanTemps_Sum=sum(Created_Population_GlobalMeanTemps_Sum,2,'omitnan');
    
    Created_Population_GlobalMeanTemps_wrt14001800(f)=Created_Population_GlobalMeanTemps_Sum/Population_Total(f); % Created Temp Mean wrt Population
end

% ___
% Std of Temp with respect to population
% ---
Numerator_ClimGen_StdPopulation=zeros(166,1); 
Numerator_Created_StdPopulation=zeros(166,1); 

ClimGen_Population_StdGlobalMeanTemps_wrt14001800=zeros(166,1);
Created_Population_StdGlobalMeanTemps_wrt14001800=zeros(166,1);
for c=1:166
    for p=1:72
        for e=1:36
            Numerator_ClimGen_StdPopulation(c)=Numerator_ClimGen_StdPopulation(c)+population(p,e,c)*(Variance_ClimGen(p,e,c+1)^2+(ClimGen_MeanTemps_wrt14001800(p,e,c+1)+ClimGen_Population_GlobalMeanTemps_wrt14001800(c))^2);
            Numerator_Created_StdPopulation(c)=Numerator_Created_StdPopulation(c)+population(p,e,c)*(Variance_Created(p,e,c+1)^2+(Created_MeanTemps_wrt14001800(p,e,c+1)+Created_Population_GlobalMeanTemps_wrt14001800(c))^2);
        end
    end
end

ClimGen_Population_StdGlobalMeanTemps_wrt14001800=Numerator_ClimGen_StdPopulation./Population_Total; % Std global temp mean CLIMGEN population
Created_Population_StdGlobalMeanTemps_wrt14001800=Numerator_Created_StdPopulation./Population_Total; % Std global temp mean CREATED population


% _____
% 1.5 K World! on POPULATION grid
% -----

Population_ClimGen_MeanTemp1p5=bsxfun(@times,True1p5_ClimGen_Formatted(:,:,1:166),population_climgengrid);
Population_ClimGen_MeanTemp1p5=sum(Population_ClimGen_MeanTemp1p5,1,'omitnan');
Population_ClimGen_MeanTemp1p5=sum(Population_ClimGen_MeanTemp1p5,2,'omitnan');

Population_ClimGen_MeanTemp1p5=squeeze(Population_ClimGen_MeanTemp1p5)./squeeze(Population_Total); % Std of 1.5 K world ClimGen

Population_Created_MeanTemp1p5=bsxfun(@times,True1p5_Created_Formatted(:,:,1:166),population_climgengrid);
Population_Created_MeanTemp1p5=sum(Population_Created_MeanTemp1p5,1,'omitnan');
Population_Created_MeanTemp1p5=sum(Population_Created_MeanTemp1p5,2,'omitnan');

Population_Created_MeanTemp1p5=squeeze(Population_Created_MeanTemp1p5)./squeeze(Population_Total); % Std of 1.5 K world Created


%% ________________________________________________________________________
%% Calculate PreInd vs 1.5K (with 5% and 95%)
%   For land/World Area
%   For population
%% ------------------------------------------------------------------------

% _______________
% LAND/World Area
% ---------------

% ClimGen Grid

WorldSum_05_1p5ClimGen_Land=zeros(167,1);
WorldSum_Median_1p5ClimGen_Land=zeros(167,1);
WorldSum_95_1p5ClimGen_Land=zeros(167,1);

WorldSum_05_1p5Created_Land=zeros(167,1);
WorldSum_Median_1p5Created_Land=zeros(167,1);
WorldSum_95_1p5Created_Land=zeros(167,1);

WorldSum_05_PreClimGen_Land=zeros(167,1);
WorldSum_Median_PreClimGen_Land=zeros(167,1);
WorldSum_95_PreClimGen_Land=zeros(167,1);

WorldSum_05_PreCreated_Land=zeros(167,1);
WorldSum_Median_PreCreated_Land=zeros(167,1);
WorldSum_95_PreCreated_Land=zeros(167,1);

% Whole world grid (including oceans)...... this will show/(confirm...by matching other studies/models) if there is more probability of over 1.5 K world for land, or whole world

WorldSum_05_1p5Created_All=zeros(167,1);
WorldSum_Median_1p5Created_All=zeros(167,1);
WorldSum_95_1p5Created_All=zeros(167,1);

WorldSum_05_PreCreated_All=zeros(167,1);
WorldSum_Median_PreCreated_All=zeros(167,1);
WorldSum_95_PreCreated_All=zeros(167,1);


for d=1:167
    for b=1:72
        for g=1:36
            
            if Patterns(b,g,1)>-1000 % (to only get those on ClimGen grid... this is important for the Created dataset
            
                if Is1p5_ClimGen(b,g,d)>=0.05
                    WorldSum_05_1p5ClimGen_Land(d)=WorldSum_05_1p5ClimGen_Land(d)+wgt(b,g)*Is1p5_ClimGen(b,g,d);
                    if Is1p5_ClimGen(b,g,d)>=0.50
                        WorldSum_Median_1p5ClimGen_Land(d)=WorldSum_Median_1p5ClimGen_Land(d)+wgt(b,g)*Is1p5_ClimGen(b,g,d);
                        if Is1p5_ClimGen(b,g,d)>=0.95
                            WorldSum_95_1p5ClimGen_Land(d)=WorldSum_95_1p5ClimGen_Land(d)+wgt(b,g)*Is1p5_ClimGen(b,g,d);
                        end
                    end
                end

                if IsPre_ClimGen(b,g,d)>=0.05
                    WorldSum_05_PreClimGen_Land(d)=WorldSum_05_PreClimGen_Land(d)+wgt(b,g)*IsPre_ClimGen(b,g,d);
                    if IsPre_ClimGen(b,g,d)>=0.50
                        WorldSum_Median_PreClimGen_Land(d)=WorldSum_Median_PreClimGen_Land(d)+wgt(b,g)*IsPre_ClimGen(b,g,d);
                        if IsPre_ClimGen(b,g,d)>=0.95
                            WorldSum_95_PreClimGen_Land(d)=WorldSum_95_PreClimGen_Land(d)+wgt(b,g)*IsPre_ClimGen(b,g,d);
                        end
                    end
                end            
            
                if Is1p5_Created(b,g,d)>=0.05
                    WorldSum_05_1p5Created_Land(d)=WorldSum_05_1p5Created_Land(d)+wgt(b,g)*Is1p5_Created(b,g,d);
                    if Is1p5_Created(b,g,d)>=0.50
                        WorldSum_Median_1p5Created_Land(d)=WorldSum_Median_1p5Created_Land(d)+wgt(b,g)*Is1p5_Created(b,g,d);
                        if Is1p5_Created(b,g,d)>=0.95
                            WorldSum_95_1p5Created_Land(d)=WorldSum_95_1p5Created_Land(d)+wgt(b,g)*Is1p5_Created(b,g,d);
                        end
                    end
                end

                if IsPre_Created(b,g,d)>=0.05
                    WorldSum_05_PreCreated_Land(d)=WorldSum_05_PreCreated_Land(d)+wgt(b,g)*IsPre_Created(b,g,d);
                    if IsPre_Created(b,g,d)>=0.50
                        WorldSum_Median_PreCreated_Land(d)=WorldSum_Median_PreCreated_Land(d)+wgt(b,g)*IsPre_Created(b,g,d);
                        if IsPre_Created(b,g,d)>=0.95
                            WorldSum_95_PreCreated_Land(d)=WorldSum_95_PreCreated_Land(d)+wgt(b,g)*IsPre_Created(b,g,d);
                        end
                    end
                end 
                
            end
            
            % Now, calculate Created for full world
            
            if Is1p5_Created(b,g,d)>=0.05
                WorldSum_05_1p5Created_All(d)=WorldSum_05_1p5Created_All(d)+Is1p5_Created(b,g,d)*wgt(b,g);
                if Is1p5_Created(b,g,d)>=0.50
                    WorldSum_Median_1p5Created_All(d)=WorldSum_Median_1p5Created_All(d)+Is1p5_Created(b,g,d)*wgt(b,g);
                    if Is1p5_Created(b,g,d)>=0.95
                       WorldSum_95_1p5Created_All(d)=WorldSum_95_1p5Created_All(d)+Is1p5_Created(b,g,d)*wgt(b,g);
                    end
                end
            end
            
            if IsPre_Created(b,g,d)>=0.05
                WorldSum_05_PreCreated_All(d)=WorldSum_05_PreCreated_All(d)+IsPre_Created(b,g,d)*wgt(b,g);
                if IsPre_Created(b,g,d)>=0.50
                    WorldSum_Median_PreCreated_All(d)=WorldSum_Median_PreCreated_All(d)+IsPre_Created(b,g,d)*wgt(b,g);
                    if IsPre_Created(b,g,d)>=0.95
                        WorldSum_95_PreCreated_All(d)=WorldSum_95_PreCreated_All(d)+IsPre_Created(b,g,d)*wgt(b,g);
                    end
                end
            end
            
        end
    end
end

% Now, divide all by the total land (or global surface area), to calculate probability in each of 167 years

WorldSum_05_1p5ClimGen_Land=(WorldSum_05_1p5ClimGen_Land./sum(wgt(Patterns(:,:,1)>-1000)))*100;
WorldSum_Median_1p5ClimGen_Land=(WorldSum_Median_1p5ClimGen_Land./sum(wgt(Patterns(:,:,1)>-1000)))*100;
WorldSum_95_1p5ClimGen_Land=(WorldSum_95_1p5ClimGen_Land./sum(wgt(Patterns(:,:,1)>-1000)))*100;

WorldSum_05_1p5Created_Land=(WorldSum_05_1p5Created_Land./sum(wgt(Patterns(:,:,1)>-1000)))*100;
WorldSum_Median_1p5Created_Land=(WorldSum_Median_1p5Created_Land./sum(wgt(Patterns(:,:,1)>-1000)))*100;
WorldSum_95_1p5Created_Land=(WorldSum_95_1p5Created_Land./sum(wgt(Patterns(:,:,1)>-1000)))*100;

WorldSum_05_PreClimGen_Land=(WorldSum_05_PreClimGen_Land./sum(wgt(Patterns(:,:,1)>-1000)))*100;
WorldSum_Median_PreClimGen_Land=(WorldSum_Median_PreClimGen_Land./sum(wgt(Patterns(:,:,1)>-1000)))*100;
WorldSum_95_PreClimGen_Land=(WorldSum_95_PreClimGen_Land./sum(wgt(Patterns(:,:,1)>-1000)))*100;

WorldSum_05_PreCreated_Land=(WorldSum_05_PreCreated_Land./sum(wgt(Patterns(:,:,1)>-1000)))*100;
WorldSum_Median_PreCreated_Land=(WorldSum_Median_PreCreated_Land./sum(wgt(Patterns(:,:,1)>-1000)))*100;
WorldSum_95_PreCreated_Land=(WorldSum_95_PreCreated_Land./sum(wgt(Patterns(:,:,1)>-1000)))*100;

WorldSum_05_1p5Created_All=(WorldSum_05_1p5Created_All./sum(sum(wgt)))*100;
WorldSum_Median_1p5Created_All=(WorldSum_Median_1p5Created_All./sum(sum(wgt)))*100;
WorldSum_95_1p5Created_All=(WorldSum_95_1p5Created_All./sum(sum(wgt)))*100;

WorldSum_05_PreCreated_All=(WorldSum_05_PreCreated_All./sum(sum(wgt)))*100;
WorldSum_Median_PreCreated_All=(WorldSum_Median_PreCreated_All./sum(sum(wgt)))*100;
WorldSum_95_PreCreated_All=(WorldSum_95_PreCreated_All./sum(sum(wgt)))*100;



% __________
% POPULATION
% ----------

% ClimGen Grid

WorldSum_05_1p5ClimGen_Pop=zeros(166,1);
WorldSum_Median_1p5ClimGen_Pop=zeros(166,1);
WorldSum_95_1p5ClimGen_Pop=zeros(166,1);

WorldSum_05_1p5Created_Pop=zeros(166,1);
WorldSum_Median_1p5Created_Pop=zeros(166,1);
WorldSum_95_1p5Created_Pop=zeros(166,1);

WorldSum_05_PreClimGen_Pop=zeros(166,1);
WorldSum_Median_PreClimGen_Pop=zeros(166,1);
WorldSum_95_PreClimGen_Pop=zeros(166,1);

WorldSum_05_PreCreated_Pop=zeros(166,1);
WorldSum_Median_PreCreated_Pop=zeros(166,1);
WorldSum_95_PreCreated_Pop=zeros(166,1);

for d=1:166
    for b=1:72
        for g=1:36
            if population_climgengrid(b,g,d)>0
                if Is1p5_ClimGen(b,g,d+1)>=0.05
                    WorldSum_05_1p5ClimGen_Pop(d)=WorldSum_05_1p5ClimGen_Pop(d)+population(b,g,d)*Is1p5_ClimGen(b,g,d+1);
                    if Is1p5_ClimGen(b,g,d)>=0.50
                        WorldSum_Median_1p5ClimGen_Pop(d)=WorldSum_Median_1p5ClimGen_Pop(d)+population(b,g,d)*Is1p5_ClimGen(b,g,d+1);
                        if Is1p5_ClimGen(b,g,d)>=0.95
                            WorldSum_95_1p5ClimGen_Pop(d)=WorldSum_95_1p5ClimGen_Pop(d)+population(b,g,d)*Is1p5_ClimGen(b,g,d+1);
                        end
                    end
                end

                if IsPre_ClimGen(b,g,d+1)>=0.05
                    WorldSum_05_PreClimGen_Pop(d)=WorldSum_05_PreClimGen_Pop(d)+population(b,g,d)*IsPre_ClimGen(b,g,d+1);
                    if IsPre_ClimGen(b,g,d)>=0.50
                        WorldSum_Median_PreClimGen_Pop(d)=WorldSum_Median_PreClimGen_Pop(d)+population(b,g,d)*IsPre_ClimGen(b,g,d+1);
                        if IsPre_ClimGen(b,g,d)>=0.95
                            WorldSum_95_PreClimGen_Pop(d)=WorldSum_95_PreClimGen_Pop(d)+population(b,g,d)*IsPre_ClimGen(b,g,d+1);
                        end
                    end
                end            
            
                if Is1p5_Created(b,g,d+1)>=0.05
                    WorldSum_05_1p5Created_Pop(d)=WorldSum_05_1p5Created_Pop(d)+population(b,g,d)*Is1p5_Created(b,g,d+1);
                    if Is1p5_Created(b,g,d)>=0.50
                        WorldSum_Median_1p5Created_Pop(d)=WorldSum_Median_1p5Created_Pop(d)+population(b,g,d)*Is1p5_Created(b,g,d+1);
                        if Is1p5_Created(b,g,d)>=0.95
                            WorldSum_95_1p5Created_Pop(d)=WorldSum_95_1p5Created_Pop(d)+population(b,g,d)*Is1p5_Created(b,g,d+1);
                        end
                    end
                end

                if IsPre_Created(b,g,d+1)>=0.05
                    WorldSum_05_PreCreated_Pop(d)=WorldSum_05_PreCreated_Pop(d)+population(b,g,d)*IsPre_Created(b,g,d+1);
                    if IsPre_Created(b,g,d)>=0.50
                        WorldSum_Median_PreCreated_Pop(d)=WorldSum_Median_PreCreated_Pop(d)+population(b,g,d)*IsPre_Created(b,g,d+1);
                        if IsPre_Created(b,g,d)>=0.95
                            WorldSum_95_PreCreated_Pop(d)=WorldSum_95_PreCreated_Pop(d)+population(b,g,d)*IsPre_Created(b,g,d+1);
                        end
                    end
                end 
            end
        end
    end
end

% Now, divide all by the total total POPULATION, to calculate probability in each of 166 years

WorldSum_05_1p5ClimGen_Pop=(WorldSum_05_1p5ClimGen_Pop./Population_Total)*100;
WorldSum_Median_1p5ClimGen_Pop=(WorldSum_Median_1p5ClimGen_Pop./Population_Total)*100;
WorldSum_95_1p5ClimGen_Pop=(WorldSum_95_1p5ClimGen_Pop./Population_Total)*100;

WorldSum_05_1p5Created_Pop=(WorldSum_05_1p5Created_Pop./Population_Total)*100;
WorldSum_Median_1p5Created_Pop=(WorldSum_Median_1p5Created_Pop./Population_Total)*100;
WorldSum_95_1p5Created_Pop=(WorldSum_95_1p5Created_Pop./Population_Total)*100;

WorldSum_05_PreClimGen_Pop=(WorldSum_05_PreClimGen_Pop./Population_Total)*100;
WorldSum_Median_PreClimGen_Pop=(WorldSum_Median_PreClimGen_Pop./Population_Total)*100;
WorldSum_95_PreClimGen_Pop=(WorldSum_95_PreClimGen_Pop./Population_Total)*100;

WorldSum_05_PreCreated_Pop=(WorldSum_05_PreCreated_Pop./Population_Total)*100;
WorldSum_Median_PreCreated_Pop=(WorldSum_Median_PreCreated_Pop./Population_Total)*100;
WorldSum_95_PreCreated_Pop=(WorldSum_95_PreCreated_Pop./Population_Total)*100;

%%
%% Now, percent of people at each latitude grid, that live in Pre Ind/1.5 K+ Worlds
%%
% pre-allocate size of variables
IsPre_ClimGen_PopLat=zeros(36,166);
Is1p5_ClimGen_PopLat=zeros(36,166);
IsPre_Created_PopLat=zeros(36,166);
Is1p5_Created_PopLat=zeros(36,166);
denlat=zeros(36,166); % this is the population 
% (pre-set to NaN)
IsPre_ClimGen_PopLat(:,:)=NaN;
Is1p5_ClimGen_PopLat(:,:)=NaN;
IsPre_Created_PopLat(:,:)=NaN;
Is1p5_Created_PopLat(:,:)=NaN;
denlat(:,:)=NaN;


for d=1:166
    for g=1:36
        
        if sum(population_climgengrid(:,g,d),'omitnan')>0 % otherwise leave values as NaN
            denlat(g,d)=sum(population_climgengrid(:,g,d),'omitnan');
            
            num=sum(IsPre_ClimGen(:,g,d).*population_climgengrid(:,g,d),'omitnan');
            IsPre_ClimGen_PopLat(g,d)=num/denlat(g,d);
            
            num=sum(Is1p5_ClimGen(:,g,d).*population_climgengrid(:,g,d),'omitnan');
            Is1p5_ClimGen_PopLat(g,d)=num/denlat(g,d);
            
            num=sum(IsPre_Created(:,g,d).*population_climgengrid(:,g,d),'omitnan');
            IsPre_Created_PopLat(g,d)=num/denlat(g,d);
            
            num=sum(Is1p5_Created(:,g,d).*population_climgengrid(:,g,d),'omitnan');
            Is1p5_Created_PopLat(g,d)=num/denlat(g,d);
            
        end
        
    end
end

%%
%% Now, percent of people at each longitude grid, that live in Pre Ind/1.5 K+ Worlds
%%
% pre-allocate size of variables
IsPre_ClimGen_PopLong=zeros(72,166);
Is1p5_ClimGen_PopLong=zeros(72,166);
IsPre_Created_PopLong=zeros(72,166);
Is1p5_Created_PopLong=zeros(72,166);
denlong=zeros(72,166);
% (pre-set to NaN)
IsPre_ClimGen_PopLong(:,:)=NaN;
Is1p5_ClimGen_PopLong(:,:)=NaN;
IsPre_Created_PopLong(:,:)=NaN;
Is1p5_Created_PopLong(:,:)=NaN;
denlong(:,:)=NaN;

for d=1:166
    for b=1:72
        
        if sum(population_climgengrid(b,:,d),'omitnan')>0 % otherwise leave values as NaN
            denlong(b,d)=sum(population(b,:,d),'omitnan');
            
            num=sum(IsPre_ClimGen(b,:,d).*population_climgengrid(b,:,d),'omitnan');
            IsPre_ClimGen_PopLong(b,d)=num/denlong(b,d);
            
            num=sum(Is1p5_ClimGen(b,:,d).*population_climgengrid(b,:,d),'omitnan');
            Is1p5_ClimGen_PopLong(b,d)=num/denlong(b,d);
            
            num=sum(IsPre_Created(b,:,d).*population_climgengrid(b,:,d),'omitnan');
            IsPre_Created_PopLong(b,d)=num/denlong(b,d);
            
            num=sum(Is1p5_Created(b,:,d).*population_climgengrid(b,:,d),'omitnan');
            Is1p5_Created_PopLong(b,d)=num/denlong(b,d);
            
        end
        
    end
end








% Save these results
ncid=netcdf.open(filename);
    nccreate(filename,'Probability_14001800Preindustrial_ClimGen','Dimensions',{'lon',72,'lat',36,'years',167});
    ncwrite(filename,'Probability_14001800Preindustrial_ClimGen',IsPre_ClimGen) 
    nccreate(filename,'Probability_14001800Preindustrial_Created','Dimensions',{'lon',72,'lat',36,'years',167});
    ncwrite(filename,'Probability_14001800Preindustrial_Created',IsPre_Created) 
    nccreate(filename,'Probability_1p5wrt14001800_ClimGen','Dimensions',{'lon',72,'lat',36,'years',167});
    ncwrite(filename,'Probability_1p5wrt14001800_ClimGen',Is1p5_ClimGen) 
    nccreate(filename,'Probability_1p5wrt14001800_Created','Dimensions',{'lon',72,'lat',36,'years',167});
    ncwrite(filename,'Probability_1p5wrt14001800_Created',Is1p5_Created)
netcdf.close(ncid)


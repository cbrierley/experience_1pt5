clc, clear all, 

% NOTE: this is the second (out of three) scripts for Natalie's dissertation   
%       [this excludes any additional function scripts]

%% GOAL: Assess if yy3a is normally distributed
% And create a 10 000 array to match Maryam's dataset


%% SET UP: Import dataset yy3a

yy3a=textread('yy3a.txt');

%Make dataset 230000x1 instead of 46000x5
yy3a=[yy3a(:,1);yy3a(:,2);yy3a(:,3);yy3a(:,4);yy3a(:,5)];

%% ASSESSMENT 1: Plot to visually examine if dataset is normally distributed

figure(1)
histogram(yy3a)
title('Histogram of yy3a')
xlabel('Value (°C)')
ylabel('Frequency')

%% ASSESSMENT 2: Assess if ~68% is within 1 std of mean, and ~95% is within 1.96 std of mean

%calculate standard deviation
stdyy3a=std(yy3a);
%calculate mean
meanyy3a=mean(yy3a);


count1=0;
for i=1:length(yy3a)
    if yy3a(i)>(meanyy3a-stdyy3a) && yy3a(i)<(meanyy3a+stdyy3a)
        count1=count1+1;
    end
end

std100=count1/length(yy3a); %This should be ~0.68, if yy3a is normally distributed


count2=0;
for i=1:length(yy3a)
    if yy3a(i)>(meanyy3a-(1.96*stdyy3a)) && yy3a(i)<(meanyy3a+(1.96*stdyy3a))
        count2=count2+1;
    end
end

std196=count2/length(yy3a); %This should be ~0.95, if yy3a is normally distributed

%% CONCLUSION: Yes - yy3a is normally distributed

fprintf('Yes, yy3a is normally distributed, \nwith a mean of %f and standard deviation of %f\n',meanyy3a,stdyy3a)

%% Create 10 000 representative temperature offsets. (Variable 'temp')

%Calculate new values for distribution (create more than 10000, so that
%can make ~perfect values for normal distribution)
tem=stdyy3a.*randn(1000000000,1)+meanyy3a;

%Create only 10000 values for the distribution (these values are ~perfect
%normal distribution)
tem=sort(tem);

temp=[];
dummy=1;

while dummy<1000000000
    
   temp=[temp;mean(tem(dummy:(dummy+99999)))]; 
   dummy=dummy+100000;

end

temp=temp(randperm(length(temp)));

%% Save the average, std, and temp into the file 

filename='ValuesUsedForNatalieDissertation3.nc';

ncid=netcdf.open(filename,'WRITE');

nccreate(filename,'PreindOff_yy3a_mean','Dimensions',{'MeanYy3a',1}) % Will be used
nccreate(filename,'PreindOff_yy3a_std','Dimensions',{'StdYy3a',1}) % Just to keep
nccreate(filename,'PreindOff_yy3a_10000','Dimensions',{'Temp10000',10000,'Yy3a',1}) % Will be used

ncwrite(filename,'PreindOff_yy3a_mean',meanyy3a) % Will use this
ncwrite(filename,'PreindOff_yy3a_std',stdyy3a) % Just to keep
ncwrite(filename,'PreindOff_yy3a_10000',temp) % Will use

netcdf.close(ncid)
        
function [ data ] = ReadIn( filenames, variable, first, last, ReorgNeed )

%% This function:
% 1)Reads in the desired data (works only for NetCDF files, (E.g.: .nc or .n4))
% 2)Calls the function 'ReorgData.m' to reorganize the read-in-data if necessary

%% Variable Explanation
% data = multi-dimensional array with all the data
% filenames = a portion of the filenames common to all files desired to be read in, which is used to identify which files to read in
% variable = (A.K.A. 'field') of interest to be read in, from the NetCDF
% ReorgDataNeed = identifies if the incoming data needs to be altered to fit the 5 by 5 degree grid (the HadCRUT grid, also Maryam's grid).  
%                   if this field = 'NoReorg', then the data is assumed to be already in the correct grid and nothing is done to the data
%                   if this field = 'DoReorg', then the data is processed through the function ReorgData.m to fit the correct 5x5 grid
% first = Indicates the first file to read in (from all the files in filenames)
% last = Indicates the last file to read in (from all the files in filenames). *this should be the same as "first" if reading in only one files

%% 
%Read in the data from all the files
data=[];
for i=first:last   
    name=char(filenames(:,i));
    %read in temperatures from ncfile
    ncid=netcdf.open(name);
    dataset=ncread(name,variable);
    if ReorgNeed=='DoReorg'
        dataset=ReorgData(dataset);
    end
    data=cat((ndims(dataset)+1),data,dataset);
    netcdf.close(ncid)
end


end


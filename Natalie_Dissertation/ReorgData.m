function [ reorged ] = ReorgData( mod )
%This Function is to change the ClimGen Pattern into the same grid as
%Maryam's Dataset


%modify data into cells
mod=mod(:);
mod=reshape(mod,280,720);

mod=mod';
%modify into same cell size as Maryam's dataset


reorged=ones(72,36)*[9.9692100e+36];

%sort out column 7 in trial
x=[];
for i=1:10:711 %for each row... 10 tempclims
    
    for ii=0:9
        iii=i+ii;
        
        if mod(iii,1)<100 %row 1
            x=[x,mod(iii,1)];
        end

        if mod(iii,2)<100 %row 2
            x=[x,mod(iii,2)];
        end    
    end
    
            
    if isempty(x)
        reorged(ceil(i/10),7)=[9.9692100e+36];
    else
        reorged(ceil(i/10),7)=mean(x);
    end
   
    x=[];
        
end


%sort out columns 8 to 34 in trial
for i=1:10:711  %rows

for h=3:10:263 %columns
    
    
    for ii=0:9
        iii=i+ii;
        
    for hh=0:9
        hhh=h+hh;
        
            if mod(iii,hhh)<100
                x=[x,mod(iii,hhh)];
            end
            
    end
    end
    
    if isempty(x)
        reorged(ceil(i/10),(8+((h-3)/10)))=[9.9692100e+36];
    else
        reorged(ceil(i/10),(8+((h-3)/10)))=mean(x);
    end
   
    x=[]; 
    
end
end

%sort out column 35 in trial
for i=1:10:711  %rows
    
    for ii=0:9
        iii=i+ii;
        
        for hhh=273:280 %columns
            
            if mod(iii,hhh)<100
                x=[x,mod(iii,hhh)];
            end
                   
        end
    end
    
    if isempty(x)
        reorged(ceil(i/10),35)=[9.9692100e+36];
    else
        reorged(ceil(i/10),35)=mean(x);
    end
   
    x=[];   
    
end    


%make all empty cells be NaN
reorged(reorged==9.9692100e+36)=NaN;

end







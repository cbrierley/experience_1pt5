clc, clear all, close all

%% GOAL: ascertain uncertainty (5:Mode:95%) for 2015 baseline w.r.t. preindustrial (1400-1800)

%% SETUP: read in data

%HadCRUT4 decadally smoothed data
%*****************This Data is Available for Download at: https://www.metoffice.gov.uk/hadobs/hadcrut4/data/current/download.html
%*****************This data is the HadCRUT4 time series: ensemble members, Global (NH+SH/2), decadally smoothed
        
        observend=zeros(50,1); %1850-1990 annual global means
        yearint=[]; %2015 (year of interest) global means

        for i=1:100 %100 HadCRUT ensembles to read in
            y=strcat('HadCRUT.4.6.0.0.annual_ns_avg_smooth.',num2str(i),'.txt');
            file=fopen(y,'r');
            HadData=fscanf(file,'%f',[6 168])';
            observend=[observend,HadData(1:50,2)];
            yearint=[yearint,HadData(166,2)]; 

        end
        observend=observend(1:50,2:101);


%Difference between 1850-1990 mean and 1400-1800

        %As calculated in MATLAB script bitbucket/experience_1pt5/temp_preind/Is_yy3a_normal_dist.m.... we know
        meandif=-0.079424;
        stddif=-0.108348;
    
%% CALCULATE: 2015 climatology w.r.t. preindustrial(1400-1800)

%Calculate the difference for each 100 HadCRUT data from mean 1850-1900 and 2015
        
        obsdif=[];
        
        for i=1:100
            
            obsdif=[obsdif,(mean(observend(1:50,i))-yearint(i))];     
            
        end
        
%Add on the difference between 1850-1990 mean and 1400-1800 mean to get the difference between 2015 and 1400-1800

        %Create 100 normally distributed temperature changes 1850-1900 to 1400-1800
        dif1850to1400=(stddif.*randn(100,1)+meandif);
        
        %Overall difference between 2015 and 1400-1800 mean
        tempdif=dif1850to1400'+obsdif;       


%% CONCLUDING RESULTS: 5 - mode - 95%

        prctile5=-1*prctile(tempdif,5);
        prctile95=-1*prctile(tempdif,95);
        mod=-1*mode(tempdif);

        fprintf('The 5:Mode:95 is %f : %f : %f\n',prctile5,mod,prctile95)

%% EXTRAS

%extras to visually inspect
        subplot(2,1,1)
        histogram(obsdif,20)

        subplot(2,1,2)
        histogram(tempdif,20)
        
%extras to test if it is normally distributed
        %calculate standard deviation
        stdtempdif=std(tempdif);
        %calculate mean
        meantempdif=mean(tempdif);


        count1=0;
        for i=1:length(tempdif)
            if tempdif(i)>(meantempdif-stdtempdif) && tempdif(i)<(meantempdif+stdtempdif)
                count1=count1+1;
            end
        end

        std100=count1/length(tempdif); %This should be ~0.68, if yy3a is normally distributed


        count2=0;
        for i=1:length(tempdif)
            if tempdif(i)>(meantempdif-(1.96*stdtempdif)) && tempdif(i)<(meantempdif+(1.96*stdtempdif))
                count2=count2+1;
            end
        end

        std196=count2/length(tempdif); %This should be ~0.95, if yy3a is normally distributed
        %conclusion of if it is normally distributed
        fprintf('\n\nThe 2015 baseline with respect to 1400-1800 is normally distributed, \nwith a mean of %f and standard deviation of %f\n',meantempdif,stdtempdif)
        
        fprintf('\n\nAlso the median is %f, and the mean is %f\n\n',median(tempdif), mean(tempdif)')

%code to call function 'temp2015' 100000 times, in order to see if the
%results stabilize

        tempdif2=[];
        
        for i=1:100000
            tempdif2=[tempdif2,temp2015(observend,yearint,meandif,stddif)];
        end
        

        prctile5=-1*prctile(tempdif2,5);
        prctile95=-1*prctile(tempdif2,95);
        mod=-1*mode(tempdif2);

        fprintf('The 5:Mode:95 is %f : %f : %f\n',prctile5,mod,prctile95)
        
        %calculate standard deviation
        stdtempdif2=std(tempdif2);
        %calculate mean
        meantempdif2=mean(tempdif2);
        
        fprintf('\n\nThe 2015 baseline with respect to 1400-1800 is normally distributed, \nwith a mean of %f and standard deviation of %f\n',meantempdif2,stdtempdif2)
        
        fprintf('\n\nAlso the median is %f, and the mean is %f\n\n',median(tempdif2), mean(tempdif2)')

                
             
        
        
        
        
        
        
    




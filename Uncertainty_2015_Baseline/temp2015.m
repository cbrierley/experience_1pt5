function [ tempdif ] = temp2015( observend, yearint, meandif, stddif )

%% temp2015 runs the 'Uncertainty_2015_Baseline.m' process

%The purpose of having this as a function, is to complete repeat
%calculations

%The hope is that with repeat calculations, the 5:Mode:95, Median, and Mean
%will become more consistent

%% CALCULATE: 2015 climatology w.r.t. preindustrial(1400-1800)

%Calculate the difference for each 100 HadCRUT data from mean 1850-1900 and 2015
        
        obsdif=[];
        
        for i=1:100
            
            obsdif=[obsdif,(mean(observend(1:50,i))-yearint(i))];     
            
        end
        
%Add on the difference between 1850-1990 mean and 1400-1800 mean to get the difference between 2015 and 1400-1800

        %Create 100 normally distributed temperature changes 1850-1900 to 1400-1800
        dif1850to1400=(stddif.*randn(100,1)+meandif);
        
        %Overall difference between 2015 and 1400-1800 mean
        tempdif=dif1850to1400'+obsdif;       


end


#!/bin/bash

#Script calculate the standard deviation of interannual variability for each ensemble member

data_dir="/data/aod/Ilyasetal2017_annual"
ens_exts="wrt_18501900 wrt_preind"
for ens_ext in $ens_exts
do  
  cd $data_dir/$ens_ext
  #create one big file for "present-day"
  input_files=`ls 1986.ann_ensmem.$ens_ext.nc 1987.ann_ensmem.$ens_ext.nc 1988.ann_ensmem.$ens_ext.nc 1989.ann_ensmem.$ens_ext.nc 199?.ann_ensmem.$ens_ext.nc 200?.ann_ensmem.$ens_ext.nc 2010.ann_ensmem.$ens_ext.nc 2011.ann_ensmem.$ens_ext.nc 2012.ann_ensmem.$ens_ext.nc 2013.ann_ensmem.$ens_ext.nc 2014.ann_ensmem.$ens_ext.nc 2015.ann_ensmem.$ens_ext.nc`
  for infil in $input_files
  do 
    ncpdq -O -a time,ens_mem,latitude,longitude $infil foo.$infil
  done  
  ncrcat -O foo.????.ann_ensmem.$ens_ext.nc rcat.1986-2015.ann_ensmem.$ens_ext.nc
  # Step 1: average
  ncwa -O -a time rcat.1986-2015.ann_ensmem.$ens_ext.nc avg.1986-2015.ann_ensmem.$ens_ext.nc
  # Step 2: anomaly
  ncbo -O rcat.1986-2015.ann_ensmem.$ens_ext.nc avg.1986-2015.ann_ensmem.$ens_ext.nc anm.1986-2015.ann_ensmem.$ens_ext.nc
  # Step 3: root-mean square
  ncra -O -y rmssdn anm.1986-2015.ann_ensmem.$ens_ext.nc std.1986-2015.ann_ensmem.$ens_ext.nc
  rm foo.????.ann_ensmem.$ens_ext.nc
done 

cd $data_dir
fils=`ls ann_hd_ens*.nc`
for fil in $fils
do
  ncwa -O -a time -d time,0,50 $fil foo.$fil
done
ncrcat -O foo.*.nc avg.18501900.wrt_19611990.nc
rm foo.*.nc
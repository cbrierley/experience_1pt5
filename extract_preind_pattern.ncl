load "~/ncl/common.ncl"

pp_dir="/data/aod/cmip5_processed_files/"

;read sampling file...
sampling_file="sample_years_preind.txt"
filename_strings=systemfunc("grep ann_gm sample_years_preind.txt | cut -d ' ' -f 2")
printVarSummary(filename_strings)
year_strings=systemfunc("grep ann_gm sample_years_preind.txt | cut -d ' ' -f 3")
gm_filenames=get_unique_values(filename_strings)
print(gm_filenames)
nfiles=dimsizes(gm_filenames)
nsamples=dimsizes(year_strings)
model=new(nfiles,string)
run=new(nfiles,string)
year=new(nsamples,integer)
;strip model and run strings via grep and cut from file
do i=0,nfiles-1
  ;take the text after the last underscore
  modelrun_handle=str_get_field(gm_filenames(i),str_fields_count(gm_filenames(i),"_"),"_")
  model(i)=str_get_field(modelrun_handle,1,".")
  run(i)=str_strip(str_sub_str(str_get_field(modelrun_handle,2,"."),str_get_dq()," "))
end do
;strip years from file as well
do i=0,nsamples-1
  if ismissing(year_strings(i)).or.isStrSubset(year_strings(i),"NA") then
    year(i)=year@_FillValue
  else
    year(i)=stringtointeger(str_strip(str_sub_str(year_strings(i),str_get_dq()," ")))
  end if
end do

;create an array to hold the resulting field
grid_file=addfile(pp_dir+"ACCESS1-0/5x5.ts_Amon_ACCESS1-0.rcp45.nc","r")
grid=grid_file->ts
grid=(/grid@_FillValue/);set all to missing
;printVarSummary(grid)
preind_climate=grid(0,:,:)
;printVarSummary(preind_climate)

;loop over individual files
running_yr_total=0
do i=0,nfiles-1
  ;load file
  filename=pp_dir+model(i)+"/5x5.ts_Amon_"+model(i)+"."+run(i)+".nc"
  print("loading "+filename+" [running_yr_total = "+running_yr_total+"]")
  if isfilepresent(filename) then
    fil=addfile(filename,"r")
    this_ensmem_years=year(ind(filename_strings.eq.gm_filenames(i)))
    if .not.all(ismissing(this_ensmem_years)) then
       these_years=get_unique_values(this_ensmem_years)
       ;print(dimsizes(these_years))
       years_count=these_years;just creating array
       do iyr=0,dimsizes(these_years)-1
       	  years_count=num(this_ensmem_years.eq.these_years(iyr))
       end do
       time=fil->time
       ;printVarSummary(time)
       array=grid(0:dimsizes(these_years),:,:)
       ;printVarSummary(array)
       array(0,:,:)=(/preind_climate/)
       do iyr=0,dimsizes(these_years)-1
          startdt=cd_inv_calendar(these_years(iyr)-2,1,1,0,0,0,time@units,0)
          enddt=cd_inv_calendar(these_years(iyr)+2,12,30,0,0,0,time@units,0)
          yr_inds=ind(time.gt.startdt.and.time.lt.enddt)
          if all(ismissing(yr_inds)) then
            years_count(iyr)=0
          else
            temps=fil->ts(yr_inds,:,:)
            array(iyr+1,:,:)=(/dim_avg_n(temps,0)/)
            delete([/temps/])
          end if
          delete([/startdt,enddt,yr_inds/])
       end do
       ;and now update preind_climate. first set wgts
       wgts=new(dimsizes(years_count)+1,float)
       wgts(0)=running_yr_total
       wgts(1:dimsizes(years_count))=(/years_count/)
       wgts=wgts/sum(wgts)
       preind_climate=(/dim_avg_wgt_n(array,wgts,1,0)/)
       running_yr_total=running_yr_total+sum(years_count)
       delete([/these_years,years_count,time,array,wgts,this_ensmem_years/])
    end if
    delete(fil)
  end if
  delete(filename)
end do


;perform sanity check 
lat=preind_climate&lat
lat!0="lat"
wgt  = NormCosWgtGlobe(lat)
wgt!0="lat"
wgt&lat=lat

gm=wgt_areaave(preind_climate,wgt,1.0,1)
print("The global mean temperature change is "+gm)

outfile=pp_dir+"ensemble_mean/preind_climate.nc"
if fileexists(outfile) then
  system("rm "+outfile)
end if
ofil=addfile(outfile,"c")
ofil->ts = preind_climate
ofil->wgt = wgt 

#!/bin/bash

# NOTES FOR LATER: THIS DOES NOT WORK AS NO DOWNLOADED DATA.
# NEEDS TO BE COMBINED WITH LAND-SEA MASK TO CALUCLATE FRACTION.
# SEE ./subsample/select...ncl FOR METHOD TO CREATE THIS. 


##############################################
# Create a necessary functions beforehand #
##############################################
mk_Amon_climo () 
{
    #this function will make a 1961-1990 climotology 
    # this input variables are:
    #  1. model name
    #  2. ensemble member (eg r1i1p1)
    #  3. historical run directory
    #  4. location of a temporary file space
    #  4. output file name

    #read inputs
    model=$1
    ensmem=$2
    hist_rundir=$3
    scratch_dir=$4
    climo_file=$5

    #determine input files
    var_name=`echo $climo_file | cut -d_ -f 1`
    hist_files=`ls $hist_rundir/latest/$var_name/$var_name\_Amon_$model\_historical_$ensmem*.nc`
    months="01 02 03 04 05 06 07 08 09 10 11 12"
    end_dt='1990-12-31'
    if [ -f $climo_file ]; then
	rm $climo_file
    fi
    for mon in $months
    do
	start_dt="1961-$mon-01"
	ncra -O -F -d time,$start_dt,$end_dt,12 $hist_files $scratch_dir/tmp/$mon$climo_file
    done
    ncrcat $scratch_dir/tmp/??$climo_file $climo_file
    rm $scratch_dir/tmp/??$climo_file  
}

###############
# Pseudo-code #
###############
# Determine land-sea fractions on HadCRUT grid and write to ls_wgt file
# Determine latitude weights for HadCRUT grid and write to wgt file
# 
# for each model; do
# For each simulation; do
# create 1961-90 ts climatology
# create 1961-90 tas climatology
# calculate historical ts anomalies on HadCRUT grid
# calculate historical tas anomalies on HadCRUT grid
# Calculate rcp26 ts anomalies on HadCRUT grid
# Calculate rcp26 tas anomalies on HadCRUT grid
# Create concatenated nc file with all tas,ts data
# Append ls_wgts and wgts to this file
# Use ncap2 to calculate merged temperatures
# Write suitable metadata to file
# Calculate associated global mean with ncwa (append)
# move resultant output files for storage
# Tidy up interim files
# Done #simulation
# done #model

#######################################
# Start running the actual processing #
#######################################

NO_OVERWRITE="False" #setting to "True" will prevent the existing files being overwritten [useful for filling holes]

#Set some common paths and variables
script_dir="/home/users/ucfaccb/codes/experience_1pt5/gcms_5x5"
pp_maindir="/home/users/ucfaccb/workspace_ucfaccb/cmip5_merged_temp_5x5"
scratch="/work/scratch-nompiio/ucfaccb"
underscore="_"
export HADCRUT_GRID="$script_dir/HadCRUT_grid.nc"

mkdir $scratch

datasets=`ls -d /home/users/ucfaccb/cmip5/output1/*/*/rcp26/mon/atmos/Amon/r*`
for dataset in $datasets
do
    model=`echo $dataset | cut -d/ -f 8`
    ensmem=`echo $dataset | cut -d/ -f 13`
    
    pp_dir=$pp_maindir/$model
    [ -d $pp_dir ] || mkdir $pp_dir
    scratch_dir=$scratch/$model
    [ -d $scratch_dir ] || mkdir $scratch_dir
    [ -d $scratch_dir/tmp ] || mkdir $scratch_dir/tmp

    hist_rundir=${dataset/rcp26/historical}

    #create 1961-1990 climatology
    cd $scratch_dir
    export CLIMO_FILE=tas_Amon_$model$underscore$ensmem.climo_1961-1990.nc
    if [ ! -f $CLIMO_FILE ] || [ ! $NO_OVERWRITE == "True" ]; then
	echo mk_Amon_climo $model $ensmem $hist_rundir $scratch_dir $CLIMO_FILE
	mk_Amon_climo $model $ensmem $hist_rundir $scratch_dir $CLIMO_FILE
    fi
    export CLIMO_FILE=ts_Amon_$model$underscore$ensmem.climo_1961-1990.nc
    if [ ! -f $CLIMO_FILE ] || [ ! $NO_OVERWRITE == "True" ]; then
	echo mk_Amon_climo $model $ensmem $hist_rundir $scratch_dir $CLIMO_FILE
	mk_Amon_climo $model $ensmem $hist_rundir $scratch_dir $CLIMO_FILE
    fi

    
    if [ -f $CLIMO_FILE ] ; then
     #Remove climatology from hist files
	varnames="tas ts"
	for varname in $varnames
	do
	    export CLIMO_FILE=$scratch_dir/$varname\_Amon_$model$underscore$ensmem.climo_1961-1990.nc
  	    cd $dataset/latest/$varname
  	    files=`ls $varname\_Amon_$model\_rcp26_$ensmem*.nc`
  	    cd $scratch_dir
  	    for fil in $files
  	    do
		export VARNAME="$varname"
  		export INPUT_FILE="$dataset/latest/$varname/$fil"
  		export OUTPUT_FILE="5x5.$fil"
 		if [ ! -f $OUTPUT_FILE ] || [ ! $NO_OVERWRITE == "True" ]; then
  		    if [ -f $OUTPUT_FILE ]; then
  			rm $OUTPUT_FILE
  		    fi
  		    ncl -n $script_dir/remove_climo_regrid.ncl
  		    ncks -O --mk_rec_dmn time $OUTPUT_FILE $OUTPUT_FILE
		fi
  	    done
            cd $hist_rundir/latest/$varname
	    end_dt='2005-12-31'
  	    files=`ls $varname\_Amon_$model\_historical_$ensmem*.nc`
  	    cd $scratch_dir
  	    for fil in $files
  	    do
		export VARNAME="$varname"
  		export INPUT_FILE="$hist_rundir/latest/$varname/$fil"
  		export OUTPUT_FILE="5x5.$fil"
  		if [ ! -f $OUTPUT_FILE ] || [ ! $NO_OVERWRITE == "True" ]; then
  		    if [ -f $OUTPUT_FILE ]; then
  			rm $OUTPUT_FILE
  		    fi
  		    ncl -n $script_dir/remove_climo_regrid.ncl
  		    ncks -O --mk_rec_dmn time -d time,,$end_dt $OUTPUT_FILE $OUTPUT_FILE
		fi
  	    done
	done

  	if [ -f $OUTPUT_FILE ]; then
  	    #Concatenate
	    cd $scratch_dir
	    varnames="tas ts"
	    for varname in $varnames
	    do
  		if [ ! -f 5x5.$varname\_Amon_$model\_rcp26_$ensmem.nc ] || [ ! $NO_OVERWRITE == "True" ]; then
  	            ncrcat -O 5x5.$varname\_Amon_$model\_historical_$ensmem\_*.nc 5x5.$varname\_Amon_$model\_rcp26_$ensmem\_*.nc 5x5.$varname\_Amon_$model\_rcp26_$ensmem.nc
		    ncks -A -v landfrac $script_dir/HadCRUT_grid.nc 5x5.$varname\_Amon_$model\_rcp26_$ensmem.nc
		    #note: the above line comes out with a warning, but it isn't worth worrying about.
		fi
	    done

	    if [ -f 5x5.tas_Amon_$model\_rcp26_$ensmem.nc ] && [ -f 5x5.ts_Amon_$model\_rcp26_$ensmem.nc ]; then
		cp 5x5.tas_Amon_$model\_rcp26_$ensmem.nc 5x5.both_$model\_rcp26_$ensmem.nc
		ncks -A -v ts 5x5.ts_Amon_$model\_rcp26_$ensmem.nc 5x5.both_$model\_rcp26_$ensmem.nc
		ncap2 -O -s "temp=landfrac*tas+(1-landfrac)*ts" 5x5.both_$model\_rcp26_$ensmem.nc 5x5.temp_$model\_rcp26_$ensmem.nc
		#Add now make some useful final files
		ncwa -O -v temp -a lat,lon -w wgt 5x5.temp_$model\_rcp26_$ensmem.nc gm_$model\_rcp26_$ensmem.mon_alaHadCRUT.nc
		ncrename -v temp,gm_temp gm_$model\_rcp26_$ensmem.mon_alaHadCRUT.nc
		ncatted -a long_name,gm_temp,o,c,Global_Mean_Temperature gm_$model\_rcp26_$ensmem.mon_alaHadCRUT.nc
                ncks -O -v temp,wgt 5x5.temp_$model\_rcp26_$ensmem.nc $pp_dir/$model\_rcp26_$ensmem.mon_alaHadCRUT.nc
		ncks -A -v gm_temp gm_$model\_rcp26_$ensmem.mon_alaHadCRUT.nc $pp_dir/$model\_rcp26_$ensmem.mon_alaHadCRUT.nc
		ncra -O --mro -d time,,,12,12 -v temp,wgt 5x5.temp_$model\_rcp26_$ensmem.nc $pp_dir/$model\_rcp26_$ensmem.ann_alaHadCRUT.nc
 		ncra -A --mro -d time,,,12,12 -v gm_temp gm_$model\_rcp26_$ensmem.mon_alaHadCRUT.nc $pp_dir/$model\_rcp26_$ensmem.ann_alaHadCRUT.nc	     
	    fi
	fi             
    fi
   
done

#rm -r $scratch

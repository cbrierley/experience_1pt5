#This file creates the individual model 30 years samples that are used as the input for the princiapl component analysis that is intergral for the pattern scaling.

cd /data/aod/cmip5_processed_files/cmip5_merged_temp_5x5/
ann_files=`ls */*.ann_alaHadCRUT.nc`
for annf in $ann_files
do 
  let nyrs=`ncdump -h $annf | grep UNLIM | cut -d "(" -f 2 | cut -d " " -f 1`
  let rem=1+$nyrs%30
  let num_climos=$nyrs/30
  runname=`echo $annf | cut -d "/" -f 2 | cut -d "." -f 1`
  ncra -O -F --mro -d time,$rem,$nyrs,30,30 $annf ${annf/ann_/30yrclim_}
  if [ $num_climos -lt "15" ] 
  then
    cp ${annf/ann_/30yrclim_} temp_file1.nc 
    let rem=rem+15
    let nyrs=nyrs-15 
    ncra -O -F --mro -d time,$rem,$nyrs,30,30 $annf temp_file2.nc
    ncrcat -O temp_file1.nc temp_file2.nc ${annf/ann_/30yrclim_}
    let foome=$nyrs-$rem
    let num_climos=$num_climos+1+$foome/30
    rm temp_file1.nc temp_file2.nc 
  fi 
  echo $runname,$num_climos 
 done
ncrcat -O */*.30yrclim_*.nc 30yrclim_combined.nc

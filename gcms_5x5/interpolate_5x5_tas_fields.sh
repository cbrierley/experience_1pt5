#!/bin/bash

# NOTES FOR LATER: THIS DOES NOT WORK AS NO DOWNLOADED DATA.
# NEEDS TO BE COMBINED WITH LAND-SEA MASK TO CALCULATE FRACTION.
# SEE ./subsample/select...ncl FOR METHOD TO CREATE THIS. 


##############################################
# Create a necessary functions beforehand #
##############################################
mk_climo () 
{
    #this function will make a 1961-1990 climotology 
    # this input variables are:
    #  1. model name
    #  2. historical run directory
    #  3. output file name

    #read inputs
    model=$1
    hist_rundir=$2
    climo_file=$3

    #determine input files
    hist_files=`ls $hist_rundir/{tas/,}tas_Amon_$model*.nc`
    months="01 02 03 04 05 06 07 08 09 10 11 12"
    end_dt='1990-12-31'
    if [ -f $climo_file ]; then
	rm $climo_file
    fi
    for mon in $months
    do
	start_dt="1961-$mon-01"
	ncra -O -F -d time,$start_dt,$end_dt,12 $hist_files /tmp/$mon$climo_file
    done
    ncrcat /tmp/??$climo_file $climo_file
    rm /tmp/??$climo_file  
}

#######################################
# Start running the actual processing #
#######################################

NO_OVERWRITE="False" #setting to "True" will prevent the existing files being overwritten [useful for filling holes]

expts="historical rcp26"
models="bcc-csm1-1 bcc-csm1-1-m BNU-ESM CanCM4 CanESM2 CMCC-CESM CMCC-CM CMCC-CMS CNRM-CM5-2 CNRM-CM5 ACCESS1-0 ACCESS1-3 CSIRO-Mk3-6-0 FIO-ESM inmcm4 IPSL-CM5A-LR IPSL-CM5A-MR IPSL-CM5B-LR FGOALS-g2 MIROC4h MIROC5 MIROC-ESM-CHEM MIROC-ESM HadCM3 HadGEM2-CC HadGEM2-ES MPI-ESM-LR MPI-ESM-MR MPI-ESM-P MRI-CGCM3 MRI-ESM1 GISS-E2-H-CC GISS-E2-H GISS-E2-R CCSM4 NorESM1-ME NorESM1-M HadGEM2-AO GFDL-CM3 GFDL-ESM2G GFDL-ESM2M CESM1-BGC CESM1-CAM5 CESM1-FASTCHEM CESM1-WACCM CanCM4"
#models="bcc-csm1-1" #name of the gcms
#expts="historical rcp85 piControl"

#Set some common paths and variables
script_dir="/home/ucfaccb/Documents/local_repos/experience_1pt5"
pp_maindir="/data/aod/cmip5_processed_files"
underscore="_"
export HADCRUT4_FILE="/data/aod/obs/HadCRUT.4.5.0.0.median.185001-201612.nc"


for model in $models
do
    pp_dir=$pp_maindir/$model
    hist_rundir=`grep $model/ $script_dir/preind_gcm_samples/historical.tas.rundirs`

    #create 1961-1990 climatology
    mkdir -p $pp_dir
    cd $pp_dir
    export CLIMO_FILE=tas_Amon_$model.climo_1961-1990.nc
    if [ ! -f $CLIMO_FILE ] || [ ! $NO_OVERWRITE == "True" ]; then
	echo mk_climo $model $hist_rundir $CLIMO_FILE
	mk_climo $model $hist_rundir $CLIMO_FILE
    fi

    
    if [ -f $CLIMO_FILE ] ; then
    #Remove climatology from hist files
    for expt in $expts
    do
        echo $expt
        expt_rundir=`grep $model/ $script_dir/preind_gcm_samples/$expt.tas.rundirs`
        if [ $? = 0 ]; then  
	    echo $expt_rundir
  	    cd $expt_rundir
  	    files=`ls {tas/,}tas_Amon_$model*.nc`
  	    cd $pp_dir
  	    for fil in $files
  	    do
  	        export INPUT_FILE="$expt_rundir/$fil"
  	        export OUTPUT_FILE="5x5.$fil"
  	        if [ ! -f $OUTPUT_FILE ] || [ ! $NO_OVERWRITE == "True" ]; then
  		    if [ -f $OUTPUT_FILE ]; then
  		        rm $OUTPUT_FILE
  		    fi
  		    ncl -n $script_dir/remove_climo_regrid.ncl
  		    ncks -O --mk_rec_dmn time $OUTPUT_FILE $OUTPUT_FILE
		fi
  	    done
  	    if [ ${expt:0:3} = "rcp" ] && [ -f $OUTPUT_FILE ]; then
  	        #Concatenate
  	        hist_str="_historical"
  	        rcp_str="_$expt"
  	        if [ ! -f 5x5.tas_Amon_$model.$expt.nc ] || [ ! $NO_OVERWRITE == "True" ]; then
  	            ncrcat -O 5x5.tas_Amon_$model$hist_str*.nc 5x5.tas_Amon_$model$rcp_str*.nc 5x5.tas_Amon_$model.$expt.nc
  	        fi
		if [ $? = 0 ]; then
		    #Create global mean
  	            if [ ! -f gm.tas_Amon_$model.$expt.nc ] || [ ! $NO_OVERWRITE == "True" ]; then
  			ncwa -O -a lat,lon -w wgt -v tas 5x5.tas_Amon_$model.$expt.nc gm.tas_Amon_$model.$expt.nc
		    fi
		    if [ $? = 0 ]; then
  			#Create annual global means
  			if [ ! -f ann_gm.tas_Amon_$model.$expt.nc ] || [ ! $NO_OVERWRITE == "True" ]; then
			    ncra -O --mro -d time,,,12,12 gm.tas_Amon_$model.$expt.nc ann_gm.tas_Amon_$model.$expt.nc 
			fi
		    fi
		fi
            fi
            if [ $expt = "piControl" ]; then
		rcp_str="_$expt"
  	        if [ ! -f 5x5.tas_Amon_$model.$expt.nc ] || [ ! $NO_OVERWRITE == "True" ]; then
  	            ncrcat -O 5x5.tas_Amon_$model$rcp_str*.nc 5x5.tas_Amon_$model.$expt.nc
  	        fi
		if [ $? = 0 ]; then
		    #Create global mean
  	            if [ ! -f gm.tas_Amon_$model.$expt.nc ] || [ ! $NO_OVERWRITE == "True" ]; then
  			ncwa -O -a lat,lon -w wgt -v tas 5x5.tas_Amon_$model.$expt.nc gm.tas_Amon_$model.$expt.nc
		    fi
		    if [ $? = 0 ]; then
  			#Create annual global means
  			if [ ! -f ann_gm.tas_Amon_$model.$expt.nc ] || [ ! $NO_OVERWRITE == "True" ]; then
			    ncra -O --mro -d time,,,12,12 gm.tas_Amon_$model.$expt.nc ann_gm.tas_Amon_$model.$expt.nc 
			fi
		    fi
		fi
  	    fi
        fi             
    done
    fi
    echo   
done


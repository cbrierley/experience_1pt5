#!/bin/bash

#select ensemble members
ens_mems=`ls /data/aod/cmip5_processed_files/*/5x5.ts_Amon_*.rcp??.nc`
mkdir -p /data/aod/cmip5_processed_files/ensemble_mean
ncea -O -d time,'1950-01-01','2079-12-31' $ens_mems /data/aod/cmip5_processed_files/ensemble_mean/5x5.ts_Anom.ensemble_mean.nc


##1.5 deg plot

rm(list=ls())
library(ncdf4)
library(fields)
library(LatticeKrig)

##FUNCTIONS
		##Function of monthly global average
		##sum(x_i_gridpoint * areaof_x_i_gridpoint)/sum(areaof_x_i_gridpoint)
		##and W= cos(lat*0.01745329)      
     
            MON_AVG<- function(A, lon, lat){  #A-3D matrix (lon, lat, time) 
		
			A1<- rep(0,dim(A)[3])
                  w<- cos(lat*(pi/180)) #weights  
			W<- matrix(rep(w,each=length(lon)),nrow=length(lon)) #weights matrix

			for (i in 1:dim(A)[3]){
                        A2<- A[,,i]
                        W1_temp<- c(W)
                        W1_temp[is.na(c(A2))]<- NA
                        W1<- matrix(W1_temp, nrow=nrow(A2), ncol=ncol(A2)) 
                        A3<- A2*W1
                        A1[i]<- sum(A3,2, na.rm=TRUE)/sum(W1, na.rm=TRUE)                    
			}
			A1
			}

		##function to calculate annual averages of a time series
		ann_avg<- function(x,S1){    
                aa<- ts(x,start=c(S1,1),frequency=12)
		    bb<- as.vector(colMeans(matrix(window(aa,start=c(S1,1)),nrow=12)))
      	bb
		}
##HadCRUT4 median ensemble member
      a1<- "D:/HadCRUT.4.5.0.0.median.nc"    #downloaded 16th Feb. 2017
     	fid.t<- nc_open(a1,verbose=TRUE,write=FALSE)
	time.t<- ncvar_get(fid.t,'time')     
	origin.t <- fid.t$dim$time$units
	tunits.t <- strsplit(fid.t$dim$time$units,split=" ") 
	if(identical(tunits.t[[1]][1],"minutes")) tunits.t[[1]][1]<-"mins"
	TIME.t<- strptime(paste(tunits.t[[1]][3],tunits.t[[1]][4]),"%Y-%m-%d %H:%M:%S",tz="GMT")+
                           as.numeric(as.difftime(time.t,units=tunits.t[[1]][1]),units="secs")
	
	lat.t <- ncvar_get(fid.t,'latitude');lat.t                 		#length(lat.t)         #36
	lon.t <- ncvar_get(fid.t,'longitude'); lon.t               		#length(lon.t)         #72
	tempr.a <- ncvar_get(fid.t,'temperature_anomaly')          		#dim(tempr) 

n<-  length(1:which(format(TIME.t,"%B-%Y")=="December-2016"))  #take, 1:2004 i.e. upto December 2016
date_start<- format(TIME.t[1], "%B-%Y")    #"January-1850"             
date_end<- format(TIME.t[n], "%B-%Y")      #"July-2016" 

n.ens<- 100
hd_ind<- (12*trunc(length(TIME.t)/12))     #leave months at the end if not a full year, 
                                           #for annual averages
S_y<- as.numeric(format(TIME.t[1],'%Y'))
E1_y<-  as.numeric(format(TIME.t[hd_ind],'%Y'))

bp.ind<- which(TIME.t>='1850-01-16' & TIME.t<='1879-12-17')       #base-period (1850-1879) index
ind_2016<- which(TIME.t>='2016-01-16' & TIME.t<='2016-12-17')     #2016 index

II<- c(11:39)

Dff<- matrix(NA, nrow=1, ncol=2592)

system.time({
for(i in 1:length(II)){
##read 100 LK-ensembles and calculate monthly area averages
		for(m in 1:100){
			g<- paste("D:/files_18feb/lk_ensembles_hadcrut_v4_5_0_0/hd_ens",II[i],"/",m,"lkens.txt",sep="")
			G<- read.table(g, row.names=NULL)  #print(dim(G))

			B1<- array(t(G[,-1]),dim=c(length(lon.t), length(lat.t),n)) #print(dim(B1))
                  	
			B_BP<- B1[,,bp.ind] 
			B_BP_med<- apply(B_BP, 1:2, mean)

			B_2016<- B1[,,ind_2016]
			B_2016_med<- apply(B_2016, 1:2, mean)

			Dff<- rbind(Dff,c(B_2016_med - B_BP_med))
 			}
}
})
DFF<- Dff[-1,]
RR<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paris_agree/1.5degplot/",II[1],"-",II[length(II)],".txt", sep="")
write.table(DFF, RR)

## time almost half an hour for one ensemble
##(53761.35+484.64)/(29*60)=31.1 #29 is the length(II)

#RR2<- read.table(RR)
#Z2<- array(t(RR2),dim=c(length(lon.t), length(lat.t),(100*length(II))))
#Z<- apply(Z2,1:2,median)


	X11()
	#par(op)
      #e<-  paste("C:/Users/maryam/Dropbox/Maryam/paper_figs_28lkens/fig1.png",sep="")
      #png(e,width = 9, height = 7, units = 'in', res = 300)
	#par(oma=c(1.5,1.5,0,0),mar=c(4,2,2,2),mfrow=c(2,2))
	#zr<- range(Z);zr    
	#brks = seq(-4.4,6.6, by=0.2);brks ;length(brks)
	#cool = rainbow(sum(brks<0), start=rgb2hsv(col2rgb('cyan'))[1], end=rgb2hsv(col2rgb('blue'))[1])
	#warm = rainbow(sum(brks>0), start=rgb2hsv(col2rgb('red'))[1], end=rgb2hsv(col2rgb('yellow'))[1])
	#lut = c(rev(cool),rev(warm))
	#NL=length(brks)-1

	zr1<- range(Z);zr1
	brks1 =  seq(0,1.5,by=0.05);brks1 ;length(brks1)
	warm1 = rainbow(length(brks1)-1, start=rgb2hsv(col2rgb('red'))[1], end=rgb2hsv(col2rgb('yellow'))[1])
	lut1 = c(rev(warm1))
	NL1=length(lut1) 

	mapping = list(x=lon.t, y=lat.t,z = matrix(Z,nrow=length(lon.t),ncol=length(lat.t)))
      image.plot(mapping, col=lut1, main=paste("1.5 deg C plot" , sep=""),
	zlim=zr1,xlim=range(lon.t), ylim=range(lat.t),ylab="",xlab="",breaks=brks1, lab.breaks=brks1, horizontal=TRUE)
      world(add=TRUE)

###################################################################################################################################
#med_ann_avg_lk<- median(ann_avg_lk)	     #median over 100 ensembles,1.070949
#LL_lk<- quantile(ann_avg_lk,probs=0.05)  #5%, 0.9471374 
#UL_lk<- quantile(ann_avg_lk,probs=0.95)  #95%, 1.227705
#Z1<- array(t(Dff),dim=c(length(lon.t), length(lat.t),(100*length(II))))
#Z<- apply(dff,1:2,median)

			#dff[,,m]<- B_2016_med - B_BP_med
#dff<- array(0,dim=c(72,36,100*length(II)))
#Dff<- matrix(0, nrow=100*length(II), ncol=2592)

rm(list=ls())
library(ncdf4)

##Length of the time series is different 75 to 547

##Model-names
A1<- c("ACCESS1-0","ACCESS1-3","bcc-csm1-1","bcc-csm1-1-m","BNU-ESM","CanCM4","CanESM2","CCSM4","CESM1-BGC","CESM1-CAM5",
       "CMCC-CESM","CMCC-CM","CMCC-CMS","CNRM-CM5","CSIRO-Mk3-6-0","FGOALS-g2", "FIO-ESM","GFDL-CM3","GFDL-ESM2G","GFDL-ESM2M",
       "GISS-E2-H","GISS-E2-H-CC","GISS-E2-R","HadCM3","HadGEM2-AO","HadGEM2-CC","HadGEM2-ES","inmcm4","IPSL-CM5A-LR","IPSL-CM5A-MR",
       "IPSL-CM5B-LR","MIROC4h","MIROC5","MIROC-ESM","MIROC-ESM-CHEM","MPI-ESM-LR","MPI-ESM-MR","MRI-CGCM3","MRI-ESM1","NorESM1-M",
       "NorESM1-ME")

##files in all models
A1_1<- c("ann_gm.ts_Amon_ACCESS1-0.rcp45","ann_gm.ts_Amon_ACCESS1-0.rcp85")
A1_2<- c("ann_gm.ts_Amon_ACCESS1-3.rcp45","ann_gm.ts_Amon_ACCESS1-3.rcp85")
A1_3<- c("ann_gm.ts_Amon_bcc-csm1-1.rcp26","ann_gm.ts_Amon_bcc-csm1-1.rcp45","ann_gm.ts_Amon_bcc-csm1-1.rcp60","ann_gm.ts_Amon_bcc-csm1-1.rcp85")
A1_4<- c("ann_gm.ts_Amon_bcc-csm1-1-m.rcp26","ann_gm.ts_Amon_bcc-csm1-1-m.rcp45","ann_gm.ts_Amon_bcc-csm1-1-m.rcp60","ann_gm.ts_Amon_bcc-csm1-1-m.rcp85")
A1_5<- c("ann_gm.ts_Amon_BNU-ESM.rcp26","ann_gm.ts_Amon_BNU-ESM.rcp45","ann_gm.ts_Amon_BNU-ESM.rcp85")
A1_6<- c("ann_gm.ts_Amon_CanCM4.rcp45")
A1_7<- c("ann_gm.ts_Amon_CanESM2.rcp26","ann_gm.ts_Amon_CanESM2.rcp45")
A1_8<- c("ann_gm.ts_Amon_CCSM4.rcp26","ann_gm.ts_Amon_CCSM4.rcp45","ann_gm.ts_Amon_CCSM4.rcp60","ann_gm.ts_Amon_CCSM4.rcp85")
A1_9<- c("ann_gm.ts_Amon_CESM1-BGC.rcp45","ann_gm.ts_Amon_CESM1-BGC.rcp85")
A1_10<- c("ann_gm.ts_Amon_CESM1-CAM5.rcp26","ann_gm.ts_Amon_CESM1-CAM5.rcp45","ann_gm.ts_Amon_CESM1-CAM5.rcp60","ann_gm.ts_Amon_CESM1-CAM5.rcp85")
A1_11<- c("ann_gm.ts_Amon_CMCC-CESM.rcp85")
A1_12<- c("ann_gm.ts_Amon_CMCC-CM.rcp45","ann_gm.ts_Amon_CMCC-CM.rcp85")
A1_13<- c("ann_gm.ts_Amon_CMCC-CMS.rcp45","ann_gm.ts_Amon_CMCC-CMS.rcp85")
A1_14<- c("ann_gm.ts_Amon_CNRM-CM5.rcp26","ann_gm.ts_Amon_CNRM-CM5.rcp45","ann_gm.ts_Amon_CNRM-CM5.rcp85")
A1_15<- c("ann_gm.ts_Amon_CSIRO-Mk3-6-0.rcp26","ann_gm.ts_Amon_CSIRO-Mk3-6-0.rcp45","ann_gm.ts_Amon_CSIRO-Mk3-6-0.rcp60","ann_gm.ts_Amon_CSIRO-Mk3-6-0.rcp85")
A1_16<- c("ann_gm.ts_Amon_FGOALS-g2.rcp26","ann_gm.ts_Amon_FGOALS-g2.rcp45","ann_gm.ts_Amon_FGOALS-g2.rcp85")
A1_17<- c("ann_gm.ts_Amon_FIO-ESM.rcp26","ann_gm.ts_Amon_FIO-ESM.rcp45","ann_gm.ts_Amon_FIO-ESM.rcp60","ann_gm.ts_Amon_FIO-ESM.rcp85")
A1_18<- c("ann_gm.ts_Amon_GFDL-CM3.rcp26","ann_gm.ts_Amon_GFDL-CM3.rcp45","ann_gm.ts_Amon_GFDL-CM3.rcp85")
A1_19<- c("ann_gm.ts_Amon_GFDL-ESM2G.rcp26","ann_gm.ts_Amon_GFDL-ESM2G.rcp45","ann_gm.ts_Amon_GFDL-ESM2G.rcp60","ann_gm.ts_Amon_GFDL-ESM2G.rcp85")
A1_20<- c("ann_gm.ts_Amon_GFDL-ESM2M.rcp26","ann_gm.ts_Amon_GFDL-ESM2M.rcp45","ann_gm.ts_Amon_GFDL-ESM2M.rcp60","ann_gm.ts_Amon_GFDL-ESM2M.rcp85")
A1_21<- c("ann_gm.ts_Amon_GISS-E2-H.rcp26","ann_gm.ts_Amon_GISS-E2-H.rcp45","ann_gm.ts_Amon_GISS-E2-H.rcp60","ann_gm.ts_Amon_GISS-E2-H.rcp85")
A1_22<- c("ann_gm.ts_Amon_GISS-E2-H-CC.rcp45","ann_gm.ts_Amon_GISS-E2-H-CC.rcp85")
A1_23<- c("ann_gm.ts_Amon_GISS-E2-R.rcp26","ann_gm.ts_Amon_GISS-E2-R.rcp45","ann_gm.ts_Amon_GISS-E2-R.rcp60","ann_gm.ts_Amon_GISS-E2-R.rcp85")
A1_24<- c("ann_gm.ts_Amon_HadCM3.rcp45")
A1_25<- c("ann_gm.ts_Amon_HadGEM2-AO.rcp26","ann_gm.ts_Amon_HadGEM2-AO.rcp45","ann_gm.ts_Amon_HadGEM2-AO.rcp60","ann_gm.ts_Amon_HadGEM2-AO.rcp85")
A1_26<- c("ann_gm.ts_Amon_HadGEM2-CC.rcp45","ann_gm.ts_Amon_HadGEM2-CC.rcp85")
A1_27<- c("ann_gm.ts_Amon_HadGEM2-ES.rcp45","ann_gm.ts_Amon_HadGEM2-ES.rcp60","ann_gm.ts_Amon_HadGEM2-ES.rcp85")
A1_28<- c("ann_gm.ts_Amon_inmcm4.rcp45","ann_gm.ts_Amon_inmcm4.rcp85")
A1_29<- c("ann_gm.ts_Amon_IPSL-CM5A-LR.rcp26","ann_gm.ts_Amon_IPSL-CM5A-LR.rcp45","ann_gm.ts_Amon_IPSL-CM5A-LR.rcp60","ann_gm.ts_Amon_IPSL-CM5A-LR.rcp85")
A1_30<- c("ann_gm.ts_Amon_IPSL-CM5A-MR.rcp26","ann_gm.ts_Amon_IPSL-CM5A-MR.rcp45","ann_gm.ts_Amon_IPSL-CM5A-MR.rcp60","ann_gm.ts_Amon_IPSL-CM5A-MR.rcp85")
A1_31<- c("ann_gm.ts_Amon_IPSL-CM5B-LR.rcp45","ann_gm.ts_Amon_IPSL-CM5B-LR.rcp85")
A1_32<- c("ann_gm.ts_Amon_MIROC4h.rcp45")
A1_33<- c("ann_gm.ts_Amon_MIROC5.rcp26","ann_gm.ts_Amon_MIROC5.rcp45","ann_gm.ts_Amon_MIROC5.rcp60","ann_gm.ts_Amon_MIROC5.rcp85")
A1_34<- c("ann_gm.ts_Amon_MIROC-ESM.rcp26","ann_gm.ts_Amon_MIROC-ESM.rcp45","ann_gm.ts_Amon_MIROC-ESM.rcp60","ann_gm.ts_Amon_MIROC-ESM.rcp85")
A1_35<- c("ann_gm.ts_Amon_MIROC-ESM-CHEM.rcp26","ann_gm.ts_Amon_MIROC-ESM-CHEM.rcp45","ann_gm.ts_Amon_MIROC-ESM-CHEM.rcp60","ann_gm.ts_Amon_MIROC-ESM-CHEM.rcp85")
A1_36<- c("ann_gm.ts_Amon_MPI-ESM-LR.rcp26","ann_gm.ts_Amon_MPI-ESM-LR.rcp45","ann_gm.ts_Amon_MPI-ESM-LR.rcp85")
A1_37<- c("ann_gm.ts_Amon_MPI-ESM-MR.rcp26","ann_gm.ts_Amon_MPI-ESM-MR.rcp45","ann_gm.ts_Amon_MPI-ESM-MR.rcp85")
A1_38<- c("ann_gm.ts_Amon_MRI-CGCM3.rcp26","ann_gm.ts_Amon_MRI-CGCM3.rcp45","ann_gm.ts_Amon_MRI-CGCM3.rcp60","ann_gm.ts_Amon_MRI-CGCM3.rcp85")
A1_39<- c("ann_gm.ts_Amon_MRI-ESM1.rcp85")
A1_40<- c("ann_gm.ts_Amon_NorESM1-M.rcp26","ann_gm.ts_Amon_NorESM1-M.rcp45","ann_gm.ts_Amon_NorESM1-M.rcp60","ann_gm.ts_Amon_NorESM1-M.rcp85")
A1_41<- c("ann_gm.ts_Amon_NorESM1-ME.rcp26", "ann_gm.ts_Amon_NorESM1-ME.rcp45","ann_gm.ts_Amon_NorESM1-ME.rcp60","ann_gm.ts_Amon_NorESM1-ME.rcp85")

AA1<- c(A1_1,A1_2,A1_3,A1_4,A1_5,A1_6,A1_7,A1_8,A1_9,A1_10,
	  A1_11,A1_12,A1_13,A1_14,A1_15,A1_16,A1_17,A1_18,A1_19,A1_20,
	  A1_21,A1_22,A1_23,A1_24,A1_25,A1_26,A1_27,A1_28,A1_29,A1_30,
	  A1_31,A1_32,A1_33,A1_34,A1_35,A1_36,A1_37,A1_38,A1_39,A1_40,A1_41)

	##~~~~Read any one .nc file and check variables~~~~
	##also check ts read below in variable TS5 using 
	##all(TS.b==TS5[,5][!is.na(TS5[,5])])

	BB<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/ann_gm.ts/",A1[14],"/",A1_14[1],".nc",sep="")
	fid.b<- nc_open(BB,verbose=FALSE,write=FALSE)
				##identify the variables
				netcdf.file <- BB
				nc = ncdf4::nc_open(netcdf.file)
				variables = names(nc[['var']])
				variables
	TS.b<- ncvar_get(fid.b,"ts")
	TS.b
	print(fid.b)
	nc_close(fid.b)

	  ##~~~~Chech starting year~~~~
	  ##manully check starting years using print(fid.b) read from 4th last line. It can be read from the .nc file as well but
	  ##some models start with year 001 that includes A1[1], A1[2] and A1[16] and A1[6] reads 1850 as starting year whereas 
	  ## it is different when we read from print(fid.b)
        TT<- c(1850,1850,1850,1850,1850, 
		   1961,1850,1850,1850,1850,
               1850,1850,1850,1850,1850, 
               1850,1850,1860,1861,1861,
               1850,1850,1850,1859,1860, 
               1859,1859,1850,1850,1850,
               1850,1950,1850,1850,1850,
               1850,1850,1850,1851,1850,
        	   1850)

	  ##starting year of each nc file derived from TT but is confirmed below in B4
        TT1<- c(rep(1850,length(A1_1)),rep(1850,length(A1_2)),rep(1850,length(A1_3)),rep(1850,length(A1_4)),rep(1850,length(A1_5)),
	  rep(1961,length(A1_6)),rep(1850,length(A1_7)),rep(1850,length(A1_8)),rep(1850,length(A1_9)),rep(1850,length(A1_10)),
        rep(1850,length(A1_11)),rep(1850,length(A1_12)),rep(1850,length(A1_13)),rep(1850,length(A1_14)),rep(1850,length(A1_15)),
	  rep(1850,length(A1_16)),rep(1850,length(A1_17)),rep(1860,length(A1_18)),rep(1861,length(A1_19)),rep(1861,length(A1_20)),
        rep(1850,length(A1_21)),rep(1850,length(A1_22)),rep(1850,length(A1_23)),rep(1859,length(A1_24)),rep(1860,length(A1_25)),
	  rep(1859,length(A1_26)),rep(1859,length(A1_27)),rep(1850,length(A1_28)),rep(1850,length(A1_29)),rep(1850,length(A1_30)),
        rep(1850,length(A1_31)),rep(1950,length(A1_32)),rep(1850,length(A1_33)),rep(1850,length(A1_34)),rep(1850,length(A1_35)),
        rep(1850,length(A1_36)),rep(1850,length(A1_37)),rep(1850,length(A1_38)),rep(1851,length(A1_39)),rep(1850,length(A1_40)),
	  rep(1850,length(A1_41)))

	  ##identify starting year
	  B2<- numeric(0)
	  for(i in 1:length(A1)){
	   M.b<- eval(parse(text=paste("A1_",i,sep=""))) #read files within each folder
	   for(j in 1:length(M.b)){
	   BBB<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/ann_gm.ts/",A1[i],"/",M.b[j],".nc",sep="")
         fid.bb<- nc_open(BBB,verbose=FALSE,write=FALSE)
	   TS1<- ncvar_get(fid.bb,"ts")
	   origin.bb <- fid.bb$dim$time$units
	   tunits.bb <- strsplit(fid.bb$dim$time$units,split=" ") 
         nc_close(fid.bb)
	   B2<- append(B2,tunits.bb[[1]][3])
		}
	   }

	 B3<- as.numeric(substring(B2,1,4))
	 cbind(AA1,B3)      

	#manually check starting year of the models showing starting year as 1, ACCESS1-0 i.e. A1[1], ACCESS1-3 i.e. A1[2], FGOALS-g2 i.e. A1[16]
	BBB1<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/ann_gm.ts/",A1[1],"/",A1_1[1],".nc",sep="")
	fid.bb1<- nc_open(BBB1,verbose=TRUE,write=FALSE)
	print(fid.bb1) #1850

	BBB2<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/ann_gm.ts/",A1[2],"/",A1_2[1],".nc",sep="")
	fid.bb2<- nc_open(BBB2,verbose=TRUE,write=FALSE)
	print(fid.bb2) #1850

	BBB3<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/ann_gm.ts/",A1[3],"/",A1_3[1],".nc",sep="")
	fid.bb3<- nc_open(BBB3,verbose=TRUE,write=FALSE)
	print(fid.bb3) #1850

	##Above are identified by comparing with a file where year is explicitly mentioned
	BBB4<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/ann_gm.ts/",A1[19],"/",A1_19[1],".nc",sep="")
	fid.bb4<- nc_open(BBB4,verbose=TRUE,write=FALSE)
	print(fid.bb4)

	which(TT1!=B3)
	cbind(AA1,TT1,B3)

	B4<- B3
	B4[which(B3==1)]<- 1850
	B4[16]<- TT1[16]
	cbind(AA1,B4)

##~~~~Find range of each time series, variable A2 and~~~~ 
##~~~~see all elements of ts are not NAs in variable A2a~~~~

A2<- numeric(0)
A2a<- numeric(0)

	for(i in 1:length(A1)){
	   M1<- eval(parse(text=paste("A1_",i,sep=""))) #read files within each folder

	   for(j in 1:length(M1)){
	   AA<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/ann_gm.ts/",A1[i],"/",M1[j],".nc",sep="")
         fid.a<- nc_open(AA,verbose=FALSE,write=FALSE)
	   TS1<- ncvar_get(fid.a,"ts")
	   nc_close(fid.a)
	   A2<- append(A2,length(TS1))
	   A2a<- append(A2a, sum(!is.na(TS1)))
		}
	}
range(A2)		   #75 and 547
cbind(AA1,A2a)       #***confirm certain model ensemble reads NA as ts

###~~~~Identify threshold years (and temperature as a check)
THR<- numeric(0)
TEM<- numeric(0)

for(i in 1:length(A1)){
	   M1<- eval(parse(text=paste("A1_",i,sep="")))      #read files within each folder
for(j in 1:length(M1)){
	   CC<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/ann_gm.ts/",A1[i],"/",M1[j],".nc",sep="")
         fid.c<- nc_open(CC,verbose=FALSE,write=FALSE)
	   TS1<- ncvar_get(fid.c,"ts")
	   TS1<- TS1+0.076
	   YR1<- TT[i]: (TT[i]+length(TS1)-1)
	   ind1<- which(TS1>=1.5)[1]     #index of threshold year
	   THR<- append(THR,YR1[ind1])
	   TEM<- append(TEM, TS1[ind1])
	   }
}

cbind(AA1,THR,TEM)

##~~~~Plot GCM ts~~~~
X11()
plot(0,type="n", ylim=c(-2,15),xlim=c(1850,2400),ylab="", xlab="year")

	for(i in 1:length(A1)){
		   M1<- eval(parse(text=paste("A1_",i,sep="")))      #read files within each folder
	for(j in 1:length(M1)){
		   CC<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/ann_gm.ts/",A1[i],"/",M1[j],".nc",sep="")
	         fid.c<- nc_open(CC,verbose=FALSE,write=FALSE)
		   TS1<- ncvar_get(fid.c,"ts")
	lines(ts(TS1,start=c(TT[i],1),frequency=1))
}
}

##~~~~Plot GCM ts upto threshold years assuming median pre-industrial correction,
##~~~~it requires storing ts in a matrix i.e. TS5

TS5<- matrix(NA, nrow=547,ncol=1)
for(i in 1:length(A1)){
	   M1<- eval(parse(text=paste("A1_",i,sep=""))) #read files within each folder
for(j in 1:length(M1)){
	   CC<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/ann_gm.ts/",A1[i],"/",M1[j],".nc",sep="")
         fid.c<- nc_open(CC,verbose=FALSE,write=FALSE)
	   TS1<- ncvar_get(fid.c,"ts")
	   TS1<- TS1+0.076
	   nc_close(fid.c)
	   TS_temp<- rep(NA,547)
	   TS_temp[1:length(TS1)]<- TS1
	   TS5<- cbind(TS5,TS_temp)
	   }
}

TS6<- TS5[,-1]   #cbind(TT1,THR,TEM),starting year, threshold year and threshold temperature

	X11()
	plot(0,type="n", ylim=c(-1,2),xlim=c(1850,2100),ylab="", xlab="year")
	abline(h=1.5, col="gray")
	#abline(h=2, col="gray")
	for(i in 1:ncol(TS6)){
	if(!is.na(THR[i]))lines(ts(TS6[,i],start=c(TT1[i],1),end=c(THR[i],1),frequency=1))
	}

##sampled years
cbind(AA1,THR)	
#write.table(cbind(AA1,THR), "C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/sample_years_from_median.txt")
read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_chris/sample_years_from_median.txt")




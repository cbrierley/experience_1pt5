rm(list=ls())
library(sROC)
CLM_O<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/1850_1900_clm.txt")
clm_o<- apply(CLM_O,2, mean)

AVG_2016<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/2016_mon_area_avg.txt")
avg_2016<- apply(AVG_2016,2, mean)

##~~CDF
##A cumulative density function of the difference between a "pre-industrial" 1400-1800 baseline and 1850-1900.
T<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_andrew/cdf_All_Forcings.txt")

		X11()
		plot(T[,2])
		##it is read as for example F(-0.39)=P(X<= -0.39)= 0 & F(-0.09)=P(X<=-0.09)= 0.0006666667 so on
		##so F(-0.38)-F(-0.39)=P(-0.39<=X<=-0.38)=0 & 
		##F(-0.09)-F(-0.1)= P(-0.1<=X<=-0.09)= 0-0.0006666667=0.0006666667 so on

##~~Obtain samples having the CDF above
		##A probability density function of the difference between a "pre-industrial" 1400-1800 baseline and 1850-1900.
	pdf_a<- diff(T[,2])
	freq<- 100*pdf_a    #multiply with 10,002 instead of 10000 so that after rounding we have 10,000
		#sum(round(freq))
		#range(T[,1])

	##Frequency distribution
	breaks<- seq(min(T[,1]),max(T[,1]), by=0.01)
	Cl<- cbind(seq(breaks[1],breaks[length(breaks)-1],by=(T[,1][2]-T[,1][1])), seq(breaks[2],breaks[length(breaks)],by=(T[,1][2]-T[,1][1])))
	XX<- apply(Cl,1,mean)
	YY<- cbind(XX,round(freq))

##samples of the difference of climatology 1400-1800 to 1850-1900 
clm_a<- rep(XX,round(freq))   #median(clm_a)=0.075 as is the median of CDF above 

##Kernel density estimate and confidence interval of the empirical cdf above
x<- clm_a
x.CDF <- kCDF(x)
x.CDF
ll<- CI.CDF(x.CDF)$Fhat.lower
uu<- CI.CDF(x.CDF)$Fhat.upper
xhat<- x.CDF$x

		#X11()
		#par(mfrow=c(1,2))
		#plot(T[,2])
		#plot(x.CDF, alpha=0.05, main="Kernel estimate of distribution function")

set.seed(102)

rr<- 100
##Samples of cdf
	FFF<- matrix(0, nrow=length(ll), ncol=rr)
	for(i in 1:length(ll)){
		FFF[i,]<- sort(sample(runif(rr,ll[i],uu[i])))
	}
	X11()
	plot(sort(FFF[,1]), type="n")
	lines(sort(FFF[,1]))
	lines(sort(FFF[,rr]))

YY2<- avg_2016-clm_o

YY1<- numeric(0)

for(k in 1:rr){
pdf_a_sam<- diff(sort(FFF[,k]))
freq_sam<- pdf_a_sam*50000
		
	##Frequency distribution
	bks<- seq(min(xhat),max(xhat), by=(xhat[2]-xhat[1]))
	Cl_sam<- cbind(seq(bks[1],bks[length(bks)-1],by=(xhat[2]-xhat[1])), seq(bks[2],bks[length(bks)],by=(xhat[2]-xhat[1])))
	XX_sam<- apply(Cl_sam,1,mean)
	YY_sam<- cbind(XX_sam,round(freq_sam))
clm_a_sam<- rep(XX_sam,round(freq_sam))
YY1<- append(YY1, YY2[k]+ sample(clm_a_sam,rr, replace=FALSE) )
}  

median(YY1)
quantile(YY1, probs=0.025)
quantile(YY1, probs=0.975)

X11()
par(mfrow=c(2,2))
plot(T[,2], "Empirical CDF")
plot(x.CDF, alpha=0.05, main="Kernel estimate of distribution function")
hist(YY1, freq=FALSE,breaks=seq(0.8,1.725,by=0.025),xaxt="n",
col="blue", xlab=substitute(paste(~Delta,"T wrt preindustrial")), ylab="probability", 
main=substitute(paste(2016~Delta,"T wrt preindustrial")))
axis(1, at= seq(0.8,1.725, by=0.1))

##Anderson-Darling test of normality
##H0: No observable difference between data and normal distribution
##Ha: Clear observable difference between data and normal distribution
ad.test(YY1)

##QQ-plot
qqnorm(YY1)
qqline(YY1, col = "red")


sum((YY1>=1.5))/length(YY1) #0.0379

> median(YY1)
[1] 1.283429
> quantile(YY1, probs=0.025)
    2.5% 
1.076738 
> quantile(YY1, probs=0.975)
   97.5% 
1.523005 






##############################################################################################################################





for(i in 1:)

	##A probability density function of the difference between a "pre-industrial" 1400-1800 baseline and 1850-1900.
	pdf_a<- diff(T[,2])
	freq<- 100*pdf_a    #multiply with 10,002 instead of 10000 so that after rounding we have 10,000
		#sum(round(freq))
		#range(T[,1])

	##Frequency distribution
	breaks<- seq(min(T[,1]),max(T[,1]), by=0.01)
	Cl<- cbind(seq(breaks[1],breaks[length(breaks)-1],by=0.01), seq(breaks[2],breaks[length(breaks)],by=0.01))
	XX<- apply(Cl,1,mean)
	YY<- cbind(XX,round(freq))
	
##samples of the difference of climatology 1400-1800 to 1850-1900 
clm_a<- rep(XX,round(freq)) 

		#X11()
		#par(mfrow=c(1,2))
		#plot(T[,2])
		#hist(clm_a)
		#abline(v=median(clm_a), col="red")

##method-1
x<- clm_a
x.CDF <- kCDF(x)
x.CDF
ll<- CI.CDF(x.CDF)$Fhat.lower
uu<- CI.CDF(x.CDF)$Fhat.upper

X11()
par(mfrow=c(1,2))
plot(T[,2])
plot(x.CDF, alpha=0.05, main="Kernel estimate of distribution function")

	FFF<- matrix(0, nrow=length(ll), ncol=10000)
	##draw samples of CDF
	for(i in 1:length(ll)){
		FFF[i,]<- sort(sample(runif(10000,ll[i],uu[i])))
	}

X11()
plot(FFF[,sample(1:10000,size=1)])




CLM_O<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/1850_1900_clm.txt")
clm_o<- apply(CLM_O,2, mean)

AVG_2016<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/2016_mon_area_avg.txt")
avg_2016<- apply(AVG_2016,2, mean)


##method-2
n			<-	length(x) 			
FF			<-	ecdf(x)			
# then we calculate the confidence band
alpha		<-	0.05
eps 		<-	sqrt(log(2/alpha)/(2*n))
xx			<-	seq(min(x)-1,max(x)+1,length.out=1000)
ll			<-	pmax(FF(xx)-eps,0) 		# pmin and pmax do element wise min/max;  min and max would find the min/max of the entire vector
uu 			<-	pmin(FF(xx)+eps,1)
# then we plot everything
X11()
par(mfrow=c(1,2))
plot(T[,2])
plot(FF, cex=0.25)
lines(xx, ll, col="blue") 		# lines (also points) doesn't need the add=TRUE command
lines(xx, uu, col="blue")

FFF<- matrix(0, nrow=100, ncol=100)

##draw samples
	for(i in 1:100){
		FFF[i,]<- sort(sample(runif(100,ll[i],uu[i])))
	}

X11()
plot(sort(FFF[,1]), ylim=c(0,1))
for(j in 50:100){
lines(sort(FFF[,j]), col=j)
}


CLM_O<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/1850_1900_clm.txt")
clm_o<- apply(CLM_O,2, mean)

AVG_2016<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/2016_mon_area_avg.txt")
avg_2016<- apply(AVG_2016,2, mean)

YY<- avg_2016-clm_o+clm_a         ##used went sent before YY<- mean(avg_2016)-clm_o+clm_a

			median(YY)			#1.24874
			##5-95% range
			quantile(YY,0.05)		#1.07549
			quantile(YY,0.95)		#1.458502 

			#95% confidence interval
			quantile(YY,0.025)	#1.050423
			quantile(YY,0.975)	#1.495576

			#99% confidence interval
			quantile(YY, 0.005)     #0.9987
			quantile(YY,0.995)	#1.5623

			#empirical probability
			length(which(YY>1.5))/length(YY)

II<- seq(1:100)
ann_avg_lk<- read.table(paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/ann_avg_lk_fr_hdens",II[1],".txt", sep=""))

for(i in 2:length(II)){
HH<- read.table(paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/ann_avg_lk_fr_hdens",II[i],".txt", sep=""))
ann_avg_lk<- cbind(HH, ann_avg_lk)
}
med_ann_lk<- apply(ann_avg_lk,1,median)
X<- c(1:nrow(ann_avg_lk))

	#check
	#avg_15<- ann_avg_lk[15,]
	#YYY<- avg_15-clm_o+clm_a  

X11()
e<- paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/figures/fig_1.png",sep="")
png(e,width = 8.5, height = 6, units = 'in', res = 300)
	grid<- matrix(c(1,1,1,0,0,
                	    1,1,1,3,3,
		    	    1,1,1,3,3,
		    	    2,2,2,3,3,
		    	    2,2,2,3,3,
		    	    2,2,2,0,0),nrow=6, ncol=5, byrow=TRUE)
layout(grid)
par(mar=c(4,4,1,0), oma=c(0.5,0.5,0,0))  

##panel(a)
	CLM_HD<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/1961_1990_clm.txt")
	clm_hd<- apply(CLM_HD,2, mean)
	ann_hdclm<- ann_avg_lk- matrix(rep(clm_hd, each=nrow(ann_avg_lk)),ncol=10000)

	med_hdclm<- apply(ann_hdclm,1,median)
	LL_hdclm<- apply(ann_hdclm,1,function(x)quantile(x, prob=0.025))
	UL_hdclm<- apply(ann_hdclm,1,function(x)quantile(x, prob=0.975))

plot(X,med_hdclm, type='n',ylab=substitute(paste(~Delta,"T wrt 1961-90")),
	 xlab="", ylim=c(-1, 2),xaxt="n",yaxt="n")                          
	 axis(side = 1,at = seq(1,length(X), by=10), labels = seq(1850,2016, by=10),tck= -.02)
	 axis(side = 2,at = seq(-1,2,by=0.2))
	 abline(h=0, lty=2, col='gray')
	 #abline(h=1.5, col='red')
	 #abline(h=2, col='red')
polygon(c(X,rev(X)),c(LL_hdclm,rev(UL_hdclm)),col = "grey75", border = FALSE)
lines(X,med_hdclm, col="black")
legend(-7,2.1, bty='n',col= c("black","grey75"),c("Median","Uncertainty"),lty=c(1,1,1), lwd=c(1,4,4))

##panel(b)
	ann_preind<- ann_avg_lk- matrix(rep(clm_o, each=nrow(ann_avg_lk)),ncol=10000)+ matrix(rep(clm_a, each=nrow(ann_avg_lk)),ncol=10000)
	med_ann_preind<- apply(ann_preind,1,median)
	LL_ann_preind<- apply(ann_preind,1,function(x)quantile(x, prob=0.025))
	UL_ann_preind<- apply(ann_preind,1,function(x)quantile(x, prob=0.975))

plot(X,med_ann_preind, type='n',ylab=substitute(paste(~Delta,"T wrt preindustrial")),
	 xlab="Time series of global annual mean temperatures", 
       ylim=c(-1, 2),xaxt="n",yaxt="n")                          
	 axis(side = 1,at = seq(1,length(X), by=10), labels = seq(1850,2016, by=10),tck= -.02)
	 axis(side = 2,at = seq(-1,2,by=0.2))
	 abline(h=0, lty=2, col='gray')
	 abline(h=1.5, col='red')
	 abline(h=2, col='red')
polygon(c(X,rev(X)),c(LL_ann_preind,rev(UL_ann_preind)),col = "grey75", border = FALSE)
lines(X,med_ann_preind, col="black")

##panel (c)
hist(YY, freq=FALSE,breaks=seq(0.8,1.7,by=0.025),xaxt="n",
col="blue", xlab=substitute(paste(~Delta,"T wrt preindustrial")), ylab="probability", 
main=substitute(paste(2016~Delta,"T wrt preindustrial")))
axis(1, at= seq(0.8,1.7, by=0.1))
dev.off()

#####
f_name=paste("D:/parameters10from250.nc")
ncnew<- nc_open(f_name, write=TRUE)
FF<- ncvar_get(ncnew,"parameters")	

####################################################################################################################
CLM_O1<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/1850_1879_clm.txt")
clm_o1<- apply(CLM_O1,2, mean)

median(clm_o1)  # -0.3363941 (-0.5096046, -0.1876418); sd=0.09856535
median(clm_o)   # -0.3405671 (-0.4811815, -0.2181678); sd=0.08019556 
median(clm_a)   #  0.075  (-0.025 , 0.215), sd= 0.07621684

X11()
par(mfrow=c(2,2))
hist(clm_o1, freq=FALSE, main= "1850-1879")
hist(clm_o, freq=FALSE, main= "1850-1900")
hist(clm_a, freq=FALSE, main= "difference 1400-1800 & 1850-1900 ")

clm_o-clm_o1

II<- seq(1:100)
ann_avg_lk<- read.table(paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/ann_avg_lk_fr_hdens",II[1],".txt", sep=""))

for(i in 2:length(II)){
HH<- read.table(paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/ann_avg_lk_fr_hdens",II[i],".txt", sep=""))
ann_avg_lk<- cbind(HH, ann_avg_lk)
}

med_ann_avg_lk<- as.vector(apply(ann_avg_lk,1,median))


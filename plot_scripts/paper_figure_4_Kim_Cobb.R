rm(list=ls())
library(ncdf4)
library(fields)

##HadCRUT4 median ensemble member
      a1<- "D:/HadCRUT.4.5.0.0.median.nc" #downloaded 16th Feb. 2017
     	fid.t<- nc_open(a1,verbose=TRUE,write=FALSE)
	time.t<- ncvar_get(fid.t,'time')     
	origin.t <- fid.t$dim$time$units
	tunits.t <- strsplit(fid.t$dim$time$units,split=" ") 
	if(identical(tunits.t[[1]][1],"minutes")) tunits.t[[1]][1]<-"mins"
	TIME.t<- strptime(paste(tunits.t[[1]][3],tunits.t[[1]][4]),"%Y-%m-%d %H:%M:%S",tz="GMT")+
                           as.numeric(as.difftime(time.t,units=tunits.t[[1]][1]),units="secs")
	
	lat.t <- ncvar_get(fid.t,'latitude');lat.t                 		#length(lat.t)         #36
	lon.t <- ncvar_get(fid.t,'longitude'); lon.t               		#length(lon.t)         #72
	tempr.a <- ncvar_get(fid.t,'temperature_anomaly')          		#dim(tempr) 

n<- 2004    # length(TIME.t)
TIME.t<- TIME.t[1:n]
date_start<- format(TIME.t[1], "%B-%Y")   #"January-1850"             
date_end<- format(TIME.t[n], "%B-%Y")     #"December-2016" 

n.ens<- 100
						
hd_ind<- (12*trunc(length(TIME.t)/12))     ##leave months at the end if not a full year, 
                                           ##for annual averages
S_y<- as.numeric(format(TIME.t[1],'%Y'))
E_y<-  as.numeric(format(TIME.t[n],'%Y'))

sst_lk<- read.table("D:/files_18feb/raw_sst_nin_3.4_2decdata.txt")
med_sst_lk<- apply(sst_lk,1,median)				         #median of oni over 100 LK ensembles
LB_sst_lk<- apply(sst_lk,1,function(x)quantile(x,probs=0.025)) #lower 5% of oni over 100 LK ensembles
UB_sst_lk<- apply(sst_lk,1,function(x)quantile(x,probs=0.975)) #upper 95% of oni over 100 LK ensembles

#over lk ensembles
sp_cl<- read.table("C:/Users/maryam/Dropbox/Maryam/data_r_files_bitbucket/lk_enso_ev.txt")

X<- 1:n

#label every fifth year
ind.lab<- seq(1,167, by=5)
full.lab<- seq(S_y,E_y, by=1)
lab.temp<- rep(NA,167)
lab.temp[ind.lab]<- full.lab[ind.lab]
lab<- lab.temp


X11()
par(op)
e<-  paste("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper_r_files/figs/fig4_kc.png",sep="")
png(e,width = 13, height = 4, units = 'in', res = 300)
par(oma=c(1,0.5,0,0),mar=c(2.4,4,1,1))

plot(X,med_sst_lk, type='l', ylab="Ni�o 3.4 SST anomaly", xlab="", 
       ylim=c(-4.5, 3.5),xaxt="n",yaxt="n")                          
axis(side = 1,las=2,at = seq(1,length(X), by=12), 
          labels = lab.temp)                          #seq(S_y,E_y, by=1))		 #,tck= -.02)
axis(side = 2,at = seq(-4,3, by=0.5))

	abline(h=-0.5,col='blue', lty=2)
	abline(h=0.5, col='red', lty=2)
	abline(h=0, lty=1)

polygon(c(X,rev(X)),c(LB_sst_lk,rev(UB_sst_lk)),col = "grey75", border = FALSE)
lines(X,med_sst_lk)

	df_ind1<- data.frame(X,format(TIME.t,"%b-%Y"),med_sst_lk)[seq(1,n,by=12),]
	DF_ind1<- df_ind1[-1,]
	CH1_ev<- data.frame(DF_ind1,sp_cl[,2])

##Add events 	
points(CH1_ev[,1][which(CH1_ev[,4]=="El Ni�o")],CH1_ev[,3][which(CH1_ev[,4]=="El Ni�o")], col="red", pch=17, cex=0.8)
points(CH1_ev[,1][which(CH1_ev[,4]=="La Ni�a")],CH1_ev[,3][which(CH1_ev[,4]=="La Ni�a")], col="blue", pch=17, cex=0.8)
points(CH1_ev[,1][which(CH1_ev[,4]=="Neutral")],CH1_ev[,3][which(CH1_ev[,4]=="Neutral")], col="green", pch=17, cex=0.8)
points(CH1_ev[,1][which(CH1_ev[,4]=="Transit")],CH1_ev[,3][which(CH1_ev[,4]=="Transit")], col="cyan", pch=17, cex=0.8)
points(CH1_ev[,1][which(CH1_ev[,4]=="Ambiguous")],CH1_ev[,3][which(CH1_ev[,4]=="Ambiguous")], col="pink", pch=17, cex=0.8)
legend("topleft", bty='n',col= c("red","blue", "green","pink","cyan"), c("E","L","D","A","R"), pch=17, cex=1, horiz=TRUE)
dev.off()


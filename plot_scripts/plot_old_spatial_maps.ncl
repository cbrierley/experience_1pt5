; Creates a figure of spatial maps
;

Ilyas_2016_filename = "/data/aod/Ilyasetal2017_annual/2016.nc"
pct_1pt5_filename = "/data/aod/pct_exceedence_oneptfive_climate.nc"
pct_preind_filename = "/data/aod/pct_exceedence_preind.nc"
preind_filename = "/data/aod/cmip5_processed_files/ensemble_mean/preind_climate.nc"
oneptfive_filename = "/data/aod/cmip5_processed_files/ensemble_mean/oneptfive_climate.nc"
popn_filename = "/home/ucfaako/DATA/popc_05x05.nc"

;load data
oneptfivefil=addfile(oneptfive_filename,"r")
temp15=oneptfivefil->ts
pifil=addfile(preind_filename,"r")
pi=pifil->ts
obsfil=addfile(Ilyas_2016_filename,"r")
lat=obsfil->latitude
lon=obsfil->longitude
all_2016_4D=obsfil->temperature_anomaly
all_2016=reshape(all_2016_4D,(/10000,36,72/))
avg_2016=temp15
avg_2016=(/dim_avg_n(all_2016,0)/)
 
pctfil=addfile(pct_1pt5_filename,"r")
pct=temp15
pct_raw=pctfil->pct(166,:,:)
pct=pct_raw*100.;convert to percentage
pctpifil=addfile(pct_preind_filename,"r")
pctpi=temp15
pctpi_raw=pctpifil->pct(166,:,:)
pctpi=pctpi_raw*100.;convert to percentage
popnfil=addfile(popn_filename,"r")
popn=popnfil->population(165,:,:)
popn=popn/1e6

;Define the resourses
res                          = True       ; plot mods desired
res@gsnDraw                  = False      ; don't draw yet
res@gsnFrame                 = False      ; don't advance frame yet
res@cnFillOn                 = True       ; turn on color fill
res@cnLinesOn                = False      ; turn off contour lines
res@cnLineLabelsOn           = False      ; turns off contour line labels
res@cnFillMode = "RasterFill"
res@lbLabelBarOn             = True      ; turns off contour info label
res@lbTitleOn        = True                  ; turn on title
res@lbTitleFontHeightF   = 0.01                              ; font height

;select colors
res@cnFillPalette            = "BlueWhiteOrangeRed"
res@gsnSpreadColors          = True       ; use full colormap
res@gsnSpreadColorEnd = 253

res@mpProjection="WinkelTripel"
res@vpYF = 0.95
res@vpHeightF = 0.3
res@vpXF = 0.2
res@vpWidthF = 0.6
res@gsnLeftStringOrthogonalPosF = -0.05
res@gsnLeftStringParallelPosF = .005
res@gsnRightStringOrthogonalPosF = -0.05
res@gsnRightStringParallelPosF = 0.96
res@gsnLeftStringFontHeightF = 0.014
res@gsnRightStringFontHeightF = 0.014
res@mpGeophysicalLineColor = "gray42"
res@mpPerimOn    = False
res@mpGridLatSpacingF =  15.            ; change latitude  line spacing
res@mpGridLonSpacingF = 15.           ; change longitude line spacing
res@mpGridLineColor   = "transparent"  ; trick ncl into drawing perimeter
res@mpGridAndLimbOn   = True           ; turn on lat/lon lines  

res@cnLevelSelectionMode="ExplicitLevels"
res@gsnLeftString = " "
res@gsnRightString  = " "

;Open the pdf
wks = gsn_open_wks("pdf","plot_spatial_maps")

;Create the panels
res@cnLevels=(/-5,-4,-3,-2,-1.5,-1.,-0.75,-0.5,-.25,0.25,0.5,0.75,1.,1.5,2,3,4,5/)
res@lbTitleString    = "Temperature w.r.t. 1961-1990 (K)"
res@tiMainString = "Annual Temperature in 2016"
plot_2016 = gsn_csm_contour_map(wks,avg_2016,res)  
res@tiMainString = "1.5oC above preindustrial"
plot_1pt5 = gsn_csm_contour_map(wks,temp15,res)
res@tiMainString = "preindustrial"
plot_pi = gsn_csm_contour_map(wks,pi,res)
delete(res@cnLevels)
res@cnFillPalette  = "MPL_BuPu"
res@gsnSpreadColorEnd = 126
res@cnLevels=(/0,1,2.5,5,10,25,50,100,500/)
res@lbTitleString    = "Population in grid box (millions, HYDE 3.2)"
res@tiMainString = "Population in 2016"
plot_popn = gsn_csm_contour_map(wks,popn,res)  
res@cnFillPalette  = "MPL_BuGn"
delete(res@cnLevels)
res@cnLevels=(/0,5,10,33,50,66,90,95,100/)
res@tiMainString = "1.5oC Probability in 2016"
res@lbTitleString    = "Percentage of Ilyas et al (2017) ensemble members"
plot_pct = gsn_csm_contour_map(wks,pct,res)  
res@tiMainString = "Preindustrial Prob. in 2016"
plot_pct_pi = gsn_csm_contour_map(wks,pctpi,res)  

;Plot the panels
resP    = True         ; modify the panel plot
resP@gsnPanelYWhiteSpacePercent = 1
resP@gsnMaximize = True
resP@gsnPanelRowSpec = True
gsn_panel(wks,(/plot_2016,plot_1pt5,plot_pi,plot_popn,plot_pct,plot_pct_pi/),(/3,3/),resP)

;close the PDF
delete(wks)

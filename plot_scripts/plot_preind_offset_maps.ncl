; Creates a figure of showing the difference between the preindustrial and 1850-1900
; It first creates all the required data fields...
; And only then will it plot them

; Create median PI offset and stddev of PI offset
patt_filepath="/data/aod/Ilyasetal2017_annual/scalable_patterns.r1i1p1_ONLY.nc"
patt_fil=addfile(patt_filepath,"r")
scalable_patterns=patt_fil->scalable_patterns(:,0,:,:) ;(/10000,1,36,72/) but loose time dimension
gmt_offset_wrt18501900=patt_fil->gmt_offset_wrt18501990
scaled_patterns=scalable_patterns*conform(scalable_patterns,tofloat(gmt_offset_wrt18501900),0)
foo=dim_pqsort_n(scaled_patterns,2,0);the 2 means that the array is now also sorted
delete(foo)
grid=scalable_patterns(0,:,:)
grid&latitude@units="degrees_north"
grid&longitude@units="degrees_east"
pi_offset_median=grid
pi_offset_stddev=grid
pi_likely_range=grid
pi_offset_median=dim_median_n(scaled_patterns,0)
pi_offset_stddev=dim_stddev_n(scaled_patterns,0)
pi_likely_range=(/dim_avg_n(scaled_patterns(8333:8334,:,:),0)-dim_avg_n(scaled_patterns(1666:1667,:,:),0)/)
pi_likely_range_error=100.*(pi_likely_range-1.934*pi_offset_stddev)/pi_offset_stddev
print("Proportion of datapoints where estimating the likely range from 1.93*stddev is more than a 1% error is "+num(abs(pi_likely_range_error).gt.1)/(36*72.))
; Look at the uncertainty from lack of early coverage
base_dir="/data/aod/Ilyasetal2017_annual/"
uncert_18501900_filename=base_dir+"avg.18501900.wrt_19611990.nc"
if .not.isfilepresent(uncert_18501900_filename) then
  print("You need to have run experience_1pt5/calculate_interannual_variability.sh")
end if
ei_fil=addfile(uncert_18501900_filename,"r")
ei=ei_fil->temperature_anomaly
ei_sort_order=dim_pqsort_n(ei,2,0);the 2 means that the array is now also sorted
ei_likely_range=grid
ei_likely_range=(/dim_avg_n(ei(8332:8333,:,:),0)-dim_avg_n(ei(1665:1666,:,:),0)/)
; Look at the internal variability in the current climate
variability_filename=base_dir+"wrt_preind/std.1986-2015.ann_ensmem.wrt_preind.nc"
if .not.isfilepresent(variability_filename) then
  print("You need to have run experience_1pt5/calculate_interannual_variability.sh")
end if
var_fil=addfile(variability_filename,"r")
var=var_fil->temperature_anomaly(:,0,:,:)
var_doubled=grid
var_doubled=2*var
; Look at how much colder the preindustrail was from the current climate
baseline_filename=base_dir+"wrt_preind/"+"baseline_offset_from_1986-2015.wrt_preind.nc"
filename_2016=base_dir+"wrt_preind/"+"5yravg.2014-2018.wrt_preind.nc"
outfile_2016=base_dir+"wrt_preind/"+"median.2014-2018.wrt_preind.nc"
if .not.isfilepresent(baseline_filename) then
  system("nces -O "+base_dir+"wrt_preind/"+"198[6789].ann*.nc "+base_dir+"wrt_preind/"+"199?.ann*.nc "+base_dir+"wrt_preind/"+"200?.ann*.nc "+\
  base_dir+"wrt_preind/"+"201[012345].ann*.nc "+baseline_filename)
  system("ncatted -a units,latitude,o,c,degrees_north -a units,longitude,o,c,degrees_east "+baseline_filename) 
end if
now_fil=addfile(baseline_filename,"r")
now=now_fil->temperature_anomaly(:,0,:,:)
now_median=grid;to get metadata
now_median=(/-dim_median_n(now,0)/)
now_sort_order=dim_pqsort_n(now,2,0);the 2 means that the array is now also sorted
now_likely=grid;for Meta
now_likely=(/dim_avg_n(now(8332:8333,:,:),0)-dim_avg_n(now(1665:1666,:,:),0)/)
; Look at how much warmer than preindustrial the past 5 years were
if .not.isfilepresent(filename_2016) then
  print("for fil in "+base_dir+"wrt_preind/"+"201[45678].ann*.nc; do ncatted -a _FillValue,temperature_anomaly,o,f,9.96921e+36 $fil; done")
  system("for fil in "+base_dir+"wrt_preind/"+"201[45678].ann*.nc; do ncatted -a _FillValue,temperature_anomaly,o,f,9.96921e+36 $fil; done")
  print("nces -O "+base_dir+"wrt_preind/"+"201[45678].ann*.nc "+filename_2016)
  system("nces -O "+base_dir+"wrt_preind/"+"201[45678].ann*.nc "+filename_2016)
  print("ncatted -a units,latitude,o,c,degrees_north -a units,longitude,o,c,degrees_east "+filename_2016) 
  system("ncatted -a units,latitude,o,c,degrees_north -a units,longitude,o,c,degrees_east "+filename_2016) 
end if
fil_2016=addfile(filename_2016,"r")  
warmest_yr=fil_2016->temperature_anomaly(:,0,:,:)
yr2016=grid
yr2016=(/dim_median_n(warmest_yr,0)/)
yr2016_prob_gt_1pt5=grid
yr2016_prob_gt_1pt5=dim_num_n(warmest_yr.gt.1.5,0)/10000.

;;;Now start to get ready for the plotting
;Define the resourses
res                          = True       ; plot mods desired
res@gsnDraw                  = False      ; don't draw yet
res@gsnFrame                 = False      ; don't advance frame yet
res@cnFillOn                 = True       ; turn on color fill
res@cnLinesOn                = False      ; turn off contour lines
res@cnLineLabelsOn           = False      ; turns off contour line labels
res@cnFillMode = "CellFill"
res@lbLabelBarOn             = True      ; turns off contour info label
res@lbTitleOn        = True                  ; turn on title
res@lbTitleFontHeightF   = 0.01                              ; font height


res@mpProjection="WinkelTripel"
res@vpYF = 0.95
res@vpHeightF = 0.3
res@vpXF = 0.2
res@vpWidthF = 0.6
res@gsnLeftStringOrthogonalPosF = -0.05
res@gsnLeftStringParallelPosF = .005
res@gsnRightStringOrthogonalPosF = -0.05
res@gsnRightStringParallelPosF = 0.96
res@gsnLeftStringFontHeightF = 0.014
res@gsnRightStringFontHeightF = 0.014
res@mpGeophysicalLineColor = "gray42"
res@mpPerimOn    = False
res@mpGridLatSpacingF =  15.            ; change latitude  line spacing
res@mpGridLonSpacingF = 15.           ; change longitude line spacing
res@mpGridLineColor   = "transparent"  ; trick ncl into drawing perimeter
res@mpGridAndLimbOn   = True           ; turn on lat/lon lines  
res@mpGeophysicalLineColor="black"
res@gsnLeftString = " "
res@gsnRightString  = " "
res@cnLevelSelectionMode="ExplicitLevels"
res@lbLabelStride=2
uncert_res=res

;select colors
levels=fspan(-1.5, 2.5, 17) 
colors = (/ (/8,69,148/),\ ;<-1.5
       	 (/33,113,181/),\ ;CBr, 8 Blue (skip whitest)
	     (/66,146,198/),\
	     (/107,174,214/),\
	     (/158,202,225/),\
	     (/198,219,239/),\
	     (/239,243,255/),\;>0
	     (/255,255,200/),\; lightest yellow taken from amwg
	     (/252,252,  0/),\; yellow-red side of nrl_sirkes
	     (/252,224,  0/),\
	     (/252,191,  0/),\
	     (/252,161,  0/),\
	     (/252,128,  0/),\
	     (/252, 97,  0/),\
	     (/252, 64,  0/),\
	     (/252, 33,  0/),\
	     (/191,  0,  0/),\
	     (/128,  0,  0/)/) / 255.   ; be sure to make this a float!

res_over=True
res_over@cnFillOn=False
res_over@cnLinesOn=True
res_over@cnLineLabelsOn=True
res_over@cnLevelSelectionMode="ExplicitLevels"
res_over@cnLevels=(/0.25,0.5,0.75,1.,1.25,1.5/)
res_over@cnMonoLineColor=True
res_over@cnLineColor="forestgreen"

;Open the pdf
wks = gsn_open_wks("pdf","~/Documents/local_repos/experience_1pt5/plots/plot_preind_offset_maps")

;Create the temperature panels
uncert_res@lbTitleString  = "Median pattern-scaled offset (K)"
uncert_res@tiMainString = "1850-1900 vs preindustrial"
uncert_res@cnLevels=fspan(-0.2, 0.2, 21)
uncert_res@cnFillPalette="BlueWhiteOrangeRed"
plot_offset = gsn_csm_contour_map(wks, pi_offset_median, uncert_res)
delete(uncert_res@cnLevels)

res@cnFillPalette = colors
res@cnSpanFillPalette = True       ; use full colormapres@cnLevelSelectionMode="ExplicitLevels"
res@lbTitleString    = "Temperature change from 1986-2015 (K)"
res@tiMainString = "Preindustrial vs current climate"
res@cnLevels=levels
plot_now = gsn_csm_contour_map_overlay(wks, now_median, now_likely, res, res_over)

shading=True
shading@cnFillOn=True
shading@cnLinesOn=False
shading@cnLineLabelsOn=False
shading@cnLevelSelectionMode="ExplicitLevels"
shading@cnLevels=(/0.05,0.95/)
shading@cnFillPatterns=(/4,0,17/)
shading@cnMonoFillPattern=False
shading@cnFillColors=(/"black","transparent","black"/)
res@tiMainString = "Average Temperature 2014-2018 (median)"
res@lbTitleString    = "Temperature change from preindustrial (K)"
plot_2016 = gsn_csm_contour_map_overlay(wks,yr2016,yr2016_prob_gt_1pt5,res,shading)  

;Plot the error bars
uncert_res@cnFillPalette = "MPL_YlGn"
uncert_res@cnLevels=fspan(0.1, 2., 20)
uncert_res@lbTitleString    = "Likely Range (K)"
uncert_res@tiMainString = "Uncertainty in preindustrial offset"
plot_uncert = gsn_csm_contour_map(wks, pi_likely_range, uncert_res)
uncert_res@lbTitleString    = "Likely Range (K)"
uncert_res@tiMainString = "Uncertainty in early industrial observations"
plot_ei = gsn_csm_contour_map(wks, ei_likely_range, uncert_res)
uncert_res@lbTitleString    = "Two standard deviations (K)"
uncert_res@tiMainString = "Interannual Variability"
plot_var = gsn_csm_contour_map(wks, var_doubled, uncert_res)

;Plot the panels
resP    = True         ; modify the panel plot
resP@gsnPanelYWhiteSpacePercent = 1
resP@gsnMaximize = True
resP@gsnPanelRowSpec = True
resP@gsnPanelFigureStrings = (/"A","B","C","D","E","F"/)
resP@amJust="TopLeft"
gsn_panel(wks,(/plot_offset,plot_uncert,plot_ei,plot_var,plot_now,plot_2016/),(/2,2,2/),resP)

;close the PDF
delete(wks)

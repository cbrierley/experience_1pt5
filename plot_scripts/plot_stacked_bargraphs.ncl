; Creates some colored lines graphs of area or population
; Calculates the median properly, by cycling through all the years/ensemble members

EI_not_PI=True

out_dir="~/Documents/local_repos/experience_1pt5/plots/"
ENS_STATS_NOT_FULL=True; calculates from the ensemble statistics file, rather than the full ensemble
PLOT_AREA=True
PLOT_POPN=True
LAND_NOT_GLOBAL_AREA=False

if EI_not_PI
  baseline_str="wrt_18501900"
  data_dir="/data/aod/Ilyasetal2017_annual/"+baseline_str+"/"
  baseline_string="early industrial"
else
  baseline_str="wrt_preind"
  data_dir="/data/aod/Ilyasetal2017_annual/"+baseline_str+"/"
  baseline_string="preindustrial"
end if

;might as well collect all data simultaneously whilst looping
if isfilepresent(data_dir+"stacked_bargraph_data.nc") then
  input_fil=addfile(data_dir+"stacked_bargraph_data.nc","r")
  popn_temp=input_fil->popn_temp
  area_temp=input_fil->area_temp
  colors=input_fil->colors
else
  ;create the data
  ;determine contour levels
  levels=fspan(-1.5, 2.5, 17) 
  colors = (/ (/8,69,148/),\ ;<-1.5
        	 (/33,113,181/),\ ;CBr, 8 Blue (skip whitest)
	         (/66,146,198/),\
	         (/107,174,214/),\
	         (/158,202,225/),\
	         (/198,219,239/),\
	         (/239,243,255/),\;>0
	         (/255,255,200/),\; lightest yellow taken from amwg
	         (/252,252,  0/),\; yellow-red side of nrl_sirkes
	         (/252,224,  0/),\
	         (/252,191,  0/),\
	         (/252,161,  0/),\
	         (/252,128,  0/),\
	         (/252, 97,  0/),\
	         (/252, 64,  0/),\
	         (/252, 33,  0/),\
	         (/191,  0,  0/),\
	         (/128,  0,  0/)/) / 255.   ; be sure to make this a float!
  n_temp=dimsizes(levels)
  temp_anom=new(n_temp+1,typeof(levels))
  temp_pm=0.5*(levels(1)-levels(0))
  temp_anom(0:n_temp-1)=(/levels-temp_pm/)
  temp_anom(n_temp)=(/max(levels)+temp_pm/)
  temp_anom@long_name="Annual mean anomaly w.r.t. "+baseline_string
  temp_anom@units="oC"

  ;Load in the population data
  ;CURRENLTY THE POPULATION FILE ONE GOES TO 2016. IT NEEDS TO EXTEND TO 2018 FOR THIS SCRIPT TO WORK
  popn_file=addfile("/data/aod/obs/HYDE32/total_population_HYDE32_5x5o_1850-2018.nc","r"); 
  popn=popn_file->population(:,::-1,:) 
  popn=where(ismissing(popn),0,popn);using as wieghts, so want 0 weight, rather than ignored
  urban_popn_file=addfile("/data/aod/obs/HYDE32/urban_population_HYDE32_5x5o_1850-2018.nc","r"); 
  urban_popn=urban_popn_file->urbanpopulation(:,::-1,:)
  rural_popn_file=addfile("/data/aod/obs/HYDE32/rural_population_HYDE32_5x5o_1850-2018.nc","r"); 
  rural_popn=rural_popn_file->ruralpopulation(:,::-1,:)
  time=popn&time
  time=time-1;correct back to 1850 start

  ;read in the urban heat island
  urban_heat_island=popn(0,:,:);just take a grid to fill 
  daytime=readAsciiTable("/data/aod/obs/Urban_Heat_Island/uhi_D_T_DIFF.asc",72,"float",6) 
  nighttime=readAsciiTable("/data/aod/obs/Urban_Heat_Island/uhi_N_T_DIFF.asc",72,"float",6)
  urban_heat_island=(/dim_avg_n((/daytime,nighttime/),0)/)
  urban_heat_island=where(urban_heat_island.lt.-10.,0,urban_heat_island);deal with missing data.
  urban_heat_island=(/urban_heat_island(::-1,:)/)
  UHI=conform(urban_popn,urban_heat_island,(/1,2/))

  ;setup some holding variables
  popn_temp=popn(:,0:n_temp,0);to copy Metadata
  popn_temp@long_name="Population at temperature (oC from "+baseline_string+")" 
  popn_temp!1="temp_anom"
  popn_temp&temp_anom=temp_anom
  popn_temp!0="time"
  popn_temp&time=time
  area_temp=popn_temp
  area_temp@long_name="Area at temperature (oC from "+baseline_string+")" 

  ;Calculate the area of the globe
  area=popn(0,:,:)
  lats=conform_dims((/36,72,4/),popn&latitude,0)
  lons=conform_dims((/36,72,4/),popn&longitude,1)
  lats=lats+2.5
  lats(:,:,1:2)=lats(:,:,1:2)-5
  lons(:,:,0:1)=lons(:,:,0:1)-2.5
  lons(:,:,2:3)=lons(:,:,2:3)+2.5
  area=(/tofloat(gc_qarea(lats,lons))/)
  if LAND_NOT_GLOBAL_AREA then
    d = addfile("$NCARG_ROOT/lib/ncarg/data/cdf/landsea.nc","r")
    basemap = d->LSMASK ;load in a landsea mask as 1x1 grid 
    basemap=lonFlip(basemap)
    landmask = tofloat(basemap)
    landmask=where(landmask.gt.1,0,landmask)
    landfrac=grid(0,:,:)
    landfrac=(/area_hi2lores(basemap&lon,basemap&lat,landmask,True,1,landfrac&longitude,landfrac&latitude,False)/)
    area=area*landfrac*100/(sum(landfrac*area));convert to area to % globe
  else
    area=area*100/(4*4.*atan(1.));convert to area to % globe
  end if

  if ENS_STATS_NOT_FULL then
    ;compute percentages w.r.t. the median temperature
    es_fil=addfile(data_dir+"/ensemble_statistics.ann_"+baseline_str+".nc","r")
    median=es_fil->median;read in a lose singleton time dimension
    median_wUHI=median
    median_wUHI=(/median+UHI/)
    ;look at area
    big_area=conform(median,area,(/1,2/))
    area_temp(:,0)=(/dim_sum_n(where(median.le.temp_anom(0),big_area,0),(/1,2/))/)
    do j=1,n_temp-1
      area_temp(:,j)=(/dim_sum_n(where((median.le.temp_anom(j)).and.\
                     (median.gt.temp_anom(j-1)),big_area,0),(/1,2/))/)
    end do
    area_temp(:,n_temp)=(/dim_sum_n(where(median.gt.temp_anom(n_temp-1),big_area,0),(/1,2/))/)
    ;look at population
    popn_temp(:,0)=(/dim_sum_n(where(median.le.temp_anom(0),rural_popn,0),(/1,2/))+\
                     dim_sum_n(where((median_wUHI).le.temp_anom(0),urban_popn,0),(/1,2/))/)
    do j=1,n_temp-1
      popn_temp(:,j)=(/dim_sum_n(where((median.le.temp_anom(j)).and.(median.gt.temp_anom(j-1)),rural_popn,0),(/1,2/))+\
        dim_sum_n(where((median_wUHI.le.temp_anom(j)).and.(median_wUHI.gt.temp_anom(j-1)),urban_popn,0),(/1,2/))/)
    end do
    popn_temp(:,n_temp)=(/dim_sum_n(where(median.gt.temp_anom(n_temp-1),rural_popn,0),(/1,2/))+\
      dim_sum_n(where(median_wUHI.gt.temp_anom(n_temp-1),urban_popn,0),(/1,2/))/)
  else
    ;compute median percentages w.r.t. the temperature
    ;NOTE: This is loop is rather slow. Expect it to take 3 hours
    do i=0,168  
      if (i%10.eq.0) then
        print("In loop: working on year "+(1850+i))
      end if
      this_yr_fil=addfile(data_dir+"/"+(1850+i)+".ann_ensmem."+baseline_str+".nc","r")
      this_yr=this_yr_fil->temperature_anomaly(:,0,:,:);read in a lose singleton time dimension
      ;look at population
      this_popn=conform(this_yr,popn(i,:,:),(/1,2/))
      popn_temp(i,0)=(/dim_median(dim_sum_n(where(this_yr.le.temp_anom(0),this_popn,0),(/1,2/)))/)
      do j=1,n_temp-1
        popn_temp(i,j)=(/dim_median(dim_sum_n(where((this_yr.le.temp_anom(j)).and.\
              (this_yr.gt.temp_anom(j-1)),this_popn,0),(/1,2/)))/)
      end do
      popn_temp(i,n_temp)=(/dim_median(dim_sum_n(where(this_yr.gt.temp_anom(n_temp-1),this_popn,0),(/1,2/)))/)
      ;look at area
      big_area=conform(this_yr,area,(/1,2/))
      area_temp(i,0)=(/dim_median(dim_sum_n(where(this_yr.le.temp_anom(0),big_area,0),(/1,2/)))/)
      do j=1,n_temp-1
        area_temp(i,j)=(/dim_median(dim_sum_n(where((this_yr.le.temp_anom(j)).and.\
              (this_yr.gt.temp_anom(j-1)),big_area,0),(/1,2/)))/)
      end do
      area_temp(i,n_temp)=(/dim_median(dim_sum_n(where(this_yr.gt.temp_anom(n_temp-1),big_area,0),(/1,2/)))/)
    end do
  end if
  ;write out data 
  system("rm "+data_dir+"stacked_bargraph_data.nc")
  input_fil=addfile(data_dir+"stacked_bargraph_data.nc","c")
  input_fil->popn_temp=popn_temp
  input_fil->area_temp=area_temp
  input_fil->colors=colors
  input_fil->levels=levels
end if

if PLOT_AREA then
  if LAND_NOT_GLOBAL_AREA then
    wks = gsn_open_wks("pdf","plots/landarea_stacked_bargraph_"+baseline_str)   ; send graphics to PNG file
  else
    wks = gsn_open_wks("pdf","plots/area_stacked_bargraph_"+baseline_str)   ; send graphics to PNG file
  end if
  res                   = True     ; Plot options desired
  res@gsnMaximize       = True     ; Maximize plot in frame
  res@xyMonoDashPattern = True     ; Solid lines for all curves
  res@gsnXYFillColors = colors
  res@trYMaxF=100
  res@tiYAxisString = "Area of Globe (%)"
  res@tiXAxisString = "Year"
  res@vpHeightF= 0.4                    ; change aspect ratio of plot
  res@vpWidthF = 0.8                  
  LineColors=new(dimsizes(colors(:,0))+1,string)
  LineColors=(/"Transparent"/)
  LineColors(min(ind(popn_temp&temp_anom.gt.0)))="Black"
  LineColors(min(ind(popn_temp&temp_anom.gt.1.5)))="Black"  
  res@xyLineColors = LineColors
  DashPatterns=new(dimsizes(colors(:,0))+1,integer)
  DashPatterns=(/0/)
  DashPatterns(min(ind(popn_temp&temp_anom.gt.1.5)))=(/2/)
  res@xyMonoDashPattern=False
  res@xyDashPatterns=DashPatterns
  year=area_temp&time+1850
  res@trXMinF=min(year)
  res@trXMaxF=max(year)
  area_for_plotting=new((/dimsizes(area_temp&temp_anom)+1,dimsizes(area_temp&time)/),typeof(area_temp))
  area_for_plotting(0,:)=(/0./);need to add in a extra line at the bottom of the plot to color between
  total_area=sum(area_temp(0,:))
  area_for_plotting(1:dimsizes(area_temp&temp_anom),:)=(/dim_cumsum_n(area_temp(temp_anom|:,time|:),2,0)/total_area*100./)
  ;remove rounding errors
  area_for_plotting=area_for_plotting*100/(conform(area_for_plotting,dim_sum_n(area_temp(temp_anom|:,time|:),0),1))
  xy = gsn_csm_xy(wks,year,area_for_plotting,res)    ; Draw the four curves
  delete([/wks,res,xy/])
end if

if PLOT_POPN then
  wks = gsn_open_wks("pdf","plots/population_stacked_bargraph_"+baseline_str)   ; send graphics to PNG file
  res                   = True     ; Plot options desired
  res@gsnMaximize       = True     ; Maximize plot in frame
  res@xyMonoDashPattern = True     ; Solid lines for all curves
  res@gsnXYFillColors = colors
  res@tiYAxisString = "Population (Billion)"
  res@tiXAxisString = "Year"
  res@vpHeightF= 0.4                    ; change aspect ratio of plot
  res@vpWidthF = 0.8                  
  LineColors=new(dimsizes(colors(:,0))+1,string)
  LineColors=(/"Transparent"/)
  LineColors(min(ind(popn_temp&temp_anom.gt.0)))="Black"
  LineColors(min(ind(popn_temp&temp_anom.gt.1.5)))="Black"  
  res@xyLineColors = LineColors
  DashPatterns=new(dimsizes(colors(:,0))+1,integer)
  DashPatterns=(/0/)
  DashPatterns(min(ind(popn_temp&temp_anom.gt.1.5)))=(/2/)
  res@xyMonoDashPattern=False
  res@xyDashPatterns=DashPatterns
  year=popn_temp&time+1850
  res@trXMinF=min(year)
  res@trXMaxF=max(year)
  popn_for_plotting=new((/dimsizes(popn_temp&temp_anom)+1,dimsizes(popn_temp&time)/),typeof(popn_temp))
  popn_for_plotting(0,:)=(/0./);need to add in a extra line at the bottom of the plot to color between
  popn_for_plotting(1:dimsizes(popn_temp&temp_anom),:)=(/(dim_cumsum_n(popn_temp(temp_anom|:,time|:),2,0))/1e9/)
  xy = gsn_csm_xy(wks,year,popn_for_plotting,res)    ; Draw the four curves
  delete([/wks,res,xy/])
end if

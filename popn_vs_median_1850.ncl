; Run this script with "ncl -n calculate_probability_population.ncl"
; This is a rather rough and ready approach, in that it makes use of neither the uncertainties nor the true preindustrial baseline.
;

load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/cd_string.ncl"
load "~/ncl/common.ncl"

CREATE_median_wrt18501900=True
POPN_WEIGHTED_TIMESERIES=False
POPN_ABOVE_TEMPS=False
AREA_ABOVE_TEMPS=False
WRITE4CSV=False
MOST_RECENT_PREIND=False
WHICH_GMT_FOR_2016=False

if CREATE_median_wrt18501900 then
  ;This takes all the separate files with a single year that each contain 10,000 ensemble members
  ;and then creates a median of them 

  dir="/data/aod/Ilyasetal2017_annual/wrt_18501900"
  ;make a file to hold the median...
  system("ncwa -O -a ens_mem "+dir+"/ann_hd_ens100.nc "+dir+"/median.nc")
  system("ncks -O --mk_rec_dmn time "+dir+"/median.nc "+dir+"/median.nc")

  mdn_fil=addfile(dir+"/median.nc","w")
  median=mdn_fil->temperature_anomaly

  do i=0,166
    yr=1850+i
    print("Working on "+yr)

    ;load data from each individual ANNUAL file
    yrfil=addfile(dir+"/"+yr+".ann_ensmem.wrt_18501900.nc","r")
    ts=yrfil->temperature_anomaly(:,0,:,:);lose degenerate time dimension on load 
    median(i,:,:)=(/dim_median_n(ts,0)/)
  end do
  mdn_fil->temperature_anomaly=median
end if

if POPN_WEIGHTED_TIMESERIES then
  ;create a global mean that is wieghted by population rather than area
  popn_file=addfile("/home/ucfaako/DATA/popc_05x05.nc","r"); 
  popn=popn_file->population ;POPN needs replacing with file variable name
  dir="/data/aod/Ilyasetal2017_annual/wrt_18501900"
  mdn_fil=addfile(dir+"/median.nc","w")
  median=mdn_fil->temperature_anomaly
  ;alter the units for the time coordinate
  time=median&time
  time@units="months since 1850-01-01"
  median&time=time
  
  area_wgts=NormCosWgtGlobe(median&latitude)
  area_wgtd_mean=wgt_areaave_Wrap(median,area_wgts, 1.0, 1)  
  
  popn_wgtd_mean=area_wgtd_mean;to copy Metadata
  ;using HYDE's 1851 population for 1850 
  popn_wgtd_mean(0)=(/wgt_areaave2(median(0,:,:),popn(0,:,:),0)/)
  do i=0,165
    popn_wgtd_mean(i+1)=(/wgt_areaave2(median(i+1,:,:),popn(i,:,:),0)/)
  end do

  system("rm "+dir+"/median_wieghted_global_means.nc")
  outfil=addfile(dir+"/median_wieghted_global_means.nc","c")
  outfil->area_wgtd_mean=area_wgtd_mean
  outfil->popn_wgtd_mean=popn_wgtd_mean
  delete(outfil)
end if

if POPN_ABOVE_TEMPS then
  ;create a global mean that is wieghted by population rather than area
  popn_file=addfile("/home/ucfaako/DATA/popc_05x05.nc","r"); 
  popn=popn_file->population ;POPN needs replacing with file variable name
  dir="/data/aod/Ilyasetal2017_annual/wrt_18501900"
  mdn_fil=addfile(dir+"/median.nc","w")
  median=mdn_fil->temperature_anomaly

  ;setup some holding variables
  popn_temp=popn(:,0:10,0);to copy Metadata
  popn_temp@long_name="Population at temperature (oC from preindustrial)" 
  popn_tot=popn(:,0,0);to copy Metadata
  popn_tot@long_name="Total global population" 
  popn_temp!1="temp_anom"
  temp_anom=(/-0.5,0,0.5,1,1.5,2,2.5,3.,3.5,4.,4.5/)
  n_temp=dimsizes(temp_anom)
  temp_pm=0.25
  temp_anom@long_name="Annual mean anomlay w.r.t. preindustrial"
  temp_anom@units="oC"
  popn_temp&temp_anom=temp_anom

  ;calculate the numbers
  do i=0,165
    popn_temp(i,0)=(/sum(where(median(i+1,:,:).lt.(temp_anom(0)+temp_pm),popn(i,:,:),0))/)
    do j=1,n_temp-2
      popn_temp(i,j)=(/sum(where((median(i+1,:,:).gt.(temp_anom(j)-temp_pm)).and.\
              (median(i+1,:,:).le.(temp_anom(j)+temp_pm)),popn(i,:,:),0))/)
    end do
    popn_temp(i,n_temp-1)=(/sum(where(median(i+1,:,:).gt.(temp_anom(n_temp-1)-temp_pm),popn(i,:,:),0))/)
    popn_tot(i)=(/sum(popn(i,:,:))/)
  end do

  if WRITE4CSV then
    yr=cd_string(popn_temp&time,"%Y")
    print("OUTPUT OF popn_vs_median_1850.ncl")
    print("Year,"+str_join(tostring(temp_anom),",")+",Total_popn")
    do i=0,165
      print(tostring(yr(i))+","+str_join(tostring(popn_temp(i,:)),",")+","+tostring(popn_tot(i)))
    end do
  else
    system("rm "+dir+"/median_popn_wTemps.nc")
    outfil=addfile(dir+"/median_popn_wTemps.nc","c")
    outfil->popn_temp=popn_temp
    delete(outfil)
  end if
end if

if AREA_ABOVE_TEMPS then
  dir="/data/aod/Ilyasetal2017_annual/wrt_18501900"
  mdn_fil=addfile(dir+"/median.nc","w")
  median=mdn_fil->temperature_anomaly

  ;setup some holding variables
  temp_interval=0.25
  temp_anom=fspan(-0.5,2.5,toint((2.5+0.5)/temp_interval+1))
  n_temp=dimsizes(temp_anom)
  temp_pm=temp_interval/2.
  temp_anom@long_name="Annual mean anomlay w.r.t. preindustrial"
  temp_anom@units="oC"
  pct_temp=median(:,0:n_temp-1,0);to copy Metadata
  pct_temp@long_name="Population at temperature (oC from preindustrial)" 
  pct_temp!1="temp_anom"
  pct_temp&temp_anom=temp_anom

  area=median(0,:,:)
  lats=conform_dims((/36,72,4/),median&latitude,0)
  lons=conform_dims((/36,72,4/),median&longitude,1)
  lats=lats+2.5
  lats(:,:,1:2)=lats(:,:,1:2)-5
  lons(:,:,0:1)=lons(:,:,0:1)-2.5
  lons(:,:,2:3)=lons(:,:,2:3)+2.5
  area=(/tofloat(gc_qarea(lats,lons))/)
  area=area*100/(4*PI_C);convert to area to % globe
  ;calculate the numbers
  do i=0,166
    pct_temp(i,0)=(/sum(where(median(i,:,:).lt.(temp_anom(0)+temp_pm),area,0))/)
    do j=1,n_temp-2
      pct_temp(i,j)=(/sum(where((median(i,:,:).gt.(temp_anom(j)-temp_pm)).and.\
              (median(i,:,:).le.(temp_anom(j)+temp_pm)),area,0))/)
    end do
    pct_temp(i,n_temp-1)=(/sum(where(median(i,:,:).gt.(temp_anom(n_temp-1)-temp_pm),area,0))/)
  end do

  if WRITE4CSV then
    time=median&time
    time@units="months since 1850-01-01"
    yr=tofloat(cd_string(time,"%Y"))
    print("OUTPUT OF popn_vs_median_1850.ncl")
    print("Year,"+str_join(tostring(temp_anom),","))
    do i=0,165
      print(tostring(yr(i))+","+str_join(tostring(pct_temp(i,:)),","))
    end do
  else
    system("rm "+dir+"/median_area_wTemps.nc")
    outfil=addfile(dir+"/median_area_wTemps.nc","c")
    outfil->pct_temp=pct_temp
    delete(outfil)
  end if
end if

if MOST_RECENT_PREIND then
  dir="/data/aod/Ilyasetal2017_annual/wrt_18501900"
  mdn_fil=addfile(dir+"/median.nc","r")
  median=mdn_fil->temperature_anomaly
  time=median&time
  time@units="months since 1850-01-01"
  yr=tofloat(cd_string(time,"%Y"))

  threshold=0.0
  preind_yrs=median
  preind_yrs=where(median.le.threshold,conform(median,yr,0),0)
  most_recent_preind=dim_max_n_Wrap(preind_yrs,0)
  roughlatlonplot(most_recent_preind)
  system("rm "+dir+"/most_recent_preind.nc")
  outfil=addfile(dir+"/most_recent_preind.nc","c")
  outfil->most_recent_preind=most_recent_preind
  delete(outfil)
end if

if WHICH_GMT_FOR_2016 then
  dir="/data/aod/Ilyasetal2017_annual/wrt_18501900"
  mdn_fil=addfile(dir+"/median.nc","r")
  median=mdn_fil->temperature_anomaly
  ;crude pattern-scaling
  rcp_fil=addfile("/data/aod/cmip5_processed_files/cmip5_merged_temp_5x5/decadal_combined.nc","r")
  dec_temps=rcp_fil->temp
  dec_gmt=wgt_areaave_Wrap(dec_temps,cos(RAD_C*dec_temps&lat),1.,0)
  pattern=median(0,:,:)
  pattern=(/dim_avg_n(dec_temps/conform(dec_temps,dec_gmt,0),0)/)
  which_gmt=median
  time=median&time
  time@units="months since 1850-01-01"
  year=toint(cd_string(time,"%Y"))
  which_gmt!0="year"
  which_gmt&year=year
  which_gmt=(/median/conform(median,pattern,(/1,2/))/)
  roughlatlonplot(median(166,:,:))
  system("rm "+dir+"/which_gmt_for_year.nc")
  outfil=addfile(dir+"/which_gmt_for_year.nc","c")
  outfil->which_gmt=which_gmt
  delete(outfil)
end if

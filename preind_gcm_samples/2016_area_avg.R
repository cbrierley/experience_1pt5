##From LK ensmbles 1850-1900 climatology
rm(list=ls())
library(ncdf4)

		##Function of monthly global average
		##sum(x_i_gridpoint * areaof_x_i_gridpoint)/sum(areaof_x_i_gridpoint)
		##and W= cos(lat*0.01745329)      
     
            MON_AVG<- function(A, lon, lat){  #A-3D matrix (lon, lat, time) 
		
			A1<- rep(0,dim(A)[3])
                  w<- cos(lat*(pi/180)) #weights  
			W<- matrix(rep(w,each=length(lon)),nrow=length(lon)) #weights matrix

			for (i in 1:dim(A)[3]){
                        A2<- A[,,i]
                        W1_temp<- c(W)
                        W1_temp[is.na(c(A2))]<- NA
                        W1<- matrix(W1_temp, nrow=nrow(A2), ncol=ncol(A2)) 
                        A3<- A2*W1
                        A1[i]<- sum(A3,2, na.rm=TRUE)/sum(W1, na.rm=TRUE)                    
			}
			A1
			}

 	a1<- "D:/HadCRUT.4.5.0.0.median.nc" #downloaded 16th Feb. 2017
     	fid.t<- nc_open(a1,verbose=TRUE,write=FALSE)
	time.t<- ncvar_get(fid.t,'time')     
	origin.t <- fid.t$dim$time$units
	tunits.t <- strsplit(fid.t$dim$time$units,split=" ") 
	if(identical(tunits.t[[1]][1],"minutes")) tunits.t[[1]][1]<-"mins"
	TIME.t<- strptime(paste(tunits.t[[1]][3],tunits.t[[1]][4]),"%Y-%m-%d %H:%M:%S",tz="GMT")+
                           as.numeric(as.difftime(time.t,units=tunits.t[[1]][1]),units="secs")
	lat.t <- ncvar_get(fid.t,'latitude');lat.t                 		#length(lat.t)         #36
	lon.t <- ncvar_get(fid.t,'longitude'); lon.t               		#length(lon.t)         #72
	
	S_y<- as.numeric(format(TIME.t[1],'%Y'))
	bp.ind<- which(TIME.t>='1850-01-16' & TIME.t<='1900-12-17')       #base-period (1850-1879) index
	ind_2016<- which(TIME.t>='2016-01-16' & TIME.t<='2016-12-17')  

mon_avg_2016<- matrix(NA, nrow=12, ncol=1)

##read 100 LK-ensembles and calculate monthly area averages
		for(j in 1:100){
		for(i in 1:100){
		filename1=paste("D:/files_18feb/lk_nc/hd_ens",j,"/ensemble",i,".nc",sep="")
		fid.t1<- nc_open(filename1,verbose=TRUE,write=FALSE)
		tempr.a1 <- ncvar_get(fid.t1,'temperature')
		B_2016<- tempr.a1[,,ind_2016] 
		mon_avg_2016<- cbind(mon_avg_2016,MON_AVG(B_2016,lon.t,lat.t))
		nc_close(fid.t1)
		}
		}
#write.table(mon_avg_2016[,-1], "C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/2016_mon_area_avg.txt")



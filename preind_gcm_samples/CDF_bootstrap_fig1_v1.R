rm(list=ls())##2200
library(sROC)
##~~CDF
##A cumulative density function of the difference between a "pre-industrial" 1400-1800 baseline and 1850-1900.
T<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_andrew/cdf_All_Forcings.txt")

		#X11()
		#plot(T[,2])
		##it is read as for example F(-0.39)=P(X<= -0.39)= 0 & F(-0.09)=P(X<=-0.09)= 0.0006666667 so on
		##so F(-0.38)-F(-0.39)=P(-0.39<=X<=-0.38)=0 & 
		##F(-0.09)-F(-0.1)= P(-0.1<=X<=-0.09)= 0-0.0006666667=0.0006666667 so on
		##https://stackoverflow.com/questions/37151571/calculate-derivative-of-cumulative-distribution-cdf-to-get-probability-density

set.seed(102)
##~~Obtain samples having the CDF above
		##A probability density function of the difference between a "pre-industrial" 1400-1800 baseline and 1850-1900.
	pdf_a<- diff(T[,2])    #/diff(T[,1])
	freq<- 23*pdf_a        #multiply with 51 so that after rounding we have 10,000
		#sum(round(freq))
		#range(T[,1])

	##Frequency distribution
	breaks<- seq(min(T[,1]),max(T[,1]), by=0.01)
	Cl<- cbind(seq(breaks[1],breaks[length(breaks)-1],by=(T[,1][2]-T[,1][1])), seq(breaks[2],breaks[length(breaks)],by=(T[,1][2]-T[,1][1])))
	XX<- apply(Cl,1,mean)
	YY<- cbind(XX,round(freq))

##samples of the difference of climatology 1400-1800 to 1850-1900 
clm_a<- rep(XX,round(freq))   #median(clm_a)=0.075 as is the median of CDF above 
					#hist(clm_a)
median(clm_a)
length(clm_a)

x<- clm_a
x.CDF <- kCDF(x, ngrid=length(T[,1]))#x.grid=T[,1],
x.CDF
ll<- CI.CDF(x.CDF)$Fhat.lower
uu<- CI.CDF(x.CDF)$Fhat.upper
xhat<- x.CDF$x
fhat<- x.CDF$Fhat

	##data based on xhat and fhat
	pdf_1<- diff(fhat)
	freq_1<- 23*pdf_1       #100000*pdf_1 

	breaks_1<- seq(min(xhat),max(xhat), by=c(xhat[2]-xhat[1]))
	Cl_1<- cbind(seq(breaks_1[1],breaks_1[length(breaks_1)-1],by=c(xhat[2]-xhat[1])), seq(breaks_1[2],breaks_1[length(breaks_1)],by=c(xhat[2]-xhat[1])))
	XX_1<- apply(Cl_1,1,mean)
	YY_1<- cbind(XX_1,round(freq_1))
	clm_1<- rep(XX_1,round(freq_1))
	median(clm_1)
	length(clm_1)
	#clm_1<- sample(clm_1,50)

B<- 10000
n<-length(clm_1)
x<- clm_1

xb<- matrix(0, nrow=n,ncol=B)

mpf<- matrix(0,nrow=B,ncol=length(XX_1))

     mn<-function(x,m){
     a<- table(x)
     b<-rep(0,m)
     b[as.integer(names(a))]<-a
     return(b)
     }
 for(b in 1:B){
    xb[,b]<- sample(1:length(XX_1),n,replace=TRUE,prob=c(pdf_1))
    mpf[b,]<- mn(xb[,b],length(XX_1))/n
    }

MPF<- apply(mpf,1,cumsum)
med<- apply(MPF,1,median)
LL<- apply(MPF,1,function(x)quantile(x,probs=0.025))
UU<- apply(MPF,1,function(x)quantile(x,probs=0.975))

X11()
par(mfrow=c(2,2))
plot(XX_1,LL,ylim=c(0,1),type="n", xlab="",ylab="",main="")
lines(XX_1,LL)
lines(XX_1,UU)
lines(XX_1,med, col=2)
legend("topleft", bty="n", c("95% CI","median"),col=c(1,2),lty=c(1,1) )
plot(x.CDF)

CLM_O<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/1850_1900_clm.txt")
clm_o<- apply(CLM_O,2, mean)

AVG_2016<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/2016_mon_area_avg.txt")
avg_2016<- apply(AVG_2016,2, mean)

YY2<- avg_2016-clm_o

yy3a<- XX_1[c(xb)] #median(yy3a)
#yy3a<- 
	#yy3a<- rep(clm_1,10000)
yy3b<- rep(YY2,each=n)
	#yy3b<- rep(YY2, each=length(clm_1))

YY4<- yy3a+yy3b
YY4<- round(YY4,2)

median(YY4)
quantile(YY4, prob=0.025)
quantile(YY4, prob=0.975)
sum(YY4>=1.5)/length(YY4)

YY2<- round(YY2,2)
median(YY2)
quantile(YY2, prob=0.025)
quantile(YY2, prob=0.975)
sum(YY2>=1.5)/length(YY2)

> median(YY2)
[1] 1.17
> quantile(YY2, prob=0.025)
2.5% 
1.03 
> quantile(YY2, prob=0.975)
97.5% 
 1.33 
> sum(YY2>=1.5)/length(YY2)
[1] 0


> median(YY4)
[1] 1.25
> quantile(YY4, prob=0.025)
2.5% 
   1 
> quantile(YY4, prob=0.975)
97.5% 
 1.52 
> sum(YY4>=1.5)/length(YY4)
[1] 0.03617826


X11()
par(mfrow=c(3,2))
plot(T[,1],T[,2],col=1,xlab="",ylab="",main="Empirical CDF", pch=1, xaxt="n")
axis(1,at=seq(-0.4,0.4, by=0.1))
	#abline(h=0.5, col=2)
	#lines(xhat,fhat,col=2)
	#legend("topleft", bty="n",c("Empirical CDF", "Kernel estimate"),col=c(1,2), pch=c(1,NA),lty=c(NA,1))

hist(clm_a,prob=TRUE,breaks=seq(-0.05,0.2,0.025), main="PDF from empirical CDF", xlab="median=0.075")
abline(v=median(clm_a),col=2)
plot(x.CDF, alpha=0.05, main="Kernel estimate of CDF", ylab="", xlab="")
#hist(clm_1,prob=TRUE,breaks=seq(-0.05,0.2,0.025), main="PDF from kernel estimate",xlab="median=0.076")
#abline(v=median(clm_1),col=2)

hist(yy3a,breaks=seq(-0.3,0.45,by=0.05), prob=TRUE,main="PDF from kernel estimate", xlab="median=0.076")
abline(v=median(yy3a), col=2)

hist(YY4, freq=FALSE,breaks=seq(0.7,1.82,by=0.02),xaxt="n",
col="blue", xlab=substitute(paste(~Delta,"T wrt preindustrial")), ylab="probability", 
main=substitute(paste(2016~Delta,"T wrt preindustrial")))
axis(1, at= seq(0.7,1.82, by=0.1))
#dev.off()
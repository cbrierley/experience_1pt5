rm(list=ls())
library(sROC)
##~~CDF
##A cumulative density function of the difference between a "pre-industrial" 1400-1800 baseline and 1850-1900.
T<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/paper1.5oC_r_files/data_andrew/cdf_All_Forcings.txt")

		#X11()
		#plot(T[,2])
		##it is read as for example F(-0.39)=P(X<= -0.39)= 0 & F(-0.09)=P(X<=-0.09)= 0.0006666667 so on
		##so F(-0.38)-F(-0.39)=P(-0.39<=X<=-0.38)=0 & 
		##F(-0.09)-F(-0.1)= P(-0.1<=X<=-0.09)= 0-0.0006666667=0.0006666667 so on
		##https://stackoverflow.com/questions/37151571/calculate-derivative-of-cumulative-distribution-cdf-to-get-probability-density

##~~Obtain samples having the CDF above
		##A probability density function of the difference between a "pre-industrial" 1400-1800 baseline and 1850-1900.
	pdf_a<- diff(T[,2])  #/diff(T[,1])
	freq<- 100*pdf_a     #multiply with 10,002 instead of 10000 so that after rounding we have 10,000
		#sum(round(freq))
		#range(T[,1])

	##Frequency distribution
	breaks<- seq(min(T[,1]),max(T[,1]), by=0.01)
	Cl<- cbind(seq(breaks[1],breaks[length(breaks)-1],by=(T[,1][2]-T[,1][1])), seq(breaks[2],breaks[length(breaks)],by=(T[,1][2]-T[,1][1])))
	XX<- apply(Cl,1,mean)
	YY<- cbind(XX,round(freq))

##samples of the difference of climatology 1400-1800 to 1850-1900 
clm_a<- rep(XX,round(freq))   #median(clm_a)=0.075 as is the median of CDF above 
					#hist(clm_a)
x<- clm_a
x.CDF <- kCDF(x)
x.CDF
ll<- CI.CDF(x.CDF)$Fhat.lower
uu<- CI.CDF(x.CDF)$Fhat.upper
xhat<- x.CDF$x
fhat<- x.CDF$Fhat

	##data based on xhat and fhat
	pdf_1<- diff(fhat)
	freq_1<- 100*pdf_1       #100000*pdf_1 

	breaks_1<- seq(min(xhat),max(xhat), by=c(xhat[2]-xhat[1]))
	Cl_1<- cbind(seq(breaks_1[1],breaks_1[length(breaks_1)-1],by=c(xhat[2]-xhat[1])), seq(breaks_1[2],breaks_1[length(breaks_1)],by=c(xhat[2]-xhat[1])))
	XX_1<- apply(Cl_1,1,mean)
	YY_1<- cbind(XX_1,round(freq_1))
	clm_1<- rep(XX_1,round(freq_1))

B<- 1000
n<-length(clm_1)
x<- clm_1

xb<- matrix(0, nrow=n,ncol=B)

mpf<- matrix(0,nrow=B,ncol=length(XX_1))

     mn<-function(x,m){
     a<- table(x)
     b<-rep(0,m)
     b[as.integer(names(a))]<-a
     return(b)
     }

 for(b in 1:B){
    xb[,b]<- sample(1:length(XX_1),n,replace=TRUE,prob=c(pdf_1))
    mpf[b,]<- mn(xb[,b],length(XX_1))/n
    }

MPF<- apply(mpf,1,cumsum)
med<- apply(MPF,1,median)
LL<- apply(MPF,1,function(x)quantile(x,probs=0.025))
UU<- apply(MPF,1,function(x)quantile(x,probs=0.975))

X11()
par(mfrow=c(2,2))
plot(XX_1,LL,ylim=c(0,1),type="n", xlab="",ylab="",main="")
lines(XX_1,LL)
lines(XX_1,UU)
lines(XX_1,med, col=2)
legend("topleft", bty="n", c("95% CI","median"),col=c(1,2),lty=c(1,1) )
plot(x.CDF)

par(mfrow=c(1,2))
plot(XX_1,LL,ylim=c(0,1),type="n", xlab="",ylab="")
lines(XX_1,LL)
lines(XX_1,UU)
lines(XX_1,med, col=2)
legend("topleft", bty="n", c("95% CI","median"),col=c(1,2),lty=c(1,1) )
plot(x.CDF)

CLM_O<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/1850_1900_clm.txt")
clm_o<- apply(CLM_O,2, mean)

AVG_2016<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/2016_mon_area_avg.txt")
avg_2016<- apply(AVG_2016,2, mean)

YY2<- avg_2016-clm_o

YY3<- matrix(0, nrow=100,ncol=10000)

for(i in 1:10000){
YY3[,i]<- sample(xb,100,replace=FALSE)
}

yy3a<- XX_1[c(YY3)]
yy3b<- rep(YY2,each=100)

YY4<- yy3a+yy3b

median(YY4)
quantile(YY4, prob=0.025)
quantile(YY4, prob=0.975)


#rm(list=ls())
f <- ecdf(rnorm(100)) 
x <- rnorm(10) 
y <- f(x) 

#If you want to get the x corresponding to given y, use linear interpolation. 

inv_ecdf <- function(f){ 
        x <- environment(f)$x 
        y <- environment(f)$y 
        approxfun(y, x) 
} 

g <- inv_ecdf(f) 
g(0.5) 



############################################################################################################################

YY4<- apply(YY3,2,function(x) YY3)

YY3<- rep(YY2,each=100)


YY4<- numeric(0)

system.time({

for(i in 1:500){
YY4<- append(YY4,YY2+sample(xb,100,replace=FALSE))
}
})

sam_sz<- 100
clm_sam_1<- sample(clm_1,sam_sz,replace=FALSE)
clm_sam_full<- rep(c(clm_sam_1),10000)






sample()










hist()












B<-200
n<-length(clm_1)
x<- clm_1

  mpf<- matrix(0,nrow=B,ncol=length(XX_1))

     mn<-function(x,m){
     a<- table(x)
     b<-rep(0,m)
     b[as.integer(names(a))]<-a
     return(b)
     }

 for(b in 1:B){
    xb<- sample(1:length(XX_1),n,replace=TRUE,prob=c(pdf_1))
    mpf[b,]<- mn(xb,length(XX_1))/n
    }

MPF<- apply(mpf,1,cumsum)
med<- apply(MPF,1,median)
LL<- apply(MPF,1,function(x)quantile(x,probs=0.025))
UU<- apply(MPF,1,function(x)quantile(x,probs=0.975))

X11()
plot(LL,ylim=c(0,1),type="n")
lines(LL)
lines(UU)
lines(med, col=2)






rm(list=ls())
##Example 1 (Bootstrap esitmate of F_n)
   B<-200
   n<-10
   x<-c(2,2,1,1,5,4,4,3,1,2) #observed sample
   mpf<-matrix(0,nrow=B,ncol=5)

   mn<-function(m,x){
     a<-table(x)
     b<-rep(0,m)
     b[as.integer(names(a))]<-a
     return(b)
     }
  
   for(b in 1:B){
    xb<-sample(1:5,n,replace=T,prob=c(0.3,0.3,0.1,0.2,0.1))
    mpf[b,]<- mn(5,xb)/n
    }

MPF<- apply(mpf,1,cumsum)
med<- apply(MPF,1,median)
LL<- apply(MPF,1,function(x)quantile(x,probs=0.025))
UU<- apply(MPF,1,function(x)quantile(x,probs=0.975))

##OR

med<- apply(mpf,2,median)
LL<- apply(mpf,2,function(x)quantile(x,probs=0.025))
UU<- apply(mpf,2,function(x)quantile(x,probs=0.975))

X11()
plot(LL,ylim=c(0,1),type="n")
lines(LL)
lines(UU)
lines(med, col=2)

cumsum(table(xb))/length(xb)







X11()
par(mfrow=c(2,2))
plot(T[,1],T[,2],col=1,xlab="",ylab="", pch=1)
lines(xhat,fhat,col=2)
legend("topleft", bty="n",c("Empirical CDF", "Kernel estimate"),col=c(1,2), pch=c(1,NA),lty=c(NA,1))
hist(clm_a,prob=TRUE, main="PDF from empirical CDF", xlab="median=0.075")
abline(v=median(clm_a),col=2)
hist(clm_1,prob=TRUE, main="PDF from kernel estimate",xlab="median=0.077")
abline(v=median(clm_1),col=2)

##samples of CDF
AA<- cbind(ll,uu)

FF<- t(apply(AA,1,function(x)seq(x[1],x[2],length=10)))

obs<- matrix(0,nrow=100, ncol=10)

for(i in 1:10){
	pdf_b<- diff(sort(FF[,i]))
	freq_b<- 100000*pdf_b 
	clm_b<- rep(XX_1,round(freq_b))
	obs[,i]<- sample(clm_b,100,replace=FALSE)
}

AVG_2016<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/2016_mon_area_avg.txt")
avg_2016<- apply(AVG_2016,2, mean)

CLM_O<- read.table("C:/Users/maryam/Dropbox/PhD UCL/kriging_and_FEOFs/post_upgrade/hd_lk_ts_data/1850_1900_clm.txt")
clm_o<- apply(CLM_O,2, mean)

YY2<- avg_2016-clm_o

YY3<- rep(YY2,each=length(obs))

clm_sam_full<- rep(c(obs),10000)
YY4<- YY3+clm_sam_full

YY3<- numeric(0)

for(h in 1:10000){

YY3<- append(YY3, (YY2[h]+c(obs)) )	
}





##Bootstrap samples from clm_1
B<- 2000
n<- length(clm_1)

clm_a_b<- matrix(0, ncol=B, nrow=n)

   for(b in 1:B){
    		ind<- sample(1:n,n,replace=TRUE)
    		clm_a_b[,b]<- clm_1[ind]
		ee<- ecdf(clm_a_b[,b])
		lines(environment(ee)$x,environment(ee)$y)
      }

e1<- ecdf(clm_a_b[,1])
e2<- ecdf(clm_a_b[,200])

  x1 <- environment(e1)$x
  y1 <- environment(e1)$y 

  x2 <- environment(e2)$x
  y2 <- environment(e2)$y 

plot(x1,y1,type="l")
lines(x2,y2, col=2)


 



XHAT<- matrix(0, ncol=B, nrow=256)
FHAT<- matrix(0, ncol=B, nrow=256)

 xb.CDF<- kCDF(clm_a_b)
    XHAT[,b]<- xb.CDF$x
    FHAT[,b] <-  xb.CDF$Fhat



##Kernel estimate and confidence interval of the empirical cdf above
x<- clm_a
x.CDF <- kCDF(x)
x.CDF
ll<- CI.CDF(x.CDF)$Fhat.lower
uu<- CI.CDF(x.CDF)$Fhat.upper
xhat<- x.CDF$x
fhat_uu<- sort(uu)   #  plot(sort(uu),type="n")
			   #  lines(sort(ll), col=1)
                     #  lines((ll),col=2)
			   #  lines(sort(uu), col=1)
			   #  lines(uu, col=2)
fhat_ll<- sort(ll)
fhat<- x.CDF$Fhat

		X11()
		par(mfrow=c(1,2))
		plot(T[,2])
		plot(x.CDF, alpha=0.05, main="Kernel estimate of distribution function")

X11()
plot(ecdf(x))
for(i in 1:1000){
ind<- sample(1:100,size=90,replace=TRUE)
lines(ecdf(clm_a[ind]),col="gray")
}


  x1 <- environment(e1)$x 
  y1 <- environment(e2)$y 

inv_ecdf <- function(f){ 
        x <- environment(f)$x 
        y <- environment(f)$y 
        approxfun(y, x) 
} 

g <- inv_ecdf(e1) 

X11()
plot(x1,y1)










x<- rnorm(500, mean=5, sd=2)

mu<- numeric(0)
for(i in 1:1000){
ind<- sample(1:500,300, replace=TRUE)
mu<- append(mu,mean(x[ind]))
}

median(mu)
quantile(mu, 0.025)
quantile(mu, 0.975)

ecdf(x)



rm(list=ls())
B<-200
n<- length(clm_a)
   x<- clm_a #observed sample

   mpf<-matrix(0,nrow=B,ncol=n)
   mn<-function(m,x){
     a<-table(x)
     b<-rep(0,m)
     b[as.integer(names(a))]<-a
     return(b)
     }
     
   for(b in 1:B){
    xb<-sample(clm_a,n,replace=T)
    mpf[b,]<-mn(5,xb)/n
    }
   probs<-apply(mpf,2,mean)
   
   becdf<-cumsum(probs)
   a<-rbind(becdf,cumsum(table(x)/n), ppois(1:5,lambda=2))
   rownames(a)<-c("Bootstrap ecdf","ecdf","cdf")
   a


rm(list=ls())
##Example 1 (Bootstrap esitmate of F_n)
   B<-200
   n<-10
   x<-c(2,2,1,1,5,4,4,3,1,2) #observed sample
   mpf<-matrix(0,nrow=B,ncol=5)
   mn<-function(m,x){
     a<-table(x)
     b<-rep(0,m)
     b[as.integer(names(a))]<-a
     return(b)
     }
  
   for(b in 1:B){
    xb<-sample(1:5,n,replace=T,prob=c(0.3,0.3,0.1,0.2,0.1))
    mpf[b,]<-mn(5,xb)/n
    }

med<- med<- apply(mpf,2,median)
LL<- apply(mpf,2,function(x)quantile(x,probs=0.025))
UU<- apply(mpf,2,function(x)quantile(x,probs=0.975))

X11()
plot(LL,ylim=c(0,1),type="n")
lines(LL)
lines(UU)
lines(med, col=2)





> LL<- apply(mpf,2,function(x)quantile(x,probs=0.025))
> LL
[1] 0.1 0.0 0.0 0.0 0.0
> UU<- apply(mpf,2,function(x)quantile(x,probs=0.975))


   probs<-apply(mpf,2,mean)
   
   becdf<-cumsum(probs)
   a<-rbind(becdf,cumsum(table(x)/n), ppois(1:5,lambda=2))
   rownames(a)<-c("Bootstrap ecdf","ecdf","cdf")
   a





data(fluazinam)
(d1 <-log10(fluazinam))
f1 <- fitdistcens(d1, "norm")
b1 <- bootdistcens(f1, niter = 101)
b1
summary(b1)
plot(b1)
quantile(b1)
CIcdfplot(b1, CI.output = "quantile")
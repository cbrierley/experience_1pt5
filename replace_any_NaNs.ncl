;Start with the 2017/2018 files
filenames=systemfunc("ls /data/aod/Ilyasetal2017_annual/wrt_*/201[78].ann_ensmem.wrt_*.nc")
do i = 0,dimsizes(filenames)-1
  fil=addfile(filenames(i),"w")
  temp=fil->temperature_anomaly
  if (any(isnan_ieee(temp))) then
    if(.not.isatt(temp,"_FillValue")) then
      temp@_FillValue = default_fillvalue(typeof(temp))
    end if
    replace_ieeenan (temp,temp@_FillValue, 0)
    fil->temperature_anomaly=temp
    print("Edited "+filenames(i))
  end if
  delete(temp)
  delete(fil)
end do
delete(filenames)

filenames=systemfunc("ls /data/aod/Ilyasetal2017_annual/wrt_*/ann_hd_ens*.nc")
do i = 0,dimsizes(filenames)-1
  fil=addfile(filenames(i),"w")
  temp=fil->temperature_anomaly
  if (any(isnan_ieee(temp))) then
    if(.not.isatt(temp,"_FillValue")) then
      temp@_FillValue = default_fillvalue(typeof(temp))
    end if
    replace_ieeenan (temp,temp@_FillValue, 0)
    fil->temperature_anomaly=temp
    print("Edited "+filenames(i))
  end if
  delete(temp)
  delete(fil)
end do


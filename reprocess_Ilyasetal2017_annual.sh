#!/bin/bash

#Script to convert Maryam monthly ensemble into annual differences

data_in_path="/home/ucakmil/lk_non_par_ensemble"
update_in_path="/data/aod/Ilyasetal2017_monthlyupdate"
data_out_path="/data/aod/Ilyasetal2017_annual"

#SCRATCH="/scratch/chris"
SCRATCH="/data/aod/stand_in_for_scratch/"
mkdir -p $SCRATCH


COUNTER=1
while [  $COUNTER -le 100 ]
do
  cd $data_in_path/hd_ens$COUNTER
  for fil in ensemble*.nc
  do 
    ncra -O --mro -d time,,,12,12 $fil $SCRATCH/ann.$fil #this command spits out warning about months. Do not worry
    ncatted -a units,time,o,c,"months since 1850-01-01" $SCRATCH/ann.$fil
    ncra -O --mro -d time,,,12,12 $update_in_path/hd_ens$COUNTER/$fil $SCRATCH/update.$fil #this command spits out warning about months. Do not worry
    ncatted -a units,time,o,c,"months since 2017-01-01" $SCRATCH/update.$fil
    ncrename -v temperature,temperature_anomaly $SCRATCH/update.$fil
    ncrcat -O $SCRATCH/ann.$fil $SCRATCH/update.$fil $SCRATCH/catted.$fil
  done  
  ncecat -O -u ens_mem $SCRATCH/catted.ensemble*.nc $data_out_path/ann_hd_ens$COUNTER.nc
  let COUNTER=COUNTER+1 
done
rm $SCRATCH/ann.*.nc

cd $data_out_path
mkdir wrt_18501900
ann_files=`ls ann_hd_ens*.nc`
for fil in $ann_files
do
    ncwa -O -a time -d time,0,49 $fil $SCRATCH/baseline.nc
    ncdiff -O $fil $SCRATCH/baseline.nc wrt_18501900/$fil
done
rm $SCRATCH/baseline.nc

cd wrt_18501900
COUNTER=0
while [  $COUNTER -lt 169 ]
do
  let yr=1850+COUNTER
  ncrcat -O -d time,$COUNTER ann_hd_ens*.nc $yr.ann_ensmem.wrt_18501900.nc
  let COUNTER=COUNTER+1 
done

rm $SCRATCH/*
rmdir $SCRATCH
clc, clear all, 

%% GOAL: Assess if yy3a is normally distributed


%% SET UP: Import dataset yy3a

yy3a=textread('bitbucket/experience_1pt5/temp_preind/yy3a.txt');

%Make dataset 230000x1 instead of 46000x5
yy3a=[yy3a(:,1);yy3a(:,2);yy3a(:,3);yy3a(:,4);yy3a(:,5)];

%% ASSESSMENT 1: Plot to visually examine if dataset is normally distributed

figure(1)
histogram(yy3a)
title('Histogram of yy3a')
xlabel('Value (°C)')
ylabel('Frequency')

%% ASSESSMENT 2: Assess if ~68% is within 1 std of mean, and ~95% is within 1.96 std of mean

%calculate standard deviation
stdyy3a=std(yy3a);
%calculate mean
meanyy3a=mean(yy3a);


count1=0;
for i=1:length(yy3a)
    if yy3a(i)>(meanyy3a-stdyy3a) && yy3a(i)<(meanyy3a+stdyy3a)
        count1=count1+1;
    end
end

std100=count1/length(yy3a); %This should be ~0.68, if yy3a is normally distributed


count2=0;
for i=1:length(yy3a)
    if yy3a(i)>(meanyy3a-(1.96*stdyy3a)) && yy3a(i)<(meanyy3a+(1.96*stdyy3a))
        count2=count2+1;
    end
end

std196=count2/length(yy3a); %This should be ~0.95, if yy3a is normally distributed

%% CONCLUSION: Yes - yy3a is normally distributed

fprintf('Yes, yy3a is normally distributed, \nwith a mean of %f and standard deviation of %f\n',meanyy3a,stdyy3a)


clc, clear all, close all


%% GOAL: Create 10000 pre-industrial pattern scales

%{
Steps
1) Create 10000 normally distributed temperature changes from 1961-1990 to 1400-1800
2) Calculate the mean difference between 1850-1900 and 1961-1990 for each of the 10000 ensemble members (Maryam's Datasets)
3) Subtract the 1850-1900/1961-1990 difference from the normally distributed temperature change 1961-1990/1400-1800
4) Apply the remaining temperature change to each ensemble with a random pattern scale(/23 options) provided from ClimGen
%}


%% STEP 1

% Import 230000 values of temperature change 1961-1990 to 1400-1800 (originating in preind_gcm_samples/dist_diff_bw_1400_1800and1961_1990
yy3a=textread('bitbucket/experience_1pt5/temp_preind/yy3a.txt');
%Make dataset 230000x1 instead of 46000x5
yy3a=[yy3a(:,1);yy3a(:,2);yy3a(:,3);yy3a(:,4);yy3a(:,5)];

%calculate standard deviation
stdyy3a=std(yy3a);
%calculate mean
meanyy3a=mean(yy3a);

% (MATLAB script Is_yy3a_normal_dist.m proves that yy4a is normally distributed)

%Create 10000 normally distributed temperature changes
temp1400to1961=stdyy3a.*randn(10000,1)+meanyy3a;

%Check variable
statstemp1400to1961 = [mean(temp1400to1961), std(temp1400to1961)];


%% ASSESSMENT 1: Plot to visually examine if dataset is normally distributed

histogram(temp1400to1961)
title('Histogram of yy3a')
xlabel('Value (°C)')
ylabel('Frequency')

%% STEP 2: 

